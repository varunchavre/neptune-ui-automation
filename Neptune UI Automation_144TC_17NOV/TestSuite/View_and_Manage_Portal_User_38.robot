*** Settings ***

# Library         Selenium2Library
Library         ExtendedSelenium2Library
Library         OperatingSystem
Library         String
Library         Dialogs
Library         BuiltIn
Resource        ${EXECDIR}/Res/Web/GenericFunction.robot
Resource        ${EXECDIR}/Res/Web/View_and_Manage_Portal_User_Resource.robot
Variables       ${EXECDIR}/VariableFiles/Web/Config.yaml
Suite Setup     SUITE SETUP
Suite Teardown  Close Browser
Metadata    Version        0.9


*** Test Cases ***

View All Portal Users table
     [Tags]  C677807 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  VALIDATE ALL PORTAL USERS TABLE ATTRIBUTES


View System Admin user in All Portal Users table
     [Tags]  C709610 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  VALIDATE SYSTEM ADMIN ALL USER PORTAL USERS TABLE


The administrator is able to go to 'Create Portal User' window
     [Tags]  C709567 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  VALIDATE CREATE PORTAL USER FIELDS


Arrange administrator user with sorting
     [Tags]  C679033 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  SORT FUNCTIONALITY VALIDATION FOR ADMIN USER


Perform a search for admin user name
     [Tags]  C679034 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  SEARCH ADMIN IN PORTAL USER

Validate that Breadcrumbs redirect to the correct page (System Management)
      [Tags]  C700612 View and Manage Portal User
     : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
     \   Log To Console  \nIteration No.: ${ITERATION}
     \   Run Keyword and Continue on Failure  VALIDATE BREADCRUMBS REDIRECT TO CORRECT PAGE


Validate the system management dashboard
      [Tags]  C700613 View and Manage Portal User
     : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
     \   Log To Console  \nIteration No.: ${ITERATION}
     \   Run Keyword and Continue on Failure  VALIDATE SYSTEM MANAGEMENT DASHBOARD


All Existing User Roles displayed in Create Portal User window
     [Tags]  C709568 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  VALIDATE USER ROLES PRESENT IN CREATE PORTAL USER


View New Admin user in All Portal Users table
     [Tags]  C709611 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATE NEW PORTAL USER WITH ANY ROLE AND VALIDATE


Create a new administrator with single role
     [Tags]  C626780 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATE PORTAL USER WITH ANY ROLE


Create a new administrator with mandatory fields only
     [Tags]  C709574 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATE PORTAL USER WITH MANDATORY FIELDS


Create a new administrator with missing mandatory fields
     [Tags]  C677810 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATE PORTAL USER WITH MISSING MANDATORY FIELDS


Create a new administrator user with empty password
     [Tags]  C662843 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATE PORTAL USER WITH EMPTY PASSWORD


Create a new administrator user with only spaces in the password
     [Tags]  C696945 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATE PORTAL USER WITH ONLY SPACES IN THE PASSWORD


Create a new administrator user with an incorrect password
     [Tags]  C662844 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATE PORTAL USER WITH INCORRECT PASSWORD


Reset Portal User password: success
     [Tags]  C704204 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  RESET PORTAL USER PASSWORD USING MANUAL RESET OPTION


Edit Portal User window
     [Tags]  C709612 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  VALIDATE EDIT PORTAL USER


Edit another administrator
     [Tags]  C677808 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  EDIT ANOTHER ADMIN IN PORTAL USER


The Administrator cannot change own password for an incorrect password
     [Tags]  C662842 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CHANGE OWN ADMIN PASSWORD WITH INCORRECT PASSWORD


The administrator cannot change own password for an empty password
     [Tags]  C662847 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CHANGE OWN ADMIN PASSWORD WITH EMPTY PASSWORD


The Administrator cannot change own password same as Old Password
     [Tags]  C709590 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CHANGE OWN ADMIN PASSWORD WITH OLD PASSWORD

The Administrator can't change own password same as Old Password
     [Tags]  C709681 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CHANGE OWN ADMIN PASSWORD WITH OLD PASSWORD


Reset System Admin password - lock icon not displayed
     [Tags]  C709685 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  RESET SYSTEM ADMIN PASSWORD


Create a new administrator user with a repeated username
     [Tags]  C662846 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATE PORTAL USER WITH REPEATED USERNAME


Create a new administrator user with an incorrect username
     [Tags]  C694245 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATE PORTAL USER WITH INCORRECT USERNAME


Create a new administrator user with more than 255 characters in the name
     [Tags]  C694212 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATE PORTAL USER WITH MORE THAN 255 CHARACTERS USERNAME


Edit own administrator account
     [Tags]  C677809 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  EDIT OWN ADMIN IN PORTAL USER


Delete another administrator account - trash icon
      [Tags]  C662848 View and Manage Portal User
     : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
     \   Log To Console  \nIteration No.: ${ITERATION}
     \   Run Keyword and Continue on Failure  DELETE ANOTHER ADMIN USING TRASH ICON


Delete another administrator - check box
      [Tags]  C709613 View and Manage Portal User
     : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
     \   Log To Console  \nIteration No.: ${ITERATION}
     \   Run Keyword and Continue on Failure  DELETE ANOTHER ADMIN USING CHECK BOX


Delete multiple administrator - check box
      [Tags]  C709614 View and Manage Portal User
     : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
     \   Log To Console  \nIteration No.: ${ITERATION}
     \   Run Keyword and Continue on Failure  DELETE MULTIPLE ADMIN USING CHECK BOX


Delete system administrator
      [Tags]  C709615 View and Manage Portal User
     : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
     \   Log To Console  \nIteration No.: ${ITERATION}
     \   Run Keyword and Continue on Failure  DELETE SYSTEM ADMIN


Delete own administrator
      [Tags]  C709616 View and Manage Portal User
     : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
     \   Log To Console  \nIteration No.: ${ITERATION}
     \   Run Keyword and Continue on Failure  DELETE OWN ADMIN


Reset own Portal User password - lock icon not displayed
     [Tags]  C709684 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  RESET OWN PORTAL USER PASSWORD


Change another admin password same as current Password
     [Tags]  C709588 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  RESET OTHER ADMIN PASSWORD SAME AS CURRENT PASSWORD


The admin is not able to change another admin password in the email reset link option if the user does not have a registered email
     [Tags]  C706311 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CHANGE ANOTHER ADMIN PASSWORD USING EMAIL RESET OPTION


Create a new administrator with Zebra System Admin role
     [Tags]  C709679 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATE NEW ADMIN WITH ZEBRA SYSTEM ADMIN ROLE


Create a new administrator with multiple roles
     [Tags]  C709609 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATE NEW ADMIN WITH MULTIPLE ROLES


Reset Locked Portal User password: success
     [Tags]  C709682 View and Manage Portal User
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  RESET LOCKED PORTAL USER PASSWORD




