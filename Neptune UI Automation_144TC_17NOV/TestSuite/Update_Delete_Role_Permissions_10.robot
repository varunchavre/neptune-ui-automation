*** Settings ***

# Library         Selenium2Library
Library         ExtendedSelenium2Library
Library         OperatingSystem
Library         String
Library         Dialogs
Resource        ${EXECDIR}/Res/Web/GenericFunction.robot
Resource        ${EXECDIR}/Res/Web/Update_Delete_Role_Permissions_Resource.robot
Variables       ${EXECDIR}/VariableFiles/Web/Config.yaml
Suite Setup     SUITE SETUP
Suite Teardown  Close Browser
Metadata    Version        0.9


*** Test Cases ***

Update Portal Role name and description and permission
     [Tags]  C697319 Update/Delete Role Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  UPDATE PORTAL ROLENAME DESCRIPTION PERMISSION


Update Portal Role with empty name
     [Tags]  C709563 Update/Delete Role Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  UPDATE PORTAL ROLE WITH EMPTY ROLE NAME


Update Portal Role with NO permission selected
     [Tags]  C709564 Update/Delete Role Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  UPDATE PORTAL ROLE WITH NO ROLE


Update Portal Role with existing name
     [Tags]  C709575 Update/Delete Role Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  UPDATE PORTAL ROLE WITH EXISTING ROLE NAME


Update Portal Role with invalid name
     [Tags]  C709576 Update/Delete Role Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  UPDATE PORTAL ROLE WITH INVALID ROLE NAME


Delete Portal Role for the current admin
     [Tags]  C709677 Update/Delete Role Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  DELETE PORTAL ROLE FOR CURRENT ADMIN


Delete a Role Permission
     [Tags]  C697399 Update/Delete Role Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  DELETE ROLE PERMISSION


Delete Several Roles permissions
     [Tags]  C697400 Update/Delete Role Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  DELETE SEVERAL ROLE PERMISSION


Update Portal Role for current admin
     [Tags]  C709677 Update/Delete Role Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  UPDATE CURRENT ADMIN PORTAL ROLE


Verify that role Zebra System Admin cannot be deleted
     [Tags]  C699088 Update/Delete Role Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  VALIDATE THAT ROLE ZEBRA SYSTEM ADMIN CANNOT BE DELETED