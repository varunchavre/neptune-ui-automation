*** Settings ***

# Library         Selenium2Library
Library         ExtendedSelenium2Library
Library         OperatingSystem
Library         String
Library         Dialogs
Resource        ${EXECDIR}/Res/Web/GenericFunction.robot
Resource        ${EXECDIR}/Res/Web/Create_and_View_Role_Permission_Resource.robot
Variables       ${EXECDIR}/VariableFiles/Web/Config.yaml
Suite Setup     LAUNCH BROWSER
Suite Teardown  Close Browser
Metadata    Version        0.9


*** Test Cases ***

Validate that search tool works correctly
     [Tags]  C697398  Create & view Role Permission
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  SEARCH TOOL WORKING VALIDATION

Portal Roles section displayed correctly
    [Tags]  C697394  Create & view Role Permission
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  PORTAL ROLES SECTION DISPLAY VALIDATION

Create Portal Role with any permission/permissions
    [Tags]  C697314  Create & view Role Permission
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATE ROLE WITH ANY PERMISSION

Permissions can be selected/unselected
    [Tags]  C709558  Create & view Role Permission
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATE ROLE WITH ANY PERMISSION

Validate mandatory fields creating a new role permission
    [Tags]  C697395  Create & view Role Permission
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  MANDATORY FIELDS FOR NEW ROLE PERMISSION VALIDATION

Create a new role permission with a duplicated name
    [Tags]  C697396  Create & view Role Permission
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATE ROLE WITH A DUPLICATED NAME

Validate Portal Role name value
    [Tags]  C709565  Create & view Role Permission
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  PORTAL ROLE NAME VALUE VALIDATION

Validate that pagination works correctly
    [Tags]  C697397  Create & view Role Permission
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  PAGINATION WORKING VALIDATION

Create Portal Role with only required fields
    [Tags]  C709556  Create & view Role Permission
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  ROLE CRATION WITH REQUIRED FIELDS

Create Portal Role gets displayed in Create Portal User window
    [Tags]  C709570  Create & view Role Permission
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  PORTAL ROLE VALIDATION IN CREATE PORTAL USER

Create Portal Role with all fields
    [Tags]  C709559  Create & view Role Permission
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATE PORTAL ROLE VERIFICATION

Create Portal Role Window Displayed
    [Tags]  C709553  Create & view Role Permission
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATE PORTAL ROLE WINDOW DISPLAYED

Create Portal Role Window closed without changes
    [Tags]  C709554  Create & view Role Permission
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATE PORTAL ROLE WINDOW CLOSED WITHOUT CHANGES

Validate Permissions section displayed correctly
    [Tags]  C709557  Create & view Role Permission
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  PERMISSIOS SECTION VALIDATION

Validate that sort functionality works correctly
    [Tags]  C697401  Create & view Role Permission
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  SORT FUNCTIONALITY VALIDATION