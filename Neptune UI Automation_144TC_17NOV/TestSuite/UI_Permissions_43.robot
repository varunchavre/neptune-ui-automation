*** Settings ***
#Section to import libraries and other dependent modules
Library         ExtendedSelenium2Library
Library         OperatingSystem
Library         String
Library         Dialogs
Resource        ${EXECDIR}/Res/Web/GenericFunction.robot
Resource        ${EXECDIR}/Res/Web/UI_Permissions_Resource.robot
Variables       ${EXECDIR}/VariableFiles/Web/Config.yaml
Suite Setup     SUITE SETUP
Suite Teardown  Close Browser
Metadata    Version        0.9

*** Test Cases ***

Portal User with 'ViewPortalUser' permission only: view
    [Tags]  C699178  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  VIEWPORTALUSER

Portal User with 'EditPortalUser' permission: view and edit
    [Tags]  C699179  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  EDITPORTALUSER

Portal User with 'CreatePortalUser' permission: view and create
    [Tags]  C699180  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATEPORTALUSER

Portal User with 'DeletePortalUser' permission: view and delete
    [Tags]  C699181  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  DELETEPORTALUSER

Portal User with 'UnlockPortalUser' permission: view and unlock
    [Tags]  C709618  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  UNLOCKPORTALUSER

Portal User with 'ChangePortalUserPassword' permission: view and change other user passowrd
    [Tags]  C709619  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CHANGEPORTALUSERPASSWORD

Portal User with 'ViewDevice' permission: view
    [Tags]  C709627  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  VIEWDEVICE

Portal User with 'DeleteDevice' permission: view and delete
    [Tags]  C709628  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  DELETEDEVICE

Portal User with 'ViewConfigProfiles' permission: view
    [Tags]  C709629  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  VIEWCONFIGPROFILES

Portal User with 'CreateConfigProfiles' permission: view and create
    [Tags]  C709630  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATECONFIGPROFILES

Portal User with 'EditConfigProfiles' permission: view and edit
    [Tags]  C709632  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  EDITCONFIGPROFILES

Portal User with 'DeleteConfigProfiles' permission: view and delete
    [Tags]  C709633  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  DELETECONFIGPROFILES

Portal User with 'ViewPortalRole' permission: view
    [Tags]  C709634  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  VIEWPORTALROLE

Portal User with 'CreatePortalRole' permission: view and create
    [Tags]  C709635  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATEPORTALROLE

Portal User with 'EditPortalRole' permission: view and edit
    [Tags]  C709636  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  EDITPORTALROLE

Portal User with 'DeletePortalRole' permission: view and delete
    [Tags]  C709637  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  DELETEPORTALROLE

Portal User with 'ViewUserAttributes' permission: view
    [Tags]  C709638  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  VIEWUSERATTRIBUTES

Portal User with 'CreateUserAttributes' permission: view and create
    [Tags]  C709639  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATEUSERATTRIBUTES

Portal User with 'EditUserAttributes' permission: view and edit
    [Tags]  C709640  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  EDITUSERATTRIBUTES

Portal User with 'ViewExtension' permission: view
    [Tags]  C709642  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  VIEWEXTENSION

Portal User with 'BulkImportExtensions' permission: view and import
    [Tags]  C709643  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  BULKIMPORTEXTENSION

Portal User with 'ViewDeviceUsers' permission: view
    [Tags]  C709644  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  VIEWDEVICEUSERS

Portal User with 'CreateDeviceUsers' permission: view and create
    [Tags]  C709645  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATEDEVICEUSERS

Portal User with 'EditDeviceUsers' permission: view and edit
    [Tags]  C709646  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  EDITDEVICEUSERS

Portal User with 'DeleteDeviceUsers' permission: view and delete
    [Tags]  C709647  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  DELETEDEVICEUSERS

Portal User with 'BulkImportUsers' permission: view and import
    [Tags]  C709648  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  BULKIMPORTUSERS

Portal User with 'ViewApplicationDefinitions' permission: view
    [Tags]  C709661  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  VIEWAPPLICATIONDEFINITIONS
    [Teardown]  Run Keyword If Test Failed  TEST TEARDOWN

Portal User with 'CreateApplicationDefinitions' permission: view and create
    [Tags]  C709662  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATEAPPLICATIONDEFINITIONS
    [Teardown]  Run Keyword If Test Failed  TEST TEARDOWN

Portal User with 'EditApplicationDefinitions' permission: view and edit
    [Tags]  C709663  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  EDITAPPLICATIONDEFINITIONS

Portal User with 'DeleteApplicationDefinitions' permission: view and delete
    [Tags]  C709664  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  DELETEAPPLICATIONDEFINITIONS

Portal User with 'ViewRules' permission: view
    [Tags]  C709665  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  VIEWRULES

Portal User with 'CreateRules' permission: view and create
    [Tags]  C709666  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATERULES

Portal User with 'EditRules' permission: view and edit
    [Tags]  C709667  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  EDITRULES

Portal User with 'DeleteRules' permission: view and delete
    [Tags]  C709668  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  DELETERULES

Portal User with 'PublishRules' permission: view and publish
    [Tags]  C709669  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  PUBLISHRULES

Portal User with 'ViewReportTemplate' permission: view
    [Tags]  C709670  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  VIEWREPORTTEMPLATE


Portal User with 'CreateReportTemplate' permission: view and create
    [Tags]  C709671  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CREATEREPORTTEMPLATE

Portal User with 'EditReportTemplate' permission: view and edit
    [Tags]  C709672  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  EDITREPORTTEMPLATE

Portal User with 'DeleteReportTemplate' permission: view and delete
    [Tags]  C709673  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  DELETEREPORTTEMPLATE

Portal User with 'GenerateReport' permission: view report template and generate report
    [Tags]  C709674  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  GENERATEREPORT

Portal User with 'ExportReport' permission: view report template and export report
    [Tags]  C709675  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  EXPORTREPORT

Portal User with 'ViewLicense' permission: view
    [Tags]  C709676  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  VIEWLICENSE

Portal User with 'DeleteUserAttributes' permission: view and delete
    [Tags]  C709641  UI Permissions
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  DELETEUSERATTRIBUTES






# Portal User with 'ViewJobs' permission: view
#     [Tags]  C709649  UI Permissions
#     : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
#     \   Log To Console  \nIteration No.: ${ITERATION}
#     \   Run Keyword and Continue on Failure  VIEWJOBS

# Portal User with 'CreateJobs' permission: view and create
#     [Tags]  C709650  UI Permissions
#     : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
#     \   Log To Console  \nIteration No.: ${ITERATION}
#     \   Run Keyword and Continue on Failure  CREATEJOBS

# Portal User with 'EditJobs' permission: view and edit
#     [Tags]  C709651  UI Permissions
#     : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
#     \   Log To Console  \nIteration No.: ${ITERATION}
#     \   Run Keyword and Continue on Failure  EDITJOBS

# Portal User with 'RunJobs' permission: view and run
#     [Tags]  C709652  UI Permissions
#     : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
#     \   Log To Console  \nIteration No.: ${ITERATION}
#     \   Run Keyword and Continue on Failure  RUNJOBS

# Portal User with 'DeleteJobs' permission: view and delete
#     [Tags]  C709653  UI Permissions
#     : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
#     \   Log To Console  \nIteration No.: ${ITERATION}
#     \   Run Keyword and Continue on Failure  DELETEJOBS

# Portal User with 'ViewJobHistory' permission: view job and view job history
#     [Tags]  C709654  UI Permissions
#     : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
#     \   Log To Console  \nIteration No.: ${ITERATION}
#     \   Run Keyword and Continue on Failure  VIEWJOBHISTORY

# Portal User with 'ViewJobHistoryDetails' permission: view job, job history and history details
#     [Tags]  C709655  UI Permissions
#     : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
#     \   Log To Console  \nIteration No.: ${ITERATION}
#     \   Run Keyword and Continue on Failure  VIEWJOBHISTORYDETAILS

# Portal User with 'ViewNotifications' permission: view jobs and jobs notifications
#     [Tags]  C709656  UI Permissions
#     : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
#     \   Log To Console  \nIteration No.: ${ITERATION}
#     \   Run Keyword and Continue on Failure  VIEWNOTIFICATIONS

# Portal User with 'CreateNotifications' permission: view and create
#     [Tags]  C709657  UI Permissions
#     : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
#     \   Log To Console  \nIteration No.: ${ITERATION}
#     \   Run Keyword and Continue on Failure  CREATENOTIFICATIONS

# Portal User with 'EditNotifications' permission: view and edit
#     [Tags]  C709659  UI Permissions
#     : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
#     \   Log To Console  \nIteration No.: ${ITERATION}
#     \   Run Keyword and Continue on Failure  EDITNOTIFICATIONS

# Portal User with 'DeleteNotifications' permission: view and delete
#     [Tags]  C709660  UI Permissions
#     : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
#     \   Log To Console  \nIteration No.: ${ITERATION}
#     \   Run Keyword and Continue on Failure  DELETENOTIFICATIONS


