*** Settings ***
# Library         Selenium2Library
Library         ExtendedSelenium2Library
Library         String
Library         Dialogs
Library         DateTime
Library         BuiltIn
Library         Collections
Resource        ${EXECDIR}/Res/Web/GenericFunction.robot
Variables       ${EXECDIR}/VariableFiles/Web/Config.yaml


*** Variables ***

${portal_users_tab}                               xpath=(//mat-card[@class="mat-card"])[6]
${portal_user_create_user}                        //a[@class="link-btn ng-star-inserted"]
${portal_user_enter_username}                     //input[@ng-reflect-name="username"]
${portal_user_enter_email}                        //input[@ng-reflect-name="email"]
${portal_user_enter_password}                     //input[@ng-reflect-name="password"]
${portal_user_enter_confirm_password}             //input[@ng-reflect-name="confirmPassword"]
${portal_user_search_for_role}                    //input[@ng-reflect-name="search"]
${portal_user_user_role_field}                    //div[@class="custom-selection-list"]
${portal_user_cancel_createuser}                  //button[@class="mat-button mat-raised-button"]
${portal_user_create_the_user}                    //button[@class="mat-raised-button mat-primary"]
${portal_user_search_option}                      //select[@placeholder="Search Column"]
${portal_user_search}                             //input[@ng-reflect-name="query"]
${portal_user_search_bar}                         //input[@ng-reflect-name="query"]
${portal_user_pagination_bar}                     //mat-paginator[@class="mat-paginator"]
${portal_user_checkbox1}                          xpath=(//div[@class="mat-checkbox-inner-container mat-checkbox-inner-container-no-side-margin"])[2]
${portal_user_checkbox2}                          xpath=(//div[@class="mat-checkbox-inner-container mat-checkbox-inner-container-no-side-margin"])[3]
${portal_user_delete_selected_users}              //a[@class='link-btn ng-star-inserted']//i[@class='fa fa-trash']
${portal_user_delete_icon}                        //i[@class="fa fa-trash"]
${portal_user_edit_icon}                          //i[@class="fa fa-pencil"]
${portal_user_lock_icon}                          //i[@class="fa fa-lock"]
${portal_user_locked}                             //mat-cell[@class="mat-cell cdk-column-locked mat-column-locked ng-star-inserted"]
${portal_user_table_username}                     //mat-cell[@class="mat-cell cdk-column-username mat-column-username ng-star-inserted"]
${portal_user_table_username2}                    xpath=(//mat-cell[@class="mat-cell cdk-column-username mat-column-username ng-star-inserted"])[2]
${portal_user_created_on_time}                    //mat-cell[@class="mat-cell cdk-column-createdOnTime mat-column-createdOnTime ng-star-inserted"]
${portal_user_last_updated_time}                  //mat-cell[@class="mat-cell cdk-column-lastUpdatedTime mat-column-lastUpdatedTime ng-star-inserted"]
${portal_user_status_notification}                //div[@class="notification-title"]
${portal_user_delete_user_yes}                    //button[@class="mat-raised-button mat-primary ng-star-inserted cdk-focused cdk-program-focused"]
${portal_user_password_error_message}             //div[@ngxerrors='password']//div[@ngxerror='pattern']
${portal_user_table}                              //div[@class='row min-height-500 middle-xs ng-star-inserted']
${portal_user_edit_username}                      //input[@formcontrolname="username"]
${portal_user_edit_email}                         //input[@formcontrolname="email"]
${portal_user_edit_user_roles_list}               //mat-selection-list[@formcontrolname="permissionLevels"]
${portal_user_incorrect_username_error_message}   //div[@ngxerrors='username']//div[@ngxerror='pattern']
${portal_user_lock_button}                        //button[@class="mat-button mat-raised-button ng-star-inserted"]
${portal_user_cancel_button}                      //button[@class="mat-button mat-raised-button"]
${portal_user_update_button}                      //button[@class="mat-raised-button mat-primary"]
${portal_user_toggle_button1}                     //div[@class="mat-list-text"]
${portal_user_toggle_button2}                     xpath=(//div[@class="mat-list-text"])[2]
${change_password_save_button}                    //button[@type='submit']
${change_password_enter_current_password}         //input[@ng-reflect-name="currentPassword"]
${change_password_enter_new_password}             //input[@ng-reflect-name="newPassword"]
${change_password_enter_new_confirm_password}     //input[@ng-reflect-name="newPasswordConfirm"]
${system_management_breadcrumb}                   //a[@href="/system-management"]
${system_management_dashboard_title}              //h1[@class='padding-top-10 padding-left-10']
${reset_the_password}                             //button[@class="mat-raised-button mat-primary"]
${portal_user_pwdreset_manual_reset}              xpath=(//div[@class="mat-tab-label-content"])
${portal_user_pwdreset_email_link}                xpath=(//div[@class="mat-tab-label-content"])[2]
${portal_user_username_error_message}             //div[@ng-reflect-control-name="username"]
${portal_user_max_length_username_error_message}  //div[@ng-reflect-control-name="username"]
${portal_users_username_button}                   //button[contains(text(),'Portal User Name')]
${role_element1}                                  xpath=(//mat-cell[@class="mat-cell cdk-column-username mat-column-username ng-star-inserted"])
${role_element2}                                  xpath=(//mat-cell[@class="mat-cell cdk-column-username mat-column-username ng-star-inserted"])[2]
${role_element3}                                  xpath=(//mat-cell[@class="mat-cell cdk-column-username mat-column-username ng-star-inserted"])[3]
${role_element4}                                  xpath=(//mat-cell[@class="mat-cell cdk-column-username mat-column-username ng-star-inserted"])[4]
${role_element5}                                  xpath=(//mat-cell[@class="mat-cell cdk-column-username mat-column-username ng-star-inserted"])[5]
${role_element6}                                  xpath=(//mat-cell[@class="mat-cell cdk-column-username mat-column-username ng-star-inserted"])[6]
${role_element7}                                  xpath=(//mat-cell[@class="mat-cell cdk-column-username mat-column-username ng-star-inserted"])[7]
${role_element8}                                  xpath=(//mat-cell[@class="mat-cell cdk-column-username mat-column-username ng-star-inserted"])[8]
${role_element9}                                  xpath=(//mat-cell[@class="mat-cell cdk-column-username mat-column-username ng-star-inserted"])[9]
${role_element10}                                 xpath=(//mat-cell[@class="mat-cell cdk-column-username mat-column-username ng-star-inserted"])[10]
${change_password_error}                          //div[@ng-reflect-ngx-error="pattern"]
${portal_user_searched_role}                      xpath=(//span[@class="search-highlight"])

${portal_user_lock/unlock_user_button}             //button[@class="mat-button mat-raised-button ng-star-inserted"]
${portal_user_lock/unlock_user_yes}               //button[@class="mat-raised-button mat-primary ng-star-inserted cdk-focused cdk-program-focused"]
${portal_user_edit_portal_user_cancel_button}     //button[@class="mat-button mat-raised-button"]


${viewportalrole}                           Permission to view portal roles
${portal_roles_01}                          Portal Roles
${portal_user_name}                         Portal User Name
${locked}                                   Locked
${createdontime}                            CreatedOnTime
${last_updated_time}                        Last Updated Time
${admin}                                    admin
${portal_user_password}                     Newuser@123
${view_only}                                View Only
${success}                                  Success
${delete_success}                           Delete Success
${portal_user_add_fail}                     Portal User Add Fail
${zebra_system_admin}                       Zebra System Admin
${show_all}                                 Show All
${delete_portaluser}                        Delete PortalUser
${delete_Portal_users}                      Delete Portal Users
${create_Portal_user}                       Create Portal User
${reset_password}                           Reset Password
${edit_portal_user_text}                    Edit Portal User
${dashboard}                                Dashboard
${system_management_text}                   System Management



*** Keywords ***

VALIDATE ALL PORTAL USERS TABLE ATTRIBUTES
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    sleep  1.0
    Page Should Contain  ${portal_user_name}
    Page Should Contain  ${locked}
    Page Should Contain  ${createdontime}
    Page Should Contain  ${last_updated_time}
    Page Should Contain Element  ${portal_user_pagination_bar}
    Page Should Contain Element  ${portal_user_search_bar}
    Page Should Contain Element  ${portal_user_create_user}
    Log to console  all portal users table attributes are validated
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


VALIDATE SYSTEM ADMIN ALL USER PORTAL USERS TABLE
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Wait Until Element Is Enabled  ${portal_user_search}  ${TIMEOUT_20}
    #Sleep  ${SLEEP}
    Input Text  ${portal_user_search}  ${admin}
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    sleep  ${SLEEP}
    Element Text Should Be  ${portal_user_table_username}  ${admin}
    Page Should Contain Element  ${portal_user_locked}   ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_created_on_time}    ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_last_updated_time}   ${TIMEOUT_20}
    Log to console  system admin in all portal users table are validated
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


VALIDATE CREATE PORTAL USER FIELDS
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    Wait Until Page Contains Element  ${portal_user_create_user}  ${TIMEOUT_20}
    Click Link  ${portal_user_create_user}
    Wait Until Page Contains  ${create_Portal_user}  ${TIMEOUT_20}
    Page Should Contain  UserName
    Element Should Be Enabled  ${portal_user_enter_username}
    Page Should Contain  Email
    Element Should Be Enabled  ${portal_user_enter_email}
    Page Should Contain  Password
    Element Should Be Enabled  ${portal_user_enter_password}
    Page Should Contain  Confirm Password
    Element Should Be Enabled  ${portal_user_enter_confirm_password}
    Page Should Contain  User Roles
    Element Should Be Enabled  ${portal_user_search_for_role}
    Element Should Be Visible  ${portal_user_user_role_field}
    Element Should Be Enabled  ${portal_user_cancel_createuser}
    Element Should Be Disabled  ${portal_user_create_the_user}
    Log to console  create portal user fields are validated
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CREATE PORTAL USER WITH ANY ROLE
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${portal_user_password}
    CREATE PORTAL USER SELECT ROLES  ${view_only}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${success}
    ${username}  Set Variable  ${portal_username}
    Log to console  created new admin in portal user with single role
    ${RESULT}  Set Variable  PASS
    Run Keyword And Ignore Error  DELETE USER  ${username}
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CREATE PORTAL USER WITH MANDATORY FIELDS
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${portal_user_password}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${success}
    ${username}  Set Variable  ${portal_username}
    Log to console  created new admin in portal user with mandatory fields
    ${RESULT}  Set Variable  PASS
    Run Keyword And Ignore Error  DELETE USER  ${username}
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CREATE PORTAL USER WITH MISSING MANDATORY FIELDS
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${EMPTY}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Element Should Be Disabled  ${portal_user_create_the_user}
    Log to console  could not create new admin with missing mandatory fields
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CREATE NEW PORTAL USER WITH ANY ROLE AND VALIDATE
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${portal_user_password}
    CREATE PORTAL USER SELECT ROLES  ${view_only}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${success}
    ${username}  Set Variable  ${portal_username}
    Wait Until Page Contains  ${show_all}  ${TIMEOUT_20}
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text  ${portal_user_search}  ${portal_username}
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Element Text Should Be  ${portal_user_table_username}  ${portal_username}
    Page Should Contain Element  ${portal_user_checkbox1}   ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_edit_icon}   ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_delete_icon}   ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_lock_icon}   ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_created_on_time}    ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_last_updated_time}   ${TIMEOUT_20}
    Log to console  created new admin in portal user with any role and validated
    ${RESULT}  Set Variable  PASS
    Run Keyword And Ignore Error  DELETE USER  ${username}
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CREATE PORTAL USER WITH EMPTY PASSWORD
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${EMPTY}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Element Should Be Disabled  ${portal_user_create_the_user}
    Input Text  ${portal_user_enter_password}  ${portal_user_password}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Element Should Be Disabled  ${portal_user_create_the_user}
    Log to console  could not create new admin due to empty password
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CREATE PORTAL USER WITH ONLY SPACES IN THE PASSWORD
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${SPACE}
    Wait Until Element Is Visible  ${portal_user_password_error_message}
    Log to console  password Should not contain only blank spaces
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CREATE PORTAL USER WITH INCORRECT PASSWORD
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  New${SPACE}${SPACE}
    Wait Until Element Is Visible  ${portal_user_password_error_message}
    Log to console  could not create new admin due to incorrect password (with blank spaces)
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CREATE PORTAL USER WITH REPEATED USERNAME
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    ${portal_username}  Get Text  ${portal_user_table_username}
    CREATE PORTAL USER INPUTS  ${portal_username}  ${portal_user_password}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${portal_user_add_fail}
    Log to console  cannot create new admin in portal user due to repeated username
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CREATE PORTAL USER WITH INCORRECT USERNAME
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${portal_user_password}  ${portal_user_password}
    Wait Until Element Is Visible  ${portal_user_username_error_message}
    Log to console  cannot create new admin in portal user due to incorrect username
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CREATE PORTAL USER WITH MORE THAN 255 CHARACTERS USERNAME
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    ${251_char_string}  Generate Random String  251  [NUMBERS]
    CREATE PORTAL USER INPUTS  User_${251_char_string}  ${portal_user_password}
    Wait Until Element Is Visible  ${portal_user_max_length_username_error_message}
    Log to console  cannot create new admin in portal user due to username exceeds max length of 255 characters
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


DELETE ANOTHER ADMIN USING TRASH ICON
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${portal_user_password}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${success}
    #Sleep  ${SLEEP}
    Wait Until Page Contains Element  ${portal_user_delete_icon}  ${TIMEOUT_20}
    ${portal_username}  Get Text  ${portal_user_table_username}
    Click Element  ${portal_user_delete_icon}
    Wait Until Page Contains  ${delete_portaluser}  ${TIMEOUT_20}
    Click Element  ${portal_user_delete_user_yes}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Wait Until Page Contains  ${delete_success}  ${TIMEOUT_20}
    #Element Text Should Be  ${portal_user_status_notification}  ${delete_success}
    Wait Until Page Contains  ${show_all}  ${TIMEOUT_20}
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text  ${portal_user_search}  ${portal_username}
    sleep  ${SLEEP}
    Element Should Not Contain  ${portal_user_table}  ${portal_username}
    Log to console  deleted another admin using trash icon
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


DELETE ANOTHER ADMIN USING CHECK BOX
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${portal_user_password}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${success}
    ${username}  Set Variable  ${portal_username}
    #Reload Page
    Wait Until Element is Visible  ${portal_user_checkbox1}  ${TIMEOUT_20}
    ${portal_username}  Get Text  ${portal_user_table_username}
    Sleep  1.0
    Click Element  ${portal_user_checkbox1}
    Click Element  ${portal_user_delete_selected_users}
    Wait Until Page Contains  ${delete_portal_users}  ${TIMEOUT_20}
    Click Element  ${portal_user_delete_user_yes}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Wait Until Page Contains  ${delete_success}  ${TIMEOUT_20}
#    Element Text Should Be  ${portal_user_status_notification}  ${delete_success}
    Wait Until Page Contains  ${show_all}  ${TIMEOUT_20}
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text  ${portal_user_search}  ${username}
    sleep  ${SLEEP}
    Element Should Not Contain  ${portal_user_table}  ${portal_username}
    Log to console  deleted another admin using check box
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


DELETE MULTIPLE ADMIN USING CHECK BOX
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${portal_user_password}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_create_user}  ${TIMEOUT_20}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${success}
    ${username1}  set variable  ${portal_username}
    Wait Until Element is Enabled  ${portal_user_create_user}  ${TIMEOUT_20}
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${portal_user_password}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_create_user}  ${TIMEOUT_20}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${success}
    ${username2}  set variable  ${portal_username}
    Wait Until Element is Visible  ${portal_user_checkbox1}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    ${portal_usernam2}  Get Text  ${portal_user_table_username}
    ${portal_username1}  Get Text  ${portal_user_table_username2}

    Click Element  ${portal_user_checkbox1}
    Click Element  ${portal_user_checkbox2}
    Click Element  ${portal_user_delete_selected_users}
    Wait Until Page Contains  ${delete_portal_users}  ${TIMEOUT_20}
    Click Element  ${portal_user_delete_user_yes}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Wait Until Page Contains  ${delete_success}  ${TIMEOUT_20}
#    Element Text Should Be  ${portal_user_status_notification}  ${delete_success}
    Wait Until Page Contains  ${show_all}  ${TIMEOUT_20}
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text  ${portal_user_search}  ${username1}
    sleep  ${SLEEP}
    Element Should Not Contain  ${portal_user_table}  ${username1}
    Clear Element Text  ${portal_user_search}
    Reload Page
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text  ${portal_user_search}  ${username2}
    Sleep  3.0
    Element Should Not Contain  ${portal_user_table}  ${username2}
    Log to console  deleted multiple admin using using checkbox
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


DELETE SYSTEM ADMIN
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text  ${portal_user_search}  ${admin}
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    sleep  ${SLEEP}
    Element Text Should Be  ${portal_user_table_username}  ${admin}
    Click Element  ${portal_user_checkbox1}
    Click Element  ${portal_user_delete_selected_users}
    Wait Until Page Contains  ${delete_portal_users}  ${TIMEOUT_20}
    Page Should Contain  Cannot delete system users
    Click Element  ${portal_user_delete_user_yes}
    Log to console  cannot delete system admin
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


VALIDATE BREADCRUMBS REDIRECT TO CORRECT PAGE
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO SPECIFIED PAGE FROM DASHBOARD  User Attributes
    Click Link  ${system_management_breadcrumb}
    Wait Until Page Contains Element  ${system_management_breadcrumb}  ${TIMEOUT_20}
    Element Text Should Be  ${system_management_breadcrumb}  ${system_management_text}
    Wait Until Page Contains Element  ${system_management_dashboard_title}  ${TIMEOUT_20}
    Element Text Should Be  ${system_management_dashboard_title}  ${system_management_text}
    Log to console  redirected to specified page according to selection of breadcrumb
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


VALIDATE SYSTEM MANAGEMENT DASHBOARD
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    Click Link  ${system_management_breadcrumb}
    Wait Until Page Contains Element  ${system_management_breadcrumb}  ${TIMEOUT_20}
    Element Text Should Be  ${system_management_breadcrumb}  ${system_management_text}
    Wait Until Page Contains Element  ${system_management_dashboard_title}  ${TIMEOUT_20}
    Element Text Should Be  ${system_management_dashboard_title}  ${system_management_text}
    Log to console  system management dashboard is displayed properly on clicking System Management Breadcrumb
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


VALIDATE EDIT PORTAL USER
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    Click Element  ${portal_user_edit_icon}
    Wait Until Page Contains  ${edit_portal_user_text}  ${TIMEOUT_20}
    Page Should Contain  UserName
    Element Should Be Disabled  ${portal_user_edit_username}
    Page Should Contain  Email
    Element Should Be Enabled  ${portal_user_edit_email}
    Page Should Contain  User Roles
    Page Should Contain Element  ${portal_user_edit_user_roles_list}
    Element Should Be Enabled  ${portal_user_lock_button}
    Element Should Be Enabled  ${portal_user_cancel_button}
    Element Should Be Disabled  ${portal_user_update_button}
    Log to console  edit portal user fields validated
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


EDIT ANOTHER ADMIN IN PORTAL USER
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    Click Element  ${portal_user_edit_icon}
    Wait Until Page Contains  ${edit_portal_user_text}  ${TIMEOUT_20}
    ${no}  Generate Random String  4  [NUMBERS]
    Input Text  ${portal_user_edit_email}  LT${no}@gmail.com
    Click Element  ${portal_user_toggle_button1}
    Click Element  ${portal_user_toggle_button2}
    Click Element  ${portal_user_update_button}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${success}
    Log to console  edited another admin portal user
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


EDIT OWN ADMIN IN PORTAL USER
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${portal_user_password}
    CREATE PORTAL USER SELECT ROLES  ${admin}
    CREATE PORTAL USER SELECT ROLES  ${zebra_system_admin}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${success}
    ${username}  Set Variable  ${portal_username}
    LOGOUT FROM NEPTUNE
    Log  ${portal_username}
    LOGIN TO NEPTUNE  ${portal_username}  ${portal_user_password}
    GO TO PORTAL USERS TAB
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Wait Until Page Contains  ${portal_user_name}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text  ${portal_user_search}  ${portal_username}
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    sleep  ${SLEEP}
    Element Text Should Be  ${portal_user_table_username}  ${portal_username}
    Click Element  ${portal_user_edit_icon}
    Wait Until Page Contains  ${edit_portal_user_text}  ${TIMEOUT_20}
    ${no}  Generate Random String  4  [NUMBERS]
    Clear Element Text  ${portal_user_edit_email}
    Input Text  ${portal_user_edit_email}  LT${no}@gmail.com
    Click Element  ${portal_user_toggle_button1}
    Click Element  ${portal_user_toggle_button2}
    Click Element  ${portal_user_update_button}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${success}
    LOGOUT FROM NEPTUNE
    Log to console  edited own admin portal user
    ${RESULT}  Set Variable  PASS
    Run Keyword And Ignore Error  DELETE USER  ${username}
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


SEARCH ADMIN IN PORTAL USER
    [Arguments]  ${portal_username}=admin
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text  ${portal_user_search}  ${portal_username}
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    sleep  ${SLEEP}
    Element Text Should Be  ${portal_user_table_username}  ${portal_username}
    Log to console  searched admin present in the portal user table
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CHANGE OWN ADMIN PASSWORD WITH EMPTY PASSWORD
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO CHANGE PASSWORD
    Clear Element Text  ${change_password_enter_current_password}
    Input Text  ${change_password_enter_current_password}  ${PSWRD}
    Element should be disabled  ${change_password_save_button}
    Clear Element Text  ${change_password_enter_new_password}
    Input Text  ${change_password_enter_new_password}  Newusers@1234
    Element should be disabled  ${change_password_save_button}
    Clear Element Text  ${change_password_enter_new_password}
    Clear Element Text  ${change_password_enter_new_confirm_password}
    Input Text  ${change_password_enter_new_confirm_password}   Newusers@12345
    Element should be disabled  ${change_password_save_button}
    Log to console  cannot change password with an empty password
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CHANGE OWN ADMIN PASSWORD WITH INCORRECT PASSWORD
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO CHANGE PASSWORD
    CHANGE PASSWORD INPUTS  Newuser1  Newuser1
    Wait until element is visible  ${change_password_error}
    Log to console  cannot change password with an incorrect password
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CHANGE OWN ADMIN PASSWORD WITH OLD PASSWORD
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO CHANGE PASSWORD
    CHANGE PASSWORD INPUTS  ${PSWRD}  ${PSWRD}
    Click Element  ${change_password_save_button}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  Reset Password Fail
    Log to console  cannot change password with an old password
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


RESET PORTAL USER PASSWORD USING MANUAL RESET OPTION
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    Click Element    ${portal_user_lock_icon}
    Wait Until Page Contains  ${reset_password}  ${TIMEOUT_20}
    Click Element    ${portal_user_pwdreset_manual_reset}
    Wait Until Page Contains  New Password  ${TIMEOUT_20}
    ${no}  Generate Random String  4  [NUMBERS]
    Input Text    ${change_password_enter_new_password}  Newuser@${no}
    Input Text    ${portal_user_enter_confirm_password}  Newuser@${no}
    Click Element  ${reset_the_password}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${success}
    Log to console  portal user password reset successfully using manual reset option
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


RESET SYSTEM ADMIN PASSWORD
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text  ${portal_user_search}  ${Login}
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    sleep  ${SLEEP}
    Element Text Should Be  ${portal_user_table_username}  ${Login}
    Page Should Not Contain Element  ${portal_user_lock_icon}
    Log to console  cannot reset password of system admin.
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


DELETE OWN ADMIN
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${portal_user_password}
    CREATE PORTAL USER SELECT ROLES  ${admin}
    CREATE PORTAL USER SELECT ROLES  ${zebra_system_admin}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${success}
    ${username}  Set Variable  ${portal_username}
    LOGOUT FROM NEPTUNE
    Log  ${portal_username}
    LOGIN TO NEPTUNE  ${portal_username}  ${portal_user_password}
    GO TO PORTAL USERS TAB
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text  ${portal_user_search}  ${portal_username}
    Wait Until Page Contains Element  ${portal_user_delete_icon}  ${TIMEOUT_20}
    sleep  ${SLEEP}
    Wait Until Page Contains Element  ${portal_user_checkbox1}  ${TIMEOUT_20}
    Click Element  ${portal_user_checkbox1}
    Click Element  ${portal_user_delete_selected_users}
    Wait Until Page Contains  ${delete_portal_users}  ${TIMEOUT_20}
    Click Element  ${portal_user_delete_user_yes}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Wait Until Page Contains  ${delete_success}  ${TIMEOUT_20}
    #Element Text Should Be  ${portal_user_status_notification}  ${delete_success}
    Wait Until Page Contains  ${show_all}  ${TIMEOUT_20}
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text  ${portal_user_search}  ${portal_username}
    Sleep  ${SLEEP}
    Element Should Not Contain  ${portal_user_table}  ${portal_username}
    Log to console  deleted own admin
    ${RESULT}  Set Variable  PASS
    Run Keyword And Ignore Error  DELETE USER  ${username}
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


RESET OWN PORTAL USER PASSWORD
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}

    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${portal_user_password}
    CREATE PORTAL USER SELECT ROLES  ${admin}
    CREATE PORTAL USER SELECT ROLES  ${zebra_system_admin}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${success}
    ${username}  Set Variable  ${portal_username}
    LOGOUT FROM NEPTUNE
    Log  ${portal_username}
    LOGIN TO NEPTUNE  ${portal_username}  ${portal_user_password}
    GO TO PORTAL USERS TAB
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text  ${portal_user_search}  ${portal_username}
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    sleep  ${SLEEP}
    Element Text Should Be  ${portal_user_table_username}  ${portal_username}
    Page Should Not Contain Element  ${portal_user_lock_icon}
    Log to console  cannot reset password of system admin.
    ${RESULT}  Set Variable  PASS
    Run Keyword And Ignore Error  DELETE USER  ${username}
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


RESET OTHER ADMIN PASSWORD SAME AS CURRENT PASSWORD
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${portal_user_password}
    CREATE PORTAL USER SELECT ROLES
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${success}
    ${username}  Set Variable  ${portal_username}
    Log  ${portal_username}
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text  ${portal_user_search}  ${portal_username}
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    sleep  ${SLEEP}
    Element Text Should Be  ${portal_user_table_username}  ${portal_username}
    Click Element    ${portal_user_lock_icon}
    Wait Until Page Contains  ${reset_password}  ${TIMEOUT_20}
    Click Element    ${portal_user_pwdreset_manual_reset}
    Wait Until Page Contains  New Password  ${TIMEOUT_20}
    Input Text    ${change_password_enter_new_password}  ${portal_user_password}
    Input Text    ${portal_user_enter_confirm_password}  ${portal_user_password}
    Click Element  ${reset_the_password}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  Reset Portal user Password Failed
    Log to console  could not reset another admin password using manual reset option as current password and new password are same
    ${RESULT}  Set Variable  PASS
    Run Keyword And Ignore Error  DELETE USER  ${username}
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CHANGE ANOTHER ADMIN PASSWORD USING EMAIL RESET OPTION
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${portal_user_password}
    CREATE PORTAL USER SELECT ROLES
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${success}
    Log  ${portal_username}
    ${username}  Set Variable  ${portal_username}
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text  ${portal_user_search}  ${portal_username}
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    sleep  ${SLEEP}
    Element Text Should Be  ${portal_user_table_username}  ${portal_username}
    Click Element    ${portal_user_lock_icon}
    Wait Until Page Contains  ${reset_password}  ${TIMEOUT_20}
    Click Element  ${portal_user_pwdreset_email_link}
    Page Should Contain  No email Id associated with the user.
    Element Should Be Disabled  ${reset_the_password}
    Log to console  could not reset another admin password using email reset option as user does not have registered email
    ${RESULT}  Set Variable  PASS
    Run Keyword And Ignore Error  DELETE USER  ${username}
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


VALIDATE USER ROLES PRESENT IN CREATE PORTAL USER
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${portal_roles_01}  ${viewportalrole}
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    Wait Until Page Contains Element  ${portal_user_create_user}  ${TIMEOUT_20}
    Click Link  ${portal_user_create_user}
    Wait Until Page Contains  ${create_Portal_user}  ${TIMEOUT_20}
    Wait Until Page Contains  UserName  ${TIMEOUT_20}
    Input Text  ${portal_user_search_for_role}  Role_${role_name}
    sleep  3.0
    Element Text Should Be  ${portal_user_searched_role}  Role_${role_name}
    Input Text  ${portal_user_search_for_role}  ${zebra_system_admin}
    sleep  3.0
	Page Should Contain  ${zebra_system_admin}
	
#    ${status}  @{stringg}=  Run keyword and ignore error  Split String  ${zebra_system_admin}  separator=${SPACE}
#    ${count}  get length  @{stringg}
#    ${1st}  Get From List  @{stringg}  0
#    ${2nd}  Get From List  @{stringg}  1
#    ${3rd}  Get From List  @{stringg}  2
#    Element Text Should Be  ${portal_user_searched_role}[2]  ${1st}     #Zebra
#    Element Text Should Be  ${portal_user_searched_role}[3]  ${2nd}     #System
#    Element Text Should Be  ${portal_user_searched_role}[4]  ${3rd}     #Admin

    Log to console  Zebra system admin role and created user role present in create portal user window
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CREATE NEW ADMIN WITH ZEBRA SYSTEM ADMIN ROLE
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${portal_user_password}
    CREATE PORTAL USER SELECT ROLES  ${zebra_system_admin}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${success}
    ${username}  Set Variable  ${portal_username}
    Wait Until Page Contains  ${show_all}  ${TIMEOUT_20}
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text  ${portal_user_search}  ${portal_username}
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  5.0
    Element Text Should Be  ${portal_user_table_username}  ${portal_username}
    Page Should Contain Element  ${portal_user_checkbox1}   ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_edit_icon}   ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_delete_icon}   ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_lock_icon}   ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_created_on_time}    ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_last_updated_time}   ${TIMEOUT_20}
    Log to console  created new admin in portal user with zebra system admin role
    ${RESULT}  Set Variable  PASS
    Run Keyword And Ignore Error  DELETE USER  ${username}
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CREATE NEW ADMIN WITH MULTIPLE ROLES
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${portal_roles_01}  ${viewportalrole}
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${portal_user_password}
    CREATE PORTAL USER SELECT ROLES  ${zebra_system_admin}
    CREATE PORTAL USER SELECT ROLES  Role_${role_name}
    CREATE PORTAL USER SELECT ROLES  ${admin}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${success}
    ${username}  Set Variable  ${portal_username}
    Wait Until Page Contains  ${show_all}  ${TIMEOUT_20}
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text  ${portal_user_search}  ${portal_username}
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  5.0
    Element Text Should Be  ${portal_user_table_username}  ${portal_username}
    Page Should Contain Element  ${portal_user_checkbox1}   ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_edit_icon}   ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_delete_icon}   ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_lock_icon}   ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_created_on_time}    ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_last_updated_time}   ${TIMEOUT_20}
    Log to console  created new admin in portal user with zebra system admin role
    ${RESULT}  Set Variable  PASS
    Run Keyword And Ignore Error  DELETE USER  ${username}
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


SORT FUNCTIONALITY VALIDATION FOR ADMIN USER
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    Sleep  1.0
    Wait Until Page Contains Element  ${portal_users_username_button}
    Click Element  ${portal_users_username_button}
    ${a}  Get Text  ${role_element1}
    ${b}  Get Text  ${role_element2}
    ${c}  Get Text  ${role_element3}
    ${d}  Get Text  ${role_element4}
    ${e}  Get Text  ${role_element5}
    ${f}  Get Text  ${role_element6}
    ${g}  Get Text  ${role_element7}
    ${h}  Get Text  ${role_element8}
    ${i}  Get Text  ${role_element9}
    ${k}  Get Text  ${role_element10}
    ${list}  Create list  ${a}  ${b}  ${c}  ${d}  ${e}  ${f}  ${g}  ${h}  ${i}  ${k}
    ${list1}  copy_list  ${list}
    sort list  ${list}
    Log to console  \n${list}
    Log to console  \n${list1}
    ${sort_validation}  Run Keyword And Return Status  Lists Should Be Equal  ${list}  ${list1}
    Log to console  ${sort_validation}
    Run Keyword If  '${sort_validation}'=='True'  Log to console  Sort function is working correctly
    Run Keyword If  '${sort_validation}'=='False'  Log to console  Sort function is not working correctly
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN



RESET LOCKED PORTAL USER PASSWORD
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${portal_user_password}
    CREATE PORTAL USER SELECT ROLES  ${zebra_system_admin}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${success}
    ${username}  Set Variable  ${portal_username}

    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Wait Until Page Contains  ${portal_user_name}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text  ${portal_user_search}  ${portal_username}
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    sleep  ${SLEEP}
    Element Text Should Be  ${portal_user_table_username}  ${portal_username}
    Click Element  ${portal_user_edit_icon}
    Wait Until Page Contains  ${edit_portal_user_text}  ${TIMEOUT_20}
    Click Element  ${portal_user_lock/unlock_user_button}
    Wait Until Page Contains Element  ${portal_user_lock/unlock_user_yes}  ${TIMEOUT_20}
    Click Element  ${portal_user_lock/unlock_user_yes}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${success}
    Click Element  ${portal_user_edit_portal_user_cancel_button}

    Wait Until Page Contains Element  ${portal_user_lock_icon}  ${TIMEOUT_20}
    Input Text  ${portal_user_search}  ${portal_username}
    Wait Until Page Contains Element  ${portal_user_lock_icon}  ${TIMEOUT_20}
    sleep  ${SLEEP}

    Click Element    ${portal_user_lock_icon}
    Wait Until Page Contains  ${reset_password}  ${TIMEOUT_20}
    Click Element    ${portal_user_pwdreset_manual_reset}
    Wait Until Page Contains  New Password  ${TIMEOUT_20}
    ${no}  Generate Random String  4  [NUMBERS]
    Input Text    ${change_password_enter_new_password}  Newuser@${no}
    Input Text    ${portal_user_enter_confirm_password}  Newuser@${no}

    ${new_pwd}  Set Variable  Newuser@${no}

    Click Element  ${reset_the_password}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${success}

    LOGOUT FROM NEPTUNE
    Log  ${portal_username}
    LOGIN TO NEPTUNE  ${portal_username}  ${new_pwd}
    Wait until element is visible  //div[@class="box error text-left ng-star-inserted"]  ${TIMEOUT_20}
    TEST SETUP
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB


    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Wait Until Page Contains  ${portal_user_name}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text  ${portal_user_search}  ${portal_username}
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    sleep  ${SLEEP}
    Element Text Should Be  ${portal_user_table_username}  ${portal_username}
    Click Element  ${portal_user_edit_icon}
    Wait Until Page Contains  ${edit_portal_user_text}  ${TIMEOUT_20}
    Click Element  ${portal_user_lock/unlock_user_button}
    Wait Until Page Contains Element  ${portal_user_lock/unlock_user_yes}  ${TIMEOUT_20}
    Click Element  ${portal_user_lock/unlock_user_yes}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  ${success}
    Click Element  ${portal_user_edit_portal_user_cancel_button}

    LOGOUT FROM NEPTUNE
    Log  ${portal_username}
    LOGIN TO NEPTUNE  ${portal_username}  ${new_pwd}
    Wait Until Page Contains  ${dashboard}  ${TIMEOUT_20}
    Wait Until Page Contains  ${system_management_text}  ${TIMEOUT_20}
#    LOGOUT FROM NEPTUNE
#    TEST SETUP
    Log to console  RESET LOCKED PORTAL USER PASSWORDsss
    ${RESULT}  Set Variable  PASS
    Run Keyword And Ignore Error  DELETE USER  ${username}
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN
