*** Settings ***

Library         ExtendedSelenium2Library
Library         String
Library         Dialogs
Library         DateTime
Variables       ${EXECDIR}/VariableFiles/Web/Config.yaml
Resource        ${EXECDIR}/Res/Web/GenericFunction.robot


*** Variables ***
${zebra_logo}  //img[@alt='Zebra Neptuen']
${user_id_input}  //input[@id='mat-input-0']
${password_input}  //input[@id='mat-input-1']
${forgot_password_link}  //a[@class='block link text-small margin-bottom-15']
${login_button}  //span[@class='mat-button-wrapper']
${terms_conditions_privacy_policy_text}  //div[@class='text-center margin-top-20 login-footer']
${terms_and_conditions}  //a[contains(text(),'terms and condition')]
${privacy_policy}  //a[contains(text(),'privacy policy')]

${search}  //input[@ng-reflect-placeholder="Start typing..."]
${user_name_dashboard}  //a[@class='user-name']
${naviagation_button}  //button[@class='mobile-menu ng-star-inserted']
${system_management_button}  //span[contains(text(),'System Management')]
${portal_users_sm}  //a[contains(text(),'Portal Users')]
${device_users_sm}  //a[contains(text(),'Device Users')]
${portal_roles_sm}  //a[contains(text(),'Portal Roles')]
${user_attributes_sm}  //a[contains(text(),'Users Attributes')]
${users_group_import_sm}  //a[contains(text(),'Users & Group Import')]
${application_definations_sm}  //a[contains(text(),'Application Definitions')]
${version}  //div[@class='app_info']
${license}  //span[contains(text(),'License')]

${no_permission_regular_admin}  no_permission_regular_admin
${lowercase_user_id}  logintest
${bad_credentials}  Bad credentials.

${disabled_login_button}  //button[@ng-reflect-disabled="true"]
*** Keywords ***

LOGIN PAGE VALIDATION
    ${RESULT}  Set Variable  FAIL
    Run Keyword And Ignore Error  LOGOUT FROM NEPTUNE
    Page should contain Element  ${zebra_logo}
    Page should contain Element  ${user_id_input}
    Page should contain Element  ${password_input}
    Page should contain Element  ${forgot_password_link}
    Page should contain Element  ${login_button}
    Page should contain Element  ${terms_conditions_privacy_policy_text}
    Page should contain Element  ${terms_and_conditions}
    Page should contain Element  ${privacy_policy}
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

DASHBOARD VALIDATION SYSTEM ADMIN
    ${RESULT}  Set Variable  FAIL
    Run Keyword And Ignore Error  LOGOUT FROM NEPTUNE
    LOGIN TO NEPTUNE
    Page should contain Element  ${zebra_logo}
    Page should contain Element  ${search}
    Page should contain Element  ${user_name_dashboard}
    Page should contain Element  ${APPLICATION_DEFINITIONS}
    Page should contain Element  ${DEVICE_USERS}
    Page should contain Element  ${JOBS}
    Page should contain Element  ${NOTIFICATIONS}
    Page should contain Element  ${PORTAL_ROLES}
    Page should contain Element  ${PORTAL_USERS}
    Page should contain Element  ${USER_ATTRIBUTES}
    Page should contain Element  ${CONFIGURATION_PROFILES}
    Page should contain Element  ${DEVICES}
    Page should contain Element  ${EXTENSION_MANAGER}
    Page should contain Element  ${REPORTS}
    Page should contain Element  ${RULES}
    Click Element  ${naviagation_button}
    Click Element  ${system_management_button}
    Page should contain Element  ${portal_users_sm}
    Page should contain Element  ${device_users_sm}
    Page should contain Element  ${portal_roles_sm}
    Page should contain Element  ${user_attributes_sm}
    Page should contain Element  ${users_group_import_sm}
    Page should contain Element  ${application_definations_sm}
    Page should contain Element  ${version}
    Page should contain Element  ${license}
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

DASHBOARD VALIDATION REGULAR ADMIN
    ${RESULT}  Set Variable  FAIL
    Run Keyword And Ignore Error  LOGOUT FROM NEPTUNE
    LOGIN TO NEPTUNE  ${TESTUSERNAME}
    Page should contain Element  ${zebra_logo}
    Page should contain Element  ${search}
    Page should contain Element  ${user_name_dashboard}
    Page should contain Element  ${USER_ATTRIBUTES}
    Click Element  ${naviagation_button}
    Click Element  ${system_management_button}
    Page should contain Element  ${user_attributes_sm}
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

DASHBOARD VALIDATION NO PERMISSION ADMIN
    ${RESULT}  Set Variable  FAIL
    Run Keyword And Ignore Error  LOGOUT FROM NEPTUNE
    LOGIN TO NEPTUNE  ${no_permission_regular_admin}
    Page should contain Element  ${zebra_logo}
    Page should contain Element  ${search}
    Page should contain Element  ${user_name_dashboard}
    Page should not contain Element  ${USER_ATTRIBUTES}
    Click Element  ${naviagation_button}
    Page should not contain Element  ${user_attributes_sm}
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

LOGIN UPPERCASE VALIDATION
    ${RESULT}  Set Variable  FAIL
    Run Keyword And Ignore Error  LOGOUT FROM NEPTUNE
    LOGIN TO NEPTUNE  ${lowercase_user_id}
    Wait Until Page Contains  ${bad_credentials}
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

LOGIN WITH INCORRECT CREDENTIALS
    ${RESULT}  Set Variable  FAIL
    Run Keyword And Ignore Error  LOGOUT FROM NEPTUNE
    LOGIN TO NEPTUNE  ${WRONGUSERNAME}
    Wait Until Page Contains  ${bad_credentials}
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

LOGOUT AS AN ADMIN
    ${RESULT}  Set Variable  FAIL
    Run Keyword And Ignore Error  LOGOUT FROM NEPTUNE
    LOGIN TO NEPTUNE
    Builtin.sleep  5
    LOGOUT FROM NEPTUNE
    Wait Until Page Contains Element  ${login_button}
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

LOGOUT AFTER 30 MINUTES
    ${RESULT}  Set Variable  FAIL
    Run Keyword And Ignore Error  LOGOUT FROM NEPTUNE
    LOGIN TO NEPTUNE
    Builtin.sleep  450
    Page should not contain Element  ${login_button}
    Builtin.sleep  450
    Page should not contain Element  ${login_button}
    Builtin.sleep  450
    Page should not contain Element  ${login_button}
    Builtin.sleep  450
    Wait Until Page Contains Element  ${login_button}  ${TIMEOUT_20}
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

LOGIN WITH EMPTY FIELDS
    ${RESULT}  Set Variable  FAIL
    Run Keyword And Ignore Error  LOGOUT FROM NEPTUNE
    Wait Until Page Contains Element  ${disabled_login_button}
    Log to Console  Login button is disabled
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

