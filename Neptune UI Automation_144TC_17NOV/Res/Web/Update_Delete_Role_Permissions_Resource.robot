*** Settings ***
# Library         Selenium2Library
Library         ExtendedSelenium2Library
Library         String
Library         Dialogs
Library         DateTime
Library         BuiltIn
Library         Collections
Resource        ${EXECDIR}/Res/Web/GenericFunction.robot
Variables       ${EXECDIR}/VariableFiles/Web/Config.yaml


*** Variables ***

#
${portal_roles_button}                xpath=(//mat-card[@class="mat-card"])[5]
${create_role_button}                 //a[@class="link-btn ng-star-inserted"]
${new_role_input}                     //input[@ng-reflect-name="name"]
${create_button}                      xpath=(//span[@class="mat-button-wrapper"])[6]
${portal_user_username}               //mat-cell[@class="mat-cell cdk-column-username mat-column-username ng-star-inserted"]
${click_element}                      //div[@class="mat-list-text"]
${dashboard_breadcrumb}               //a[@ng-reflect-router-link='/dashboard']
${portal_role_description}            //textarea[@formcontrolname="description"]
${portal_role_edit}                   //i[@class="fa fa-pencil"]
${portal_role_zebra_admin_view_icon}  //i[@class="fa fa-eye"]
${portal_role_search_option}          //select[@placeholder="Search Column"]
${portal_role_input_name_search}      //input[@name="query"]
${portal_role_table_name}             //mat-cell[@class="mat-cell cdk-column-name mat-column-name ng-star-inserted"]
${portal_role_delete_icon}            //i[@class="fa fa-trash"]
${portal_role_delete_role_yes}        //button[@class="mat-raised-button mat-primary ng-star-inserted cdk-focused cdk-program-focused"]
${status_notification}                //div[@class="notification-title"]
${portal_role_checkbox1}              xpath=(//div[@class="mat-checkbox-inner-container mat-checkbox-inner-container-no-side-margin"])[2]
${portal_role_delete_selected_role}   //div[@class='grid-tools']//a[2]
${portal_role_invalid_rolename_msg}   //div[@ngxerror='pattern']
${portal_role_no_role_selected_msg}   //div[@class='float-err ng-star-inserted']
${portal_role_empty_rolename_msg}     //div[@ngxerrors='name']

${user_attributes1}                      User Attributes
${permission_to_delete_user_attributes}  Permission to delete user attributes
${portal_user_password}                  Newuser@123
${new_role}                              a
${viewportalrole}                        Permission to view portal roles
${portal_roles_01}                       Portal Roles
${deleteportaluser}                      Permission to delete portal roles
${viewportalrole}                        Permission to view portal roles
${portal_roles_01}                       Portal Roles
${createportalrole}                      Permission to create portal roles
${editportalrole}                        Permission to edit portal roles
${portal_user_name}                      Portal User Name
${locked}                                Locked
${createdontime}                         CreatedOnTime
${last_updated_time}                     Last Updated Time
${admin}                                 admin
${view_only}                             View Only
${success}                               Success
${delete_success}                        Delete Success
${portal_user_add_fail}                  Portal User Add Fail
${zebra_system_admin}                    Zebra System Admin
${show_all}                              Show All
${delete_portaluser}                     Delete PortalUser
${delete_Portal_users}                   Delete Portal Users
${create_Portal_user}                    Create Portal User
${reset_password}                        Reset Password
${edit_portal_user}                      Edit Portal User
${dashboard}                             Dashboard
${system_management}                     System Management



*** Keywords ***


###Generic Function Keyword###
CREATE ROLE WITH ANY PERMISSION GENERIC
    [Arguments]  ${new_role_arg}=${new_role}  ${user_attributes_arg}=${user_attributes1}  ${permission_to_delete_user_attributes_arg}=${permission_to_delete_user_attributes}  ${description}=${EMPTY}
    Wait Until Page Contains Element  ${portal_role_description}  ${TIMEOUT_20}
    Input Text  ${portal_role_description}  ${description}
    Wait Until Page Contains Element    ${new_role_input}  ${TIMEOUT_20}
    Input Text    ${new_role_input}  ${new_role_arg}
    ${new_role_name}  set variable  ${new_role_arg}
    Set Global Variable  ${new_role_name}
    Sleep  1.0
    Click Element  //mat-panel-title[contains(text(),'${user_attributes_arg}')]
    ${role_attribute}  set variable  //mat-panel-title[contains(text(),'${user_attributes_arg}')]
    Set Global Variable  ${role_attribute}
    Wait Until Page Contains Element  xpath=//*[contains(text(), "${permission_to_delete_user_attributes_arg}")]  ${TIMEOUT_20}
    Scroll element into view  xpath=//*[contains(text(), "${permission_to_delete_user_attributes_arg}")]
    Sleep  ${SLEEP}
    Click Element    xpath=//*[contains(text(), "${permission_to_delete_user_attributes_arg}")]
    Click Element  //mat-panel-title[contains(text(),'${user_attributes_arg}')]




###normal keyword###
UPDATE PORTAL ROLENAME DESCRIPTION PERMISSION
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element  ${PORTAL_ROLES}  ${TIMEOUT_20}
    click element  ${PORTAL_ROLES}
    Wait Until Page Contains Element  ${create_role_button}  ${TIMEOUT_20}
    click element  ${create_role_button}
    ${role_name}  Generate Random String  5  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GENERIC  Role_${role_name}  ${portal_roles_01}  ${viewportalrole}
    Click Element    ${create_button}
    Element Text Should Be  ${status_notification}  Success
    Wait Until Page Contains  Show All  ${TIMEOUT_20}
    Select From List By Value    ${portal_role_search_option}    name
    Wait Until Page Contains Element  ${portal_role_edit}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text    ${portal_role_input_name_search}    ${new_role_name}
    Wait Until Page Contains Element  ${portal_role_edit}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Element Text Should Be  ${portal_role_table_name}  ${new_role_name}
    Click Element  ${portal_role_edit}
    ${role_name}  Generate Random String  5  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GENERIC  Role_${role_name}  ${portal_roles_01}  ${editportalrole}  DESC
    ${portal_role_rolename}  Set Variable  ${new_role_name}
    Click Element    ${create_button}
    Element Text Should Be  ${status_notification}  Success
    Log to console  portal rolename, description,permission updated successfully
    ${RESULT}  Set Variable  PASS
    Run Keyword And Ignore Error  DELETE ROLE  ${portal_role_rolename}
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


UPDATE PORTAL ROLE WITH EMPTY ROLE NAME
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element  ${PORTAL_ROLES}  ${TIMEOUT_20}
    click element  ${PORTAL_ROLES}
    Wait Until Page Contains Element  ${create_role_button}  ${TIMEOUT_20}
    click element  ${create_role_button}
    ${role_name}  Generate Random String  5  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GENERIC  Role_${role_name}  ${portal_roles_01}  ${viewportalrole}
    ${portal_role_rolename}  Set Variable  ${new_role_name}
    Click Element    ${create_button}
    Element Text Should Be  ${status_notification}  Success
    Wait Until Page Contains  Show All  ${TIMEOUT_20}
    Select From List By Value    ${portal_role_search_option}    name
    Wait Until Page Contains Element  ${portal_role_edit}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text    ${portal_role_input_name_search}    ${new_role_name}
    Wait Until Page Contains Element  ${portal_role_edit}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Element Text Should Be  ${portal_role_table_name}  ${new_role_name}
    Click Element  ${portal_role_edit}
    CREATE ROLE WITH ANY PERMISSION GENERIC  ${EMPTY}  ${portal_roles_01}  ${viewportalrole}
    Scroll element into view  ${new_role_input}
    Wait Until Element is Visible  ${portal_role_empty_rolename_msg}  ${TIMEOUT_20}
    Log to console  cannot update portal role with empty role name
    ${RESULT}  Set Variable  PASS
    Run Keyword And Ignore Error  DELETE ROLE  ${portal_role_rolename}
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


UPDATE PORTAL ROLE WITH NO ROLE
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element  ${PORTAL_ROLES}  ${TIMEOUT_20}
    click element  ${PORTAL_ROLES}
    Wait Until Page Contains Element  ${create_role_button}  ${TIMEOUT_20}
    click element  ${create_role_button}
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GENERIC  Role_${role_name}  ${portal_roles_01}  ${viewportalrole}
    ${portal_role_rolename}  Set Variable  ${new_role_name}
    Click Element    ${create_button}
    Wait Until Page contains element  ${status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${status_notification}  Success
    Wait Until Page Contains  Show All  ${TIMEOUT_20}
    Select From List By Value    ${portal_role_search_option}    name
    Wait Until Page Contains Element  ${portal_role_edit}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text    ${portal_role_input_name_search}    ${new_role_name}
    Wait Until Page Contains Element  ${portal_role_edit}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Element Text Should Be  ${portal_role_table_name}  ${new_role_name}
    Click Element  ${portal_role_edit}
    CREATE ROLE WITH ANY PERMISSION GENERIC  ${EMPTY}  ${portal_roles_01}  ${viewportalrole}
    Click Element  ${role_attribute}
#    Scroll element into view  ${new_role_input}
    Wait Until Element is Visible  ${portal_role_no_role_selected_msg}  ${TIMEOUT_20}
    Log to console  cannot update portal role with empty role
    ${RESULT}  Set Variable  PASS
    Run Keyword And Ignore Error  DELETE ROLE  ${portal_role_rolename}
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


UPDATE PORTAL ROLE WITH EXISTING ROLE NAME
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element  ${PORTAL_ROLES}  ${TIMEOUT_20}
    click element  ${PORTAL_ROLES}
    Wait Until Page Contains Element  ${create_role_button}  ${TIMEOUT_20}
    ${existing_rolename}  Get Text  ${portal_role_table_name}
    click element  ${create_role_button}
    ${role_name}  Generate Random String  5  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GENERIC  Role_${role_name}  ${portal_roles_01}  ${viewportalrole}
    ${portal_role_rolename}  Set Variable  ${new_role_name}
    Click Element    ${create_button}
    Wait Until Page contains element  ${status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${status_notification}  Success
    Wait Until Page Contains  Show All  ${TIMEOUT_20}
    Select From List By Value    ${portal_role_search_option}    name
    Wait Until Page Contains Element  ${portal_role_edit}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text    ${portal_role_input_name_search}  ${new_role_name}
    Wait Until Page Contains Element  ${portal_role_edit}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Element Text Should Be  ${portal_role_table_name}  ${new_role_name}
    Click Element  ${portal_role_edit}
    CREATE ROLE WITH ANY PERMISSION GENERIC  ${existing_rolename}  ${portal_roles_01}  ${deleteportaluser}
    Click Element    ${create_button}
    Wait Until Page contains element  ${status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${status_notification}  Update Portal Role Error
    Log to console  cannot update portal role with existing role name
    ${RESULT}  Set Variable  PASS
    Run Keyword And Ignore Error  DELETE ROLE  ${portal_role_rolename}
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


UPDATE PORTAL ROLE WITH INVALID ROLE NAME
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element  ${PORTAL_ROLES}  ${TIMEOUT_20}
    click element  ${PORTAL_ROLES}
    Wait Until Page Contains Element  ${create_role_button}  ${TIMEOUT_20}
    click element  ${create_role_button}
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GENERIC  Role_${role_name}  ${portal_roles_01}  ${viewportalrole}
    ${portal_role_rolename}  Set Variable  ${new_role_name}
    Click Element    ${create_button}
    Element Text Should Be  ${status_notification}  Success
    Wait Until Page Contains  Show All  ${TIMEOUT_20}
    Select From List By Value    ${portal_role_search_option}    name
    Wait Until Page Contains Element  ${portal_role_edit}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text    ${portal_role_input_name_search}    ${new_role_name}
    Wait Until Page Contains Element  ${portal_role_edit}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Element Text Should Be  ${portal_role_table_name}  ${new_role_name}
    Click Element  ${portal_role_edit}
    CREATE ROLE WITH ANY PERMISSION GENERIC  ${role_name}@@  ${portal_roles_01}  ${viewportalrole}
    Scroll element into view  ${new_role_input}
    Wait Until Element is Visible  ${portal_role_invalid_rolename_msg}  ${TIMEOUT_20}
    Log to console  cannot update portal role with invalid role name
    ${RESULT}  Set Variable  PASS
    Run Keyword And Ignore Error  DELETE ROLE  ${portal_role_rolename}
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


UPDATE CURRENT ADMIN PORTAL ROLE
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element  ${PORTAL_ROLES}  ${TIMEOUT_20}
    click element  ${PORTAL_ROLES}
    Wait Until Page Contains Element  ${create_role_button}  ${TIMEOUT_20}
    click element  ${create_role_button}
    ${role_name}  Generate Random String  5  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GENERIC  Role_${role_name}  ${portal_roles_01}  ${editportalrole}
    ${portal_role_rolename}  Set Variable  ${new_role_name}
    Click Element    ${create_button}
    Wait until page contains element  ${status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${status_notification}  Success
    Sleep  1.0
    ${role_name1}  set variable  ${new_role_name}
    Click Element  ${dashboard_breadcrumb}
    Wait Until Page Contains  Dashboard  ${TIMEOUT_20}

    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${portal_user_password}
    CREATE PORTAL USER SELECT ROLES  ${role_name1}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${status_notification}  Success
    LOGOUT FROM NEPTUNE
    Log  ${portal_username}
    LOGIN TO NEPTUNE  ${portal_username}  ${portal_user_password}
    Wait Until Page Contains Element  ${PORTAL_ROLES}  ${TIMEOUT_20}
    click element  ${PORTAL_ROLES}
    Wait Until Page Contains  All Portal Roles  ${TIMEOUT_20}
    Select From List By Value    ${portal_role_search_option}    name
    Wait Until Page Contains Element  ${portal_role_edit}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text    ${portal_role_input_name_search}    ${role_name1}
    Wait Until Page Contains Element  ${portal_role_edit}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Element Text Should Be  ${portal_role_table_name}  ${role_name1}
    Click Element  ${portal_role_edit}
    CREATE ROLE WITH ANY PERMISSION GENERIC  ${role_name1}  ${portal_roles_01}  ${createportalrole}
    Click Element  ${role_attribute}
    Scroll element into view  xpath=//*[contains(text(), "${deleteportaluser}")]
    Sleep  ${SLEEP}
    Click Element  xpath=//*[contains(text(), "${deleteportaluser}")]
    Click Element  xpath=//span[contains(text(),'Update')]
    Element Text Should Be  ${status_notification}  Success
    LOGOUT FROM NEPTUNE
    LOGIN TO NEPTUNE  ${portal_username}  ${portal_user_password}
    Wait Until Page Contains Element  ${PORTAL_ROLES}  ${TIMEOUT_20}
    click element  ${PORTAL_ROLES}
    Wait Until Page Contains  All Portal Roles  ${TIMEOUT_20}
    Page Should Contain Element  ${create_role_button}
    Page Should Contain Element  ${portal_role_delete_icon}
    LOGOUT FROM NEPTUNE
    Log to console  updated current admin portal role
    ${RESULT}  Set Variable  PASS
    Run Keyword And Ignore Error  DELETE ROLE  ${portal_role_rolename}
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


DELETE ROLE PERMISSION
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element  ${PORTAL_ROLES}  ${TIMEOUT_20}
    click element  ${PORTAL_ROLES}
    Wait Until Page Contains Element  ${create_role_button}  ${TIMEOUT_20}
    click element  ${create_role_button}
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GENERIC  Role_${role_name}  ${portal_roles_01}  ${viewportalrole}
    Click Element    ${create_button}
    Element Text Should Be  ${status_notification}  Success
    Wait Until Page Contains  Show All  ${TIMEOUT_20}
    Select From List By Value    ${portal_role_search_option}    name
    Wait Until Page Contains Element  ${portal_role_edit}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text    ${portal_role_input_name_search}    ${new_role_name}
    Wait Until Page Contains Element  ${portal_role_edit}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Element Text Should Be  ${portal_role_table_name}  ${new_role_name}
    Click Element    ${portal_role_delete_icon}
    Wait Until Page Contains  Delete Portal Role  ${TIMEOUT_20}
    Click Element    ${portal_role_delete_role_yes}
    Element Text Should Be  ${status_notification}  Delete Success
    Log to console  successfully deleted role permission
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


DELETE SEVERAL ROLE PERMISSION
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element  ${PORTAL_ROLES}  ${TIMEOUT_20}
    click element  ${PORTAL_ROLES}
    Wait Until Page Contains Element  ${create_role_button}  ${TIMEOUT_20}
    click element  ${create_role_button}
    ${role_name}  Generate Random String  5  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GENERIC  Role_${role_name}  ${portal_roles_01}  ${viewportalrole}
    Click Element    ${create_button}
    Wait until page contains element  ${status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${status_notification}  Success
    Sleep  1.0
    ${role_name1}  set variable  ${new_role_name}
    Wait Until Page Contains Element  ${create_role_button}  ${TIMEOUT_20}
    click element  ${create_role_button}
    ${role_name}  Generate Random String  5  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GENERIC  Role_${role_name}  ${portal_roles_01}  ${deleteportaluser}
    Click Element    ${create_button}
    Wait until page contains element  ${status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${status_notification}  Success
    ${role_name2}  set variable  ${new_role_name}
    Select From List By Value    ${portal_role_search_option}    name
    Wait Until Page Contains Element  ${portal_role_edit}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text    ${portal_role_input_name_search}    ${role_name1}
    Wait Until Page Contains Element  ${portal_role_edit}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Element Text Should Be  ${portal_role_table_name}  ${role_name1}
    Click Element  ${portal_role_checkbox1}
    Input Text    ${portal_role_input_name_search}    ${role_name2}
    Wait Until Page Contains Element  ${portal_role_edit}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Element Text Should Be  ${portal_role_table_name}  ${role_name2}
    Click Element  ${portal_role_checkbox1}
    Click Element  ${portal_role_delete_selected_role}
    Wait Until Page Contains  Delete Portal Role  ${TIMEOUT_20}
    Click Element    ${portal_role_delete_role_yes}
    Wait until page contains element  ${status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${status_notification}  Delete Success
    Log to console  successfully deleted several role permission
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


DELETE PORTAL ROLE FOR CURRENT ADMIN
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element  ${PORTAL_ROLES}  ${TIMEOUT_20}
    click element  ${PORTAL_ROLES}
    Wait Until Page Contains Element  ${create_role_button}  ${TIMEOUT_20}
    click element  ${create_role_button}
    ${role_name}  Generate Random String  5  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GENERIC  Role_${role_name}  ${portal_roles_01}  ${deleteportaluser}
    Click Element    ${create_button}
    Wait until page contains element  ${status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${status_notification}  Success
    Sleep  1.0
    ${role_name1}  set variable  ${new_role_name}
    Click Element  ${dashboard_breadcrumb}
    Wait Until Page Contains  Dashboard  ${TIMEOUT_20}

    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${portal_user_password}
    CREATE PORTAL USER SELECT ROLES  ${role_name1}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${status_notification}  Success
    LOGOUT FROM NEPTUNE
    Log  ${portal_username}
    LOGIN TO NEPTUNE  ${portal_username}  ${portal_user_password}
    Wait Until Page Contains Element  ${PORTAL_ROLES}  ${TIMEOUT_20}
    click element  ${PORTAL_ROLES}
    Wait Until Page Contains  All Portal Roles  ${TIMEOUT_20}
    Select From List By Value    ${portal_role_search_option}    name
    #Wait Until Page Contains Element  ${portal_role_edit}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text    ${portal_role_input_name_search}    ${new_role_name}
    Wait Until Page Contains Element  ${portal_role_delete_icon}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Element Text Should Be  ${portal_role_table_name}  ${new_role_name}
    Click Element    ${portal_role_delete_icon}
    Wait Until Page Contains  Delete Portal Role  ${TIMEOUT_20}
    Click Element    ${portal_role_delete_role_yes}
    Element Text Should Be  ${status_notification}  Delete Success
    LOGOUT FROM NEPTUNE
    Log to console  successfully deleted portal role for current admin
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


VALIDATE THAT ROLE ZEBRA SYSTEM ADMIN CANNOT BE DELETED
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element  ${PORTAL_ROLES}  ${TIMEOUT_20}
    click element  ${PORTAL_ROLES}
    Select From List By Value    ${portal_role_search_option}    name
    Wait Until Page Contains Element  ${portal_role_edit}  ${TIMEOUT_20}
    Sleep  ${SLEEP}
    Input Text    ${portal_role_input_name_search}    Zebra System Admin
    Sleep  ${SLEEP}
    Page should contain Element  ${portal_role_zebra_admin_view_icon}
    Page Should Not Contain Element  ${portal_role_delete_icon}
    Log to console  cannot delete zebra system admin role
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN















