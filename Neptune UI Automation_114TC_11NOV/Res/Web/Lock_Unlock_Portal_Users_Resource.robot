*** Settings ***

Library         ExtendedSelenium2Library
Library         String
Library         Dialogs
Library         DateTime
Variables       ${EXECDIR}/VariableFiles/Web/Config.yaml
Resource        ${EXECDIR}/Res/Web/GenericFunction.robot


*** Variables ***

${wrongpassword}  wrongpassword
${userwithoutpermission}  Logintest
${edit_icon}  xpath=(//i[@class="fa fa-pencil"])

*** Keywords ***

ONE OR TWO LOGIN TRIALS INCORRECT PASSWORD
    ${RESULT}  Set Variable  FAIL
    Run Keyword And Ignore Error  LOGOUT FROM NEPTUNE
    LOGIN TO NEPTUNE  ${USERNAME}  ${wrongpassword}
    Wait Until Page Contains  Bad credentials.  ${TIMEOUT_20}
    Log to Console  Bad Credentials
    LOGIN TO NEPTUNE
    Builtin.Sleep  8
    Log to Console  login with incorrect password for 1 or 2 trials validated
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

LOGIN AFTER ONE OR TWO FAILURES
    ${RESULT}  Set Variable  FAIL
    Run Keyword And Ignore Error  LOGOUT FROM NEPTUNE
    LOGIN TO NEPTUNE  ${USERNAME}  ${wrongpassword}
    Wait Until Page Contains  Bad credentials.  ${TIMEOUT_20}
    Log to Console  Bad Credentials
    LOGIN TO NEPTUNE
    Builtin.Sleep  8
    Log to Console  Login with correct password after 1 or 2 failure trials validated
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

USER WITHOUT PERMISSION CANNOT LOCK/UNLOCK ANOTHER USER
    ${RESULT}  Set Variable  FAIL
    Run Keyword And Ignore Error  LOGOUT FROM NEPTUNE
    LOGIN TO NEPTUNE  ${userwithoutpermission}
    Wait Until Page Contains Element  ${PORTAL_USERS}  ${TIMEOUT_20}
    click element  ${PORTAL_USERS}
    Page Should Not Contain Element  ${edit_icon}
    Log to Console  User cannot lock/unlock another user
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

