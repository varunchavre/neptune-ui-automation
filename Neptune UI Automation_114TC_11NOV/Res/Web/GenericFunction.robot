
*** Settings ***


Library         ExtendedSelenium2Library
Library         String
Library         Dialogs
Library         DateTime
Variables       ${EXECDIR}/VariableFiles/Web/Config.yaml

*** Variables ***

${input_user_id}  //input[@ng-reflect-name="username"]
${input_password}  //input[@ng-reflect-name="password"]
${login_button}  //span[@class="mat-button-wrapper"]
${neptune_dashboard}  https://k8s-qa.pttpro.zebra.com/dashboard
${username_button}  //a[@class="user-name"]
${logout_button}  //span[contains(text(),'Logout')]

#
${portal_roles_button}  xpath=(//mat-card[@class="mat-card"])[5]
${create_role_button}  //a[@class="link-btn ng-star-inserted"]
${user_attributes1}  User Attributes
${permission_to_delete_user_attributes}  Permission to delete user attributes
${new_role_input}  //input[@ng-reflect-name="name"]
${create_button}  xpath=(//span[@class="mat-button-wrapper"])[6]
${new_role}  a


${portal_users_tab}                      xpath=(//mat-card[@class="mat-card"])[6]
${portal_user_create_user}             //a[@class="link-btn ng-star-inserted"]
${portal_user_enter_username}  //input[@ng-reflect-name="username"]
${portal_user_search_for_role}         //input[@ng-reflect-name="search"]
${portal_user_create_the_user}         //button[@class="mat-raised-button mat-primary"]
${portal_user_username}                //mat-cell[@class="mat-cell cdk-column-username mat-column-username ng-star-inserted"]
${portal_user_password}  Newuser@123
${click_element}  //div[@class="mat-list-text"]

# ${portal_user_enter_email}             //input[@ng-reflect-name="email"]
${portal_user_enter_password}          //input[@ng-reflect-name="password"]
${portal_user_enter_confirm_password}  //input[@ng-reflect-name="confirmPassword"]

${change_password_button}                      //span[contains(text(),'Change Password')]
${change_password_enter_current_password}      //input[@ng-reflect-name="currentPassword"]
${change_password_enter_new_password}          //input[@ng-reflect-name="newPassword"]
${change_password_enter_new_confirm_password}  //input[@ng-reflect-name="newPasswordConfirm"]
${role_names}


*** Keywords ***

SETUP CHROMEDRIVER
    Set Environment Variable  webdriver.chrome.driver  ${EXECDIR}/chromedriver.exe

SETUP FIREFOXDRIVER
    Set Environment Variable  webdriver.firefox.driver  ${EXECDIR}/geckodriver.exe

SETUP IEDRIVER
    Set Environment Variable  webdriver.ie.driver  ${EXECDIR}/MicrosoftWebDriver.exe

SUITE SETUP
    LAUNCH BROWSER

SUITE TEARDOWN
    Close Browser

TEST SETUP
    Go To  ${neptune_dashboard}
    Builtin.Sleep  2
    Run Keyword and ignore error  LOGIN TO NEPTUNE

TEST TEARDOWN
    Close Browser
    LAUNCH BROWSER
    LOGIN TO NEPTUNE

LAUNCH BROWSER
    Open Browser    ${SITEURL}    ${BROWSER_TYPE}
    Maximize Browser Window

LOGIN TO NEPTUNE
    [Arguments]  ${Login}=${USERNAME}  ${PSWRD}=${PASSWORD}
    Wait Until Page Contains Element    ${input_user_id}  ${TIMEOUT_20}
    Input Text    ${input_user_id}    ${Login}
    Wait Until Page Contains Element    ${input_password}  1
    Input Text    ${input_password}    ${PSWRD}
    Wait Until Page Contains Element    ${login_button}  1
    Click Element    ${login_button}
    Set Global Variable  ${Login}
    Set Global Variable  ${PSWRD}


LOGOUT FROM NEPTUNE
    Go To  ${SITEURL}
    Wait Until Page Contains Element  ${username_button}  ${TIMEOUT_20}
    Builtin.Sleep  2
    Click Element    ${username_button}
    # Builtin.Sleep  1
    Wait Until Page Contains Element    ${logout_button}  ${TIMEOUT_20}
    # Builtin.Sleep  3
    Click Element    ${logout_button}


CREATE ROLE WITH ANY PERMISSION GEN
    [Arguments]  ${new_role_arg}=${new_role}  ${user_attributes_arg}=${user_attributes1}  ${permission_to_delete_user_attributes_arg}=${permission_to_delete_user_attributes}
    Wait Until Page Contains Element  ${PORTAL_ROLES}  ${TIMEOUT_20}
    click element  ${PORTAL_ROLES}
    Wait Until Page Contains Element  ${create_role_button}  ${TIMEOUT_20}
    click element  ${create_role_button}
    Wait Until Page Contains Element    ${new_role_input}  ${TIMEOUT_20}
    Input Text    ${new_role_input}  ${new_role_arg}
    Builtin.sleep  1
    Click Element  //mat-panel-title[contains(text(),'${user_attributes_arg}')]
    #xpath=//*[contains(text(), "${user_attributes_arg}")]
    Wait Until Page Contains Element  xpath=//*[contains(text(), "${permission_to_delete_user_attributes_arg}")]  ${TIMEOUT_20}
    Scroll element into view  xpath=//*[contains(text(), "${permission_to_delete_user_attributes_arg}")]
    Builtin.sleep  3
    Click Element    xpath=//*[contains(text(), "${permission_to_delete_user_attributes_arg}")]
    Click Element    ${create_button}
    Log to Console  Role succesfully created


CREATE PORTAL USER WITH ANY ROLE 01
    [Arguments]  ${portal_user_username}=xyzabc  ${portal_user_rolename}=View_Only
    TEST SETUP

    Wait Until Page Contains Element  ${portal_users_tab}  ${TIMEOUT_20}
    Click Element  ${portal_users_tab}
    Wait Until Page Contains Element  ${portal_user_create_user}  ${TIMEOUT_20}
    Click Link  ${portal_user_create_user}
    Wait Until Page Contains  UserName  ${TIMEOUT_20}
    Builtin.sleep  2
    Input Text  ${portal_user_enter_username}  ${portal_user_username}
    Input Text  ${portal_user_enter_password}  ${portal_user_password}
    Input Text  ${portal_user_enter_confirm_password}  ${portal_user_password}
    Input Text  ${portal_user_search_for_role}  ${portal_user_rolename}
    Builtin.sleep  3
    Wait Until Page Contains Element  ${click_element}  ${TIMEOUT_20}
    Builtin.sleep  3
    Click Element  ${click_element}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Builtin.sleep  3
    Click Element    ${portal_user_create_the_user}

#    GO TO PORTAL USERS TAB
#    CREATE PORTAL USER INPUTS  ${portal_user_rolename}
#    CREATE PORTAL USER SELECT ROLES  ${portal_user_rolename}
#    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
#    Builtin.sleep  3
#    Click Element    ${portal_user_create_the_user}

    Log to console  created new administrator with single role


GENERATE RANDOM NO
    ${role_names}  Generate Random String  4  [NUMBERS]
    Set Global Variable  ${role_names}

GO TO PORTAL USERS TAB
    Wait Until Page Contains Element  ${portal_users_tab}  ${TIMEOUT_20}
    Click Element  ${portal_users_tab}
    Wait Until Page Contains  Portal Users  ${TIMEOUT_20}
    Wait Until Page Contains  Show All  ${TIMEOUT_20}

CREATE PORTAL USER INPUTS
    [Arguments]  ${portal_user_username}=${EMPTY}  ${portal_user_password}=Newuser@123
    Wait Until Page Contains Element  ${portal_user_create_user}  ${TIMEOUT_20}
    Click Link  ${portal_user_create_user}
    Wait Until Page Contains  Create Portal User  ${TIMEOUT_20}
    Wait Until Page Contains  UserName  ${TIMEOUT_20}
    ${role_names}  Set Variable  ${EMPTY}
    run keyword if  '${portal_user_username}' == '${EMPTY}'  GENERATE RANDOM NO
    Log  ${role_names}
    ${portal_user_username}=  Set Variable If  '${role_names}' != '${EMPTY}'  User_${role_names}
    ...  ${portal_user_username}
    Set Global Variable  ${portal_user_username}
    Wait Until Page Contains Element  ${portal_user_enter_username}  ${TIMEOUT_20}
    Input Text  ${portal_user_enter_username}  ${portal_user_username}
    Wait Until Page Contains Element  ${portal_user_enter_password}  ${TIMEOUT_20}
    Input Text  ${portal_user_enter_password}  ${portal_user_password}
    Wait Until Page Contains Element  ${portal_user_enter_confirm_password}  ${TIMEOUT_20}
    Input Text  ${portal_user_enter_confirm_password}  ${portal_user_password}

CREATE PORTAL USER SELECT ROLES
    [Arguments]  ${portal_user_rolename}=View Only
    Input Text  ${portal_user_search_for_role}  ${portal_user_rolename}
    ${status}  @{stringg}=  Run keyword and ignore error  Split String  ${portal_user_rolename}  separator=${SPACE}
    ${count}  get length  @{stringg}
    #${strings}=  Get From List  @{stringg}  -1
    #${strings}  set variable  ${portal_user_rolename}
	#${portal_user_searched_user_role}  set variable  ${portal_user_searched_roles}${strings}")])
    sleep  3.0
    #Click Element  ${portal_user_searched_user_role}
    run keyword if  '${count}' > '1'  Click Element  xpath=(//span[@class="search-highlight"])[4]
    run keyword if  '${count}' == '1'  Click Element  //span[@class="search-highlight"]


GO TO CHANGE PASSWORD
    Click Link  ${username_button}
    Wait Until Page Contains  Change Password  ${TIMEOUT_20}
    Click Element  ${change_password_button}
    #Click Element    xpath=(//span)[23]
    Wait Until Page Contains  Reset Password  ${TIMEOUT_20}

CHANGE PASSWORD INPUTS
    [Arguments]  ${new_passwd}=${EMPTY}  ${new_confirm_passwd}=${EMPTY}
    Clear Element Text  ${change_password_enter_current_password}
    Input Text  ${change_password_enter_current_password}  ${PSWRD}
    Clear Element Text  ${change_password_enter_new_password}
    Input Text  ${change_password_enter_new_password}  ${new_passwd}
    Clear Element Text  ${change_password_enter_new_confirm_password}
    Input Text  ${change_password_enter_new_confirm_password}  ${new_confirm_passwd}


GO TO SPECIFIED PAGE FROM DASHBOARD
    [Arguments]  ${option}=Portal Users
    Wait Until Page Contains Element  //mat-card[contains(text(),'${option}')]  ${TIMEOUT_20}
    Click Element  //mat-card[contains(text(),'${option}')]
    Wait Until Page Contains  ${option}  ${TIMEOUT_20}
    Page Should Contain  ${option}