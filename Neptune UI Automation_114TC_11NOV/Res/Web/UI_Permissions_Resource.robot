*** Settings ***

Library         ExtendedSelenium2Library
Library         String
Library         Dialogs
Library         DateTime
Variables       ${EXECDIR}/VariableFiles/Web/Config.yaml
Resource        ${EXECDIR}/Res/Web/GenericFunction.robot


*** Variables ***
${application_definition_01}  Application Definition
${configuration_profile_01}  Configuration Profile
${devices_01}  Devices
${jobs_01}  Jobs
${device_users_01}  Device Users
${extension_manager_01}  Extension Manager
${licenses_01}  Licenses
${portal_roles_01}  Portal Roles
${portal_users_01}  Portal Users
${reports_01}  Reports
${rules_01}  Rules
${users_group_import_01}  Users Group Import
${user_attributes_01}  User Attributes

${viewapplicationdefinitions}  Permission to view list of application definition
${createapplicationdefinitions}  Permission to create application definitions
${editapplicationdefinitions}  Permission to edit application definitions
${deleteapplicationdefinitions}  Permission to delete application definitions

${viewconfigprofile}  Permission to view list of configuration profiles
${createconfigprofile}  Permission to create configuration profile
${editconfigprofile}  Permission to edit configuration profile
${deleteconfigprofile}  Permission to delete configuration profile


${viewdevice}  Permission to view list of devices
${deletedevice}  Permission to de-enroll devices

${viewdeviceusers}  Permission to view list of device users
${createdeviceusers}  Permission to create device users
${editdeviceusers}  Permission to edit device users
${deletedeviceusers}  Permission to delete device users
${bulkimportdeviceusers}  Permission to bulk import device users


${viewextension}  Permission to view list of extensions
${bulkimportextension}  Permission to bulk import extensions


${viewlicense}  Permission to view license information


${viewportalrole}  Permission to view portal roles
${createportalrole}  Permission to create portal roles
${editportalrole}  Permission to edit portal roles
${deleteportalrole}  Permission to delete portal roles

${viewportaluser}  Permission to view list of portal users
${unlockportaluser}  Permission to lock/unlock portal users
${editportaluser}  Permission to edit portal users
${createportaluser}  Permission to create portal users
${deleteportaluser}  Permission to delete portal user
${changeportaluserpassword}  Permission to reset password for other portal users

${viewreporttemplate}  Permission to view saved reports
${generatereport}  Permission to generate reports (view permission for reports)
${exportreport}  Permission to export reports
${createreporttemplate}  Permission to save report as template
${editreporttemplate}  Permission to edit saved reports
${deletereporttemplate}  Permission to delete saved reports


${viewrules}  Permission to view list of rules
${createrules}  Permission to create rules
${editrules}  Permission to edit rules
${deleterules}  Permission to delete rules
${publishrules}  Permission to publish rules


${viewjobs}  Permission to view list of jobs
${createjobs}  Permission to create a job
${editjobs}  Permission to edit a job
${deletejobs}  Permission to delete a job
${runjobs}  Permission to run a job manually
${viewjobhistory}  Permission to view history of a job
${viewjobhistorydetails}  Permission to view details of an entry in job history
${viewnotifications}  Permission to view list of notification templates
${createnotifications}  Permission to create notification templates
${editnotifications'}  Permission to edit notification templates
${deletenotifications}  Permission to delete notification templates


${viewuserattributes}  Permission to view list of user attributes
${createuserattributes}  Permission to create user attributes
${edituserattributes}  Permission to edit user attributes
${deleteuserattributes}  Permission to delete user attributes


${generic_delete_icon}             //i[@class="fa fa-trash"]
${generic_edit_icon}               //i[@class="fa fa-pencil"]
${generic_create_user}             //a[@class="link-btn ng-star-inserted"]

${lock_user}  //span[contains(text(),'Lock')]
${unlock_user}  //span[contains(text(),'UnLock')]
${yes_popup}  //button[@class="mat-raised-button mat-primary ng-star-inserted cdk-focused cdk-program-focused"]
#//button[@class='mat-raised-button mat-primary ng-star-inserted']
# ${no_popup}  //button[contains(text(),'No')]
# ${cancel_popup}  //span[contains(text(),'Cancel')]

# ${new_user_password_confirm}  //input[@ng-reflect-name="confirmPassword"]
# ${new_password_input}  //input[@ng-reflect-name="newPassword"]
${reset_password_click}  //span[contains(text(),'Reset')]
${reset_password_button}  xpath=(//i[@class="fa fa-lock"])
#//mat-table[@class='stripped mat-table']//mat-row[3]
${manual_reset}  //div[contains(text(),'Manual Reset')]


${user_dashboard_element}  //mat-card[@class='mat-card']

#Extension
${import_extensions}  //a[@class='mat-tab-link ng-star-inserted']
*** Keywords ***



# UI PERMISSION
#PORTAL USER
VIEWPORTALUSER
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${portal_users_01}  ${viewportaluser}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${PORTAL_USERS}
    Wait Until Page Contains Element  ${PORTAL_USERS}  ${TIMEOUT_20}
    Click Element  ${PORTAL_USERS}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for View Portal User Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

EDITPORTALUSER
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${portal_users_01}  ${editportaluser}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${PORTAL_USERS}
    Wait Until Page Contains Element  ${PORTAL_USERS}  ${TIMEOUT_20}
    Click Element  ${PORTAL_USERS}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Edit Portal User Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

CREATEPORTALUSER
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${portal_users_01}  ${createportaluser}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${PORTAL_USERS}
    Wait Until Page Contains Element  ${PORTAL_USERS}  ${TIMEOUT_20}
    Click Element  ${PORTAL_USERS}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Edit Portal User Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

DELETEPORTALUSER
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${portal_users_01}  ${deleteportaluser}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${PORTAL_USERS}
    Wait Until Page Contains Element  ${PORTAL_USERS}  ${TIMEOUT_20}
    Click Element  ${PORTAL_USERS}
    Page Should Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Delete Portal User Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

UNLOCKPORTALUSER
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${portal_users_01}  ${unlockportaluser}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${PORTAL_USERS}
    Wait Until Page Contains Element  ${PORTAL_USERS}  ${TIMEOUT_20}
    Click Element  ${PORTAL_USERS}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    BuiltIn.sleep  2
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Unlock Portal User Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

CHANGEPORTALUSERPASSWORD
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${portal_users_01}  ${changeportaluserpassword}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Wait Until Page Contains Element  ${PORTAL_USERS}  ${TIMEOUT_20}
    Click Element  ${PORTAL_USERS}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    Wait Until Page Contains Element  ${reset_password_button}  ${TIMEOUT_20}
    BuiltIn.Sleep  6
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Change Portal User Password Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


#DEVICES

VIEWDEVICE
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${devices_01}  ${viewdevice}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Wait Until Page Contains Element  ${DEVICES}  ${TIMEOUT_20}
    Click Element  ${DEVICES}
    Page Should Not Contain Element  ${generic_delete_icon}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for View Device Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

DELETEDEVICE
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${devices_01}  ${deletedevice}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Wait Until Page Contains Element  ${DEVICES}  ${TIMEOUT_20}
    Click Element  ${DEVICES}
    Page Should Contain Element  ${generic_delete_icon}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Delete Device Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

#CONFIGURATION PROFILE

VIEWCONFIGPROFILES
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${configuration_profile_01}  ${viewconfigprofile}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Wait Until Page Contains Element  ${user_dashboard_element}  ${TIMEOUT_20}
    Click Element  ${user_dashboard_element}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for View Configuration Profiles Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

CREATECONFIGPROFILES
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${configuration_profile_01}  ${createconfigprofile}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Wait Until Page Contains Element  ${user_dashboard_element}  ${TIMEOUT_20}
    Click Element  ${user_dashboard_element}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Create Configuration Profiles Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


EDITCONFIGPROFILES
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${configuration_profile_01}  ${editconfigprofile}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Wait Until Page Contains Element  ${user_dashboard_element}  ${TIMEOUT_20}
    Click Element  ${user_dashboard_element}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Edit Configuration Profiles Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


DELETECONFIGPROFILES
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${configuration_profile_01}  ${deleteconfigprofile}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Wait Until Page Contains Element  ${user_dashboard_element}  ${TIMEOUT_20}
    Click Element  ${user_dashboard_element}
    Page Should Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Delete Configuration Profiles Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

#PORTAL ROLES

VIEWPORTALROLE
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${portal_roles_01}  ${viewportalrole}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${PORTAL_ROLES}
    Wait Until Page Contains Element  ${PORTAL_ROLES}  ${TIMEOUT_20}
    Click Element  ${PORTAL_ROLES}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for View Portal Role Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

CREATEPORTALROLE
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${portal_roles_01}  ${createportalrole}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${PORTAL_ROLES}
    Wait Until Page Contains Element  ${PORTAL_ROLES}  ${TIMEOUT_20}
    Click Element  ${PORTAL_ROLES}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Create Portal Role Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

EDITPORTALROLE
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${portal_roles_01}  ${editportalrole}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${PORTAL_ROLES}
    Wait Until Page Contains Element  ${PORTAL_ROLES}  ${TIMEOUT_20}
    Click Element  ${PORTAL_ROLES}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Edit Portal Role Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

DELETEPORTALROLE
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${portal_roles_01}  ${deleteportalrole}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${PORTAL_ROLES}
    Wait Until Page Contains Element  ${PORTAL_ROLES}  ${TIMEOUT_20}
    Click Element  ${PORTAL_ROLES}
    Page Should Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Delete Portal Role Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

#USER ATTRIBUTES

VIEWUSERATTRIBUTES
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${user_attributes_01}  ${viewuserattributes}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${user_dashboard_element}
    Wait Until Page Contains Element  ${user_dashboard_element}  ${TIMEOUT_20}
    Click Element  ${user_dashboard_element}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for View Portal Role Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

CREATEUSERATTRIBUTES
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${user_attributes_01}  ${createuserattributes}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${user_dashboard_element}
    Wait Until Page Contains Element  ${user_dashboard_element}  ${TIMEOUT_20}
    Click Element  ${user_dashboard_element}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Create Portal Role Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

EDITUSERATTRIBUTES
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${user_attributes_01}  ${edituserattributes}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${user_dashboard_element}
    Wait Until Page Contains Element  ${user_dashboard_element}  ${TIMEOUT_20}
    Click Element  ${user_dashboard_element}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Edit Portal Role Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

DELETEUSERATTRIBUTES
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${user_attributes_01}  ${deleteuserattributes}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${user_dashboard_element}
    Wait Until Page Contains Element  ${user_dashboard_element}  ${TIMEOUT_20}
    Click Element  ${user_dashboard_element}
    Page Should Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Delete Portal Role Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

#EXTENSION MANAGER

VIEWEXTENSION
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${extension_manager_01}  ${viewextension}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${EXTENSION_MANAGER}
    Wait Until Page Contains Element  ${EXTENSION_MANAGER}  ${TIMEOUT_20}
    Click Element  ${EXTENSION_MANAGER}
    Page Should Not Contain Element  ${import_extensions}
    #Page Should Not Contain Element  ${generic_edit_icon}
    #Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for View Extension Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

BULKIMPORTEXTENSION
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${extension_manager_01}  ${bulkimportextension}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${EXTENSION_MANAGER}
    Wait Until Page Contains Element  ${EXTENSION_MANAGER}  ${TIMEOUT_20}
    Click Element  ${EXTENSION_MANAGER}
    Page Should Contain Element  ${import_extensions}
    #Page Should Not Contain Element  ${generic_edit_icon}
    #Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Bulk Import Extension Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN\

#DEVICE USERS

VIEWDEVICEUSERS
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${device_users_01}  ${viewdeviceusers}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${DEVICE_USERS}
    Wait Until Page Contains Element  ${DEVICE_USERS}  ${TIMEOUT_20}
    Click Element  ${DEVICE_USERS}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for View Device Users Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

CREATEDEVICEUSERS
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${device_users_01}  ${createdeviceusers}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${DEVICE_USERS}
    Wait Until Page Contains Element  ${DEVICE_USERS}  ${TIMEOUT_20}
    Click Element  ${DEVICE_USERS}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Contain Element  ${generic_create_user}
    ${count_1}  Get Matching Xpath Count  ${generic_create_user}
    Should Be True  ${count_1} < 2
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Create Device Users Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

EDITDEVICEUSERS
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${device_users_01}  ${editdeviceusers}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${DEVICE_USERS}
    Wait Until Page Contains Element  ${DEVICE_USERS}  ${TIMEOUT_20}
    Click Element  ${DEVICE_USERS}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Edit Device Users Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

DELETEDEVICEUSERS
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${device_users_01}  ${deletedeviceusers}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${DEVICE_USERS}
    Wait Until Page Contains Element  ${DEVICE_USERS}  ${TIMEOUT_20}
    Click Element  ${DEVICE_USERS}
    Page Should Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Delete Device Users Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

BULKIMPORTUSERS
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${device_users_01}  ${bulkimportdeviceusers}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${DEVICE_USERS}
    Wait Until Page Contains Element  ${DEVICE_USERS}  ${TIMEOUT_20}
    Click Element  ${DEVICE_USERS}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Contain Element  ${generic_create_user}
    ${count_1}  Get Matching Xpath Count  ${generic_create_user}
    Should Be True  ${count_1} < 2
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Bulk Import Users Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN



#APPLICATION DEFINITION

VIEWAPPLICATIONDEFINITIONS
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${application_definition_01}  ${viewapplicationdefinitions}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${APPLICATION_DEFINITIONS}
    Wait Until Page Contains Element  ${APPLICATION_DEFINITIONS}  ${TIMEOUT_20}
    Click Element  ${APPLICATION_DEFINITIONS}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for View Application Definitions Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

CREATEAPPLICATIONDEFINITIONS
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${application_definition_01}  ${createapplicationdefinitions}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${APPLICATION_DEFINITIONS}
    Wait Until Page Contains Element  ${APPLICATION_DEFINITIONS}  ${TIMEOUT_20}
    Click Element  ${APPLICATION_DEFINITIONS}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Create Application Definitions Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

EDITAPPLICATIONDEFINITIONS
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${application_definition_01}  ${editapplicationdefinitions}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${APPLICATION_DEFINITIONS}
    Wait Until Page Contains Element  ${APPLICATION_DEFINITIONS}  ${TIMEOUT_20}
    Click Element  ${APPLICATION_DEFINITIONS}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Edit Application Definitions Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

DELETEAPPLICATIONDEFINITIONS
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${application_definition_01}  ${deleteapplicationdefinitions}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${APPLICATION_DEFINITIONS}
    Wait Until Page Contains Element  ${APPLICATION_DEFINITIONS}  ${TIMEOUT_20}
    Click Element  ${APPLICATION_DEFINITIONS}
    Page Should Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Delete Application Definitions Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

#RULES

VIEWRULES
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${rules_01}  ${viewrules}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${RULES}
    Wait Until Page Contains Element  ${RULES}  ${TIMEOUT_20}
    Click Element  ${RULES}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for View Device Users Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

CREATERULES
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${rules_01}  ${createrules}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${RULES}
    Wait Until Page Contains Element  ${RULES}  ${TIMEOUT_20}
    Click Element  ${RULES}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Contain Element  ${generic_create_user}
    ${count_1}  Get Matching Xpath Count  ${generic_create_user}
    Should Be True  ${count_1} < 2
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Create Rules Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

EDITRULES
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${rules_01}  ${editrules}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${RULES}
    Wait Until Page Contains Element  ${RULES}  ${TIMEOUT_20}
    Click Element  ${RULES}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Edit Rules Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

DELETERULES
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${rules_01}  ${deleterules}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${RULES}
    Wait Until Page Contains Element  ${RULES}  ${TIMEOUT_20}
    Click Element  ${RULES}
    Page Should Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Not Contain Element  ${generic_create_user}
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Delete Rules Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

PUBLISHRULES
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${role_name}  Generate Random String  4  [NUMBERS]
    CREATE ROLE WITH ANY PERMISSION GEN  Role_${role_name}  ${rules_01}  ${publishrules}
    CREATE PORTAL USER WITH ANY ROLE 01  User_${role_name}  Role_${role_name}
    BuiltIn.sleep  5
    LOGOUT FROM NEPTUNE
    BuiltIn.sleep  5
    LOGIN TO NEPTUNE  User_${role_name}  ${PASSWORD}
    BuiltIn.Sleep  8
    ${count}  Get Matching Xpath Count  xpath=(//mat-card[@class="mat-card"])
    Should Be True  ${count} < 2
    Page Should Contain Element  ${RULES}
    Wait Until Page Contains Element  ${RULES}  ${TIMEOUT_20}
    Click Element  ${RULES}
    Page Should Not Contain Element  ${generic_delete_icon}
    Page Should Not Contain Element  ${generic_edit_icon}
    Page Should Contain Element  ${generic_create_user}
    ${count_1}  Get Matching Xpath Count  ${generic_create_user}
    Should Be True  ${count_1} < 2
    LOGOUT FROM NEPTUNE
    Log To Console  Expected Results for Publish Rules Verified Successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN
