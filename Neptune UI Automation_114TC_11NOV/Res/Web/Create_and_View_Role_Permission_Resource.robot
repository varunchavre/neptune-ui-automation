*** Settings ***

# Library         Selenium2Library
Library         ExtendedSelenium2Library
Library         String
Library         Dialogs
Library         DateTime
Variables       ${EXECDIR}/VariableFiles/Web/Config.yaml
Resource        ${EXECDIR}/Res/Web/GenericFunction.robot

*** Variables ***

${search}  //input[@name="query"]
${viewlicenses_role_search}  Viewlicenses
${viewlicenses_role}  xpath=//*[contains(text(), "Viewlicenses")]
${portal_roles_button}  //a[@title='Click here to manage portal roles']//mat-card[@class='mat-card']
${delete}  //i[@class="fa fa-trash"]
${edit}  //i[@class="fa fa-pencil"]
${last_updated_time}  Last Updated Time
${createdontime}  CreatedOnTime
${description}  Description
${type}  Type
${name}  Name
${size}  //div[@class="mat-paginator-page-size ng-star-inserted"]
${range_actions}  //div[@class="mat-paginator-range-actions"]
${create_role_button}  //a[@class="link-btn ng-star-inserted"]
${permission_to_delete_user_attributes1}  Permission to delete user attributes
${new_role_input}  //input[@ng-reflect-name="name"]
${create_button}  xpath=(//span[@class="mat-button-wrapper"])[6]
${new_role}  a

${permission_is_required_to_create_role_error}  //*['Permission is required to create role']
${role_name_is_required_error}  //*['Role name is required']

${first_role_name}  //mat-cell[@class="mat-cell cdk-column-name mat-column-name ng-star-inserted"]
${item_already_exists_error}  Item already exists

${role_value_error}  //*['Role name should contain only alpha numeric characters']

${first_page_button}  xpath=//*[@ng-reflect-message="First page"]
${last_page_button}  xpath=//*[@ng-reflect-message="Last page"]
${next_page_button}  xpath=//*[@ng-reflect-message="Next page"]
${previous_page_button}  xpath=//*[@ng-reflect-message="Previous page"]
${page_range_selector}  //div[@class="mat-select-arrow"]
${view_20_per_page}  xpath=//*[@ng-reflect-value="20"]
${range_indicator}  //div[@class="mat-paginator-range-label"]

${create_portal_user}  //a[@href="/system-management/portal-users/list/add"]
${portal_role_in_create_portal_user}  portal_role_validation

${create_portal_role_element}  //h1[@class="no-margin"]
${create_portal_role_close_button_element}  //div[@class="cdk-overlay-backdrop cdk-overlay-dark-backdrop cdk-overlay-backdrop-showing"]
${create_portal_role_name_required_element}  //div[@class="cdk-overlay-backdrop cdk-overlay-dark-backdrop cdk-overlay-backdrop-showing"]
${create_portal_role_descriptio_not_required}  xpath=(//div[@class="mat-input-flex mat-form-field-flex"])[3]
${create_role_permission_required_element}  //h4[@class="margin-bottom-20"]
${create_role_permission_require_element_asterick}  //div[@class="cdk-overlay-backdrop cdk-overlay-dark-backdrop cdk-overlay-backdrop-showing"]
${cancel_button_displayed_enabled_element}   //div[@class="cdk-overlay-backdrop cdk-overlay-dark-backdrop cdk-overlay-backdrop-showing"]
${create_button_displayed_disabled}  xpath=(//span[@class="mat-button-wrapper"])[6]

${close_create_portal_x}  //i[@class="fa fa-times-circle"]
${close_create_portal_cancel_button}  xpath=(//span[@class="mat-button-wrapper"])[5]

${application_definition_01}  Application Definition
${configuration_profile_01}  Configuration Profile
${devices_01}  Devices
${device_users_01}  Device Users
${extension_manager_01}  Extension Manager
${licenses_01}  Licenses
${portal_roles_01}  Portal Roles
${portal_users_01}  Portal Users
${reports_01}  Reports
${rules_01}  Rules
${users_group_import_01}  Users Group Import
${user_attributes_01}  User Attributes

${viewapplicationdefinitions}  Permission to view list of application definition
${createapplicationdefinitions}  Permission to create application definitions
${editapplicationdefinitions}  Permission to edit application definitions
${deleteapplicationdefinitions}  Permission to delete application definitions

${viewconfigprofile}  Permission to view list of configuration profiles
${createconfigprofile}  Permission to create configuration profile
${editconfigprofile}  Permission to edit configuration profile
${deleteconfigprofile}  Permission to delete configuration profile

${viewdevice}  Permission to view list of devices
${deletedevice}  Permission to de-enroll devices

${viewdeviceusers}  Permission to view list of device users
${deletedeviceusers}  Permission to create device users
${editdeviceusers}  Permission to edit device users
${deletedeviceusers}  Permission to delete device users
${bulkimportdeviceusers}  Permission to bulk import device users

${viewextension}  Permission to view list of extensions
${bulkimportextension}  Permission to bulk import extensions

${viewlicense}  Permission to view license information

${viewportalrole}  Permission to view portal roles
${createportalrole}  Permission to create portal roles
${editportalrole}  Permission to edit portal roles
${deleteportalrole}  Permission to delete portal roles

${viewportaluser}  Permission to view list of portal users
${unlockportaluser}  Permission to lock/unlock portal users
${editportaluser}  Permission to edit portal users
${createportalusers}  Permission to create portal users
${deleteportaluser}  Permission to delete portal user
${changeportaluserpassword}  Permission to reset password for other portal users

${viewreporttemplate}  Permission to view saved reports
${generatereport}  Permission to generate reports (view permission for reports)
${exportreport}  Permission to export reports
${createreporttemplate}  Permission to save report as template
${editreporttemplate}  Permission to edit saved reports
${deletereporttemplate}  Permission to delete saved reports

${viewrules}  Permission to view list of rules
${createrules}  Permission to create rules
${editrules}  Permission to edit rules
${deleterules}  Permission to delete rules
${publishrules}  Permission to publish rules

${viewjobs}  Permission to view list of jobs
${createjobs}  Permission to create a job
${editjobs}  Permission to edit a job
${deletejobs}  Permission to delete a job
${runjobs}  Permission to run a job manually
${viewjobhistory}  Permission to view history of a job
${viewjobhistorydetails}  Permission to view details of an entry in job history
${viewjobs}  Permission to view list of notification templates
${createnotifications}  Permission to create notification templates
${editnotifications}  Permission to edit notification templates
${deletenotifications}  Permission to delete notification templates

${viewuserattributes}  Permission to view list of user attributes
${createuserattributes}  Permission to create user attributes
${edituserattributes}  Permission to edit user attributes
${deleteuserattributes}  Permission to delete user attributes


*** Keywords ***

SEARCH TOOL WORKING VALIDATION
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${portal_roles_button}  ${TIMEOUT_20}
    Click Element    ${portal_roles_button}
    Wait Until Page Contains Element    ${search}  ${TIMEOUT_20}
    Input Text    ${search}  ${viewlicenses_role_search}
    Wait Until Page Contains Element    ${viewlicenses_role}  ${TIMEOUT_20}
    Log to console  Search tool working properly
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

PORTAL ROLES SECTION DISPLAY VALIDATION
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element  ${portal_roles_button}  ${TIMEOUT_20}
    click element  ${portal_roles_button}
    Wait Until Page Contains  Portal Roles  ${TIMEOUT_20}
    Wait Until Page Contains  All Portal Roles  ${TIMEOUT_20}
    Page Should Contain Element  ${size}
    Page Should Contain Element  ${range_actions}
    Page Should Contain Element  ${search}
    Page Should Contain  ${name}
    Page Should Contain  ${type}
    Page Should Contain  ${description}
    Page Should Contain  ${createdontime}
    Page Should Contain  ${last_updated_time}
    Page Should Contain Element  ${edit}
    Page Should Contain Element  ${delete}
    Log to Console  Portal roles section displayed correctly
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

CREATE ROLE WITH ANY PERMISSION
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${random_role}  Generate Random String
    CREATE ROLE WITH ANY PERMISSION GEN  ${random_role}
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

MANDATORY FIELDS FOR NEW ROLE PERMISSION VALIDATION
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${random_role}  Generate Random String
    CREATE ROLE WITH ANY PERMISSION GEN  ${random_role}
    TEST SETUP
    Wait Until Page Contains Element  ${portal_roles_button}  ${TIMEOUT_20}
    click element  ${portal_roles_button}
    Wait Until Page Contains Element  ${create_role_button}  ${TIMEOUT_20}
    click element  ${create_role_button}
    Wait Until Page Contains Element    ${new_role_input}  ${TIMEOUT_20}
    Input Text    ${new_role_input}  ${new_role}
    Click Element    ${create_button}
    Wait Until Element Is Visible    ${permission_is_required_to_create_role_error}  ${TIMEOUT_20}
    Log to console  Permission is required to create role
    TEST SETUP
    Wait Until Page Contains Element  ${portal_roles_button}  ${TIMEOUT_20}
    click element  ${portal_roles_button}
    Wait Until Page Contains Element  ${create_role_button}  ${TIMEOUT_20}
    click element  ${create_role_button}
    Click Element    xpath=//*[contains(text(), "${user_attributes_01}")]
    Wait Until Page Contains Element  xpath=//*[contains(text(), "${permission_to_delete_user_attributes1}")]  ${TIMEOUT_20}
    Scroll element into view  xpath=//*[contains(text(), "${permission_to_delete_user_attributes1}")]
    Click Element    xpath=//*[contains(text(), "${permission_to_delete_user_attributes1}")]
    Click Element    ${create_button}
    Wait Until Element Is Visible    ${role_name_is_required_error}
    Log to console  Role name is required
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

CREATE ROLE WITH A DUPLICATED NAME
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element  ${portal_roles_button}  ${TIMEOUT_20}
    click element  ${portal_roles_button}
    ${role_name}  Get text  ${first_role_name}
    Log to console  Role name is ${role_name}
    TEST SETUP
    CREATE ROLE WITH ANY PERMISSION GEN  ${role_name}
    Wait Until Page Contains  ${item_already_exists_error}  ${TIMEOUT_20}
    Log to console  Item already exists
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

PORTAL ROLE NAME VALUE VALIDATION
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    CREATE ROLE WITH ANY PERMISSION GEN  -
    Wait Until Element Is Visible  ${role_value_error}  ${TIMEOUT_20}
    Log to console  Role name should contain only alpha numeric characters
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

PAGINATION WORKING VALIDATION
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element  ${portal_roles_button}  ${TIMEOUT_20}
    click element  ${portal_roles_button}
    Wait Until Page Contains Element  ${next_page_button}
    ${first_page_count}  Get text  ${range_indicator}
    Log to console  First page count: ${first_page_count}
    Should contain  ${first_page_count}  1 - 10
    click element  ${next_page_button}
    ${next_page_count}  Get text  ${range_indicator}
    Log to console  Next page count: ${next_page_count}
    Should contain  ${next_page_count}  11 - 20
    click element  ${last_page_button}
    ${last_page_count}  Get text  ${range_indicator}
    Log to console  Last page count: ${last_page_count}
    click element  ${previous_page_button}
    click element  ${first_page_button}
    Click Element    ${page_range_selector}
    Click Element    ${view_20_per_page}
    ${first_page_count_20_per_page}  Get text  ${range_indicator}
    Log to console  First page count for 20 roles: ${first_page_count_20_per_page}
    Should contain  ${first_page_count_20_per_page}  1 - 20
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

ROLE CRATION WITH REQUIRED FIELDS
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${random_role}  Generate Random String
    CREATE ROLE WITH ANY PERMISSION GEN  ${random_role}
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

PORTAL ROLE VALIDATION IN CREATE PORTAL USER
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${random_role}  Generate Random String
    CREATE ROLE WITH ANY PERMISSION GEN  ${random_role}
    TEST SETUP
    Wait Until Page Contains Element  ${PORTAL_USERS}  ${TIMEOUT_20}
    click element  ${PORTAL_USERS}
    Wait Until Page Contains Element  ${create_portal_user}  ${TIMEOUT_20}
    click element  ${create_portal_user}
    Wait Until Page Contains  ${random_role}
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

CREATE PORTAL ROLE VERIFICATION
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    ${random_role}  Generate Random String
    CREATE ROLE WITH ANY PERMISSION GEN  ${random_role}
    Wait Until Page Contains  Success  ${TIMEOUT_20}
    Wait Until Page Contains Element  ${search}  ${TIMEOUT_20}
    Input Text    ${search}    ${random_role}
    Wait Until Page Contains Element  xpath=//*[contains(text(), "${random_role}")]
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

CREATE PORTAL ROLE WINDOW DISPLAYED
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${portal_roles_button}  ${TIMEOUT_20}
    Click Element    ${portal_roles_button}
    Wait Until Page Contains Element    ${create_role_button}
    Click Link    ${create_role_button}
    Page Should Contain Element  ${create_portal_role_element}
    Page Should Contain Element  ${create_portal_role_close_button_element}
    Page Should Contain Element  ${create_portal_role_name_required_element}
    Page Should Contain Element  ${create_portal_role_descriptio_not_required}
    Page Should Contain Element  ${create_role_permission_required_element}
    Page Should Contain Element  ${create_role_permission_require_element_asterick}
    Page Should Contain Element  ${cancel_button_displayed_enabled_element}
    Page Should Contain Element  ${create_button_displayed_disabled}
    Log to Console  Portal role element displayed correctly
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

CREATE PORTAL ROLE WINDOW CLOSED WITHOUT CHANGES
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${portal_roles_button}  ${TIMEOUT_20}
    Click Element    ${portal_roles_button}
    Wait Until Page Contains Element    ${create_role_button}  ${TIMEOUT_20}
    Click Link    ${create_role_button}
    Wait Until Page Contains Element    ${close_create_portal_x}  ${TIMEOUT_20}
    Click Element    ${close_create_portal_x}
    Wait Until Page Contains Element    ${create_role_button}  ${TIMEOUT_20}
    Click Link    ${create_role_button}
    Wait Until Page Contains Element    ${close_create_portal_cancel_button}  ${TIMEOUT_20}
    Click Element    ${close_create_portal_cancel_button}
    Log to Console  Portal role element window closed without changes
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

PERMISSIOS SECTION VALIDATION
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element  ${PORTAL_ROLES}  ${TIMEOUT_20}
    click element  ${PORTAL_ROLES}
    Wait Until Page Contains Element  ${create_role_button}  ${TIMEOUT_20}
    click element  ${create_role_button}
    Wait Until Page Contains Element    ${new_role_input}  ${TIMEOUT_20}
    Builtin.sleep  1

    Click Element  //mat-panel-title[contains(text(),'${application_definition_01}')]
    Wait Until Page Contains  ${viewapplicationdefinitions}
    Wait Until Page Contains  ${createapplicationdefinitions}
    Wait Until Page Contains  ${editapplicationdefinitions}
    Wait Until Page Contains  ${deleteapplicationdefinitions}
    Click Element  //mat-panel-title[contains(text(),'${application_definition_01}')]

    Click Element  //mat-panel-title[contains(text(),'${configuration_profile_01}')]
    Wait Until Page Contains  ${viewconfigprofile}
    Wait Until Page Contains  ${createconfigprofile}
    Wait Until Page Contains  ${editconfigprofile}
    Wait Until Page Contains  ${deleteconfigprofile}
    Click Element  //mat-panel-title[contains(text(),'${configuration_profile_01}')]

    Click Element  //mat-panel-title[contains(text(),'${devices_01}')]
    Wait Until Page Contains  ${viewdevice}
    Wait Until Page Contains  ${deletedevice}
    Click Element  //mat-panel-title[contains(text(),'${devices_01}')]

    Click Element  //mat-panel-title[contains(text(),'${device_users_01}')]
    Wait Until Page Contains  ${viewdeviceusers}
    Wait Until Page Contains  ${deletedeviceusers}
    Wait Until Page Contains  ${editdeviceusers}
    Wait Until Page Contains  ${deletedeviceusers}
    Wait Until Page Contains  ${bulkimportdeviceusers}
    Click Element  //mat-panel-title[contains(text(),'${device_users_01}')]

    Click Element  //mat-panel-title[contains(text(),'${extension_manager_01}')]
    Wait Until Page Contains  ${viewextension}
    Wait Until Page Contains  ${bulkimportextension}
    Click Element  //mat-panel-title[contains(text(),'${extension_manager_01}')]

    Click Element  //mat-panel-title[contains(text(),'${licenses_01}')]
    Wait Until Page Contains  ${viewlicense}
    Click Element  //mat-panel-title[contains(text(),'${licenses_01}')]

    Click Element  //mat-panel-title[contains(text(),'${portal_roles_01}')]
    Wait Until Page Contains  ${viewportalrole}
    Wait Until Page Contains  ${createportalrole}
    Wait Until Page Contains  ${editportalrole}
    Wait Until Page Contains  ${deleteportalrole}
    Click Element  //mat-panel-title[contains(text(),'${portal_roles_01}')]

    Click Element  //mat-panel-title[contains(text(),'${portal_users_01}')]
    Wait Until Page Contains  ${viewportaluser}
    Wait Until Page Contains  ${unlockportaluser}
    Wait Until Page Contains  ${editportaluser}
    Wait Until Page Contains  ${createportalusers}
    Wait Until Page Contains  ${deleteportaluser}
    Wait Until Page Contains  ${changeportaluserpassword}
    Click Element  //mat-panel-title[contains(text(),'${portal_users_01}')]

    Click Element  //mat-panel-title[contains(text(),'${reports_01}')]
    Wait Until Page Contains  ${viewreporttemplate}
    Wait Until Page Contains  ${generatereport}
    Wait Until Page Contains  ${exportreport}
    Wait Until Page Contains  ${createreporttemplate}
    Wait Until Page Contains  ${editreporttemplate}
    Wait Until Page Contains  ${deletereporttemplate}
    Click Element  //mat-panel-title[contains(text(),'${reports_01}')]

    Click Element  //mat-panel-title[contains(text(),'${rules_01}')]
    Wait Until Page Contains  ${viewrules}
    Wait Until Page Contains  ${createrules}
    Wait Until Page Contains  ${editrules}
    Wait Until Page Contains  ${deleterules}
    Wait Until Page Contains  ${publishrules}
    Click Element  //mat-panel-title[contains(text(),'${rules_01}')]

    Click Element  //mat-panel-title[contains(text(),'${users_group_import_01}')]
    Wait Until Page Contains  ${viewjobs}
    Wait Until Page Contains  ${createjobs}
    Wait Until Page Contains  ${editjobs}
    Wait Until Page Contains  ${deletejobs}
    Wait Until Page Contains  ${runjobs}
    Wait Until Page Contains  ${viewjobhistory}
    Wait Until Page Contains  ${viewjobhistorydetails}
    Wait Until Page Contains  ${viewjobs}
    Wait Until Page Contains  ${createnotifications}
    Wait Until Page Contains  ${editnotifications}
    Wait Until Page Contains  ${deletenotifications}
    Click Element  //mat-panel-title[contains(text(),'${users_group_import_01}')]

    Click Element  //mat-panel-title[contains(text(),'${user_attributes_01}')]
    Wait Until Page Contains  ${viewuserattributes}
    Wait Until Page Contains  ${createuserattributes}
    Wait Until Page Contains  ${edituserattributes}
    Wait Until Page Contains  ${deleteuserattributes}
    Click Element  //mat-panel-title[contains(text(),'${user_attributes_01}')]

    Log to console  Permissions section displayed correctly
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN
