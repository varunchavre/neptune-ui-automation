*** Settings ***

# Library         Selenium2Library
Library         ExtendedSelenium2Library
Library         String
Library         Dialogs
Library         DateTime
Library         BuiltIn
Library         Collections
Resource        ${EXECDIR}/Res/Web/GenericFunction.robot
Variables       ${EXECDIR}/VariableFiles/Web/Config.yaml


*** Variables ***

${portal_users_tab}                      xpath=(//mat-card[@class="mat-card"])[6]
${portal_user_create_user}             //a[@class="link-btn ng-star-inserted"]
${portal_user_enter_username}          //input[@ng-reflect-name="username"]
${portal_user_enter_email}             //input[@ng-reflect-name="email"]
${portal_user_enter_password}          //input[@ng-reflect-name="password"]
${portal_user_enter_confirm_password}  //input[@ng-reflect-name="confirmPassword"]
${portal_user_search_for_role}         //input[@ng-reflect-name="search"]
${portal_user_user_role_field}         //div[@class="custom-selection-list"]
${portal_user_searched_roles}          xpath=(//*[contains(text(), "
#${portal_user_searched_user_roles}     xpath=(//*[contains(text(), "such")])

${portal_user_cancel_createuser}      //button[@class="mat-button mat-raised-button"]
${portal_user_create_the_user}         //button[@class="mat-raised-button mat-primary"]

${portal_user_search_option}           //select[@placeholder="Search Column"]
${portal_user_search}                  //input[@ng-reflect-name="query"]
${portal_user_search_bar}              //input[@ng-reflect-name="query"]
${portal_user_pagination_bar}          //mat-paginator[@class="mat-paginator"]

${portal_user_checkbox1}               xpath=(//div[@class="mat-checkbox-inner-container mat-checkbox-inner-container-no-side-margin"])[2]
${portal_user_checkbox2}               xpath=(//div[@class="mat-checkbox-inner-container mat-checkbox-inner-container-no-side-margin"])[3]
${portal_user_delete_selected_users}   //a[@class='link-btn ng-star-inserted']//i[@class='fa fa-trash']
${portal_user_delete_icon}             //i[@class="fa fa-trash"]
${portal_user_edit_icon}               //i[@class="fa fa-pencil"]
${portal_user_lock_icon}               //i[@class="fa fa-lock"]
${portal_user_locked}                  //mat-cell[@class="mat-cell cdk-column-locked mat-column-locked ng-star-inserted"]
${portal_user_table_username}          //mat-cell[@class="mat-cell cdk-column-username mat-column-username ng-star-inserted"]
${portal_user_table_username2}         xpath=(//mat-cell[@class="mat-cell cdk-column-username mat-column-username ng-star-inserted"])[2]
${portal_user_created_on_time}         //mat-cell[@class="mat-cell cdk-column-createdOnTime mat-column-createdOnTime ng-star-inserted"]
${portal_user_last_updated_time}       //mat-cell[@class="mat-cell cdk-column-lastUpdatedTime mat-column-lastUpdatedTime ng-star-inserted"]
${portal_user_status_notification}     //div[@class="notification-title"]
#${portal_user_delete_user_yes}         //button[@class='mat-raised-button mat-primary ng-star-inserted']
${portal_user_delete_user_yes}         //button[@class="mat-raised-button mat-primary ng-star-inserted cdk-focused cdk-program-focused"]
${portal_user_password_error_message}  //div[@ngxerrors='password']//div[@ngxerror='pattern']
${portal_user_table}                   //div[@class='row min-height-500 middle-xs ng-star-inserted']
${portal_user_edit_username}           //input[@formcontrolname="username"]
${portal_user_edit_email}              //input[@formcontrolname="email"]
${portal_user_edit_user_roles_list}    //mat-selection-list[@formcontrolname="permissionLevels"]
${portal_user_incorrect_username_error_message}   //div[@ngxerrors='username']//div[@ngxerror='pattern']

${portal_user_lock_button}    //button[@class="mat-button mat-raised-button ng-star-inserted"]
${portal_user_cancel_button}  //button[@class="mat-button mat-raised-button"]
${portal_user_update_button}  //button[@class="mat-raised-button mat-primary"]
${portal_user_toggle_button1}  //div[@class="mat-list-text"]
${portal_user_toggle_button2}  xpath=(//div[@class="mat-list-text"])[2]
${change_password_save_button}  //button[@type='submit']

${change_password_enter_current_password}      //input[@ng-reflect-name="currentPassword"]
${change_password_enter_new_password}          //input[@ng-reflect-name="newPassword"]
${change_password_enter_new_confirm_password}  //input[@ng-reflect-name="newPasswordConfirm"]
${system_management_breadcrumb}                //a[@href="/system-management"]
${system_management_dashboard_title}           //h1[@class='padding-top-10 padding-left-10']
${reset_the_password}                          //span[contains(text(),'Reset')]
${portal_user_pwdreset_manual_reset}           xpath=(//div[@class="mat-tab-label-content"])
${portal_user_pwdreset_email_link}             xpath=(//div[@class="mat-tab-label-content"])[2]
${portal_user_username_error_message}          //div[@ng-reflect-control-name="username"]
${portal_user_max_length_username_error_message}  //div[@ng-reflect-control-name="username"]


*** Keywords ***


VALIDATE ALL PORTAL USERS TABLE ATTRIBUTES
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO PORTAL USERS TAB
    sleep  1.0
    Page Should Contain  Portal User Name
    Page Should Contain  Locked
    Page Should Contain  CreatedOnTime
    Page Should Contain  Last Updated Time
    Page Should Contain Element  ${portal_user_pagination_bar}
    Page Should Contain Element  ${portal_user_search_bar}
    Page Should Contain Element  ${portal_user_create_user}
    Log to console  all portal users table attributes are validated
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

VALIDATE SYSTEM ADMIN ALL USER PORTAL USERS TABLE
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO PORTAL USERS TAB
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  2.0
    Input Text  ${portal_user_search}  admin
    #Wait Until Element Contains  //mat-row[@class="mat-row ng-star-inserted"]  ${TIMEOUT_20}
    sleep  2.0
    Element Text Should Be  ${portal_user_table_username}  admin
    Page Should Contain Element  ${portal_user_locked}   ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_created_on_time}    ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_last_updated_time}   ${TIMEOUT_20}
    Log to console  system admin in all portal users table are validated
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

VALIDATE CREATE PORTAL USER FIELDS
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO PORTAL USERS TAB
    Wait Until Page Contains Element  ${portal_user_create_user}  ${TIMEOUT_20}
    Click Link  ${portal_user_create_user}
    Wait Until Page Contains  Create Portal User  ${TIMEOUT_20}
    Page Should Contain  UserName
    Element Should Be Enabled  ${portal_user_enter_username}
    Page Should Contain  Email
    Element Should Be Enabled  ${portal_user_enter_email}
    Page Should Contain  Password
    Element Should Be Enabled  ${portal_user_enter_password}
    Page Should Contain  Confirm Password
    Element Should Be Enabled  ${portal_user_enter_confirm_password}
    Page Should Contain  User Roles
    Element Should Be Enabled  ${portal_user_search_for_role}
    Element Should Be Visible  ${portal_user_user_role_field}
    Element Should Be Enabled  ${portal_user_cancel_createuser}
    Element Should Be Disabled  ${portal_user_create_the_user}
    Log to console  create portal user fields are validated
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CREATE PORTAL USER WITH ANY ROLE
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  Newuser@123
    CREATE PORTAL USER SELECT ROLES  View Only
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  Success
    Log to console  created new admin in portal user with single role
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN



CREATE PORTAL USER WITH MANDATORY FIELDS
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  Newuser@123
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  Success
    Log to console  created new admin in portal user with mandatory fields
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CREATE PORTAL USER WITH MISSING MANDATORY FIELDS
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${EMPTY}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Element Should Be Disabled  ${portal_user_create_the_user}
    Log to console  could not create new admin with missing mandatory fields
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CREATE NEW PORTAL USER WITH ANY ROLE AND VALIDATE
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  Newuser@123
    CREATE PORTAL USER SELECT ROLES  View Only
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  Success
    Wait Until Page Contains  Show All  ${TIMEOUT_20}
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Input Text  ${portal_user_search}  ${portal_user_username}
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  3.0
    Element Text Should Be  ${portal_user_table_username}  ${portal_user_username}
    Page Should Contain Element  ${portal_user_checkbox1}   ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_edit_icon}   ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_delete_icon}   ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_lock_icon}   ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_created_on_time}    ${TIMEOUT_20}
    Page Should Contain Element  ${portal_user_last_updated_time}   ${TIMEOUT_20}
    Log to console  created new admin in portal user with any role and validated
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CREATE PORTAL USER WITH EMPTY PASSWORD
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${EMPTY}
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Element Should Be Disabled  ${portal_user_create_the_user}
    Input Text  ${portal_user_enter_password}  ${portal_user_password}
    #Press Combination  KEY.TAB
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Element Should Be Disabled  ${portal_user_create_the_user}
    Log to console  could not create new admin due to empty password
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN





CREATE PORTAL USER WITH ONLY SPACES IN THE PASSWORD
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  ${SPACE}
    Wait Until Element Is Visible  ${portal_user_password_error_message}
    Log to console  password Should not contain only blank spaces
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN



CREATE PORTAL USER WITH INCORRECT PASSWORD
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  ${EMPTY}  New${SPACE}${SPACE}
    Wait Until Element Is Visible  ${portal_user_password_error_message}
    Log to console  could not create new admin due to incorrect password (with blank spaces)
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CREATE PORTAL USER WITH REPEATED USERNAME
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO PORTAL USERS TAB
    ${portal_user_username}  Get Text  ${portal_user_table_username}
    CREATE PORTAL USER INPUTS  ${portal_user_username}  Newuser@123
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  Portal User Add Fail
    Log to console  cannot create new admin in portal user due to repeated username
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CREATE PORTAL USER WITH INCORRECT USERNAME
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO PORTAL USERS TAB
    CREATE PORTAL USER INPUTS  Newuser@123  Newuser@123
    Wait Until Element Is Visible  ${portal_user_username_error_message}
    Log to console  cannot create new admin in portal user due to incorrect username
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CREATE PORTAL USER WITH MORE THAN 255 CHARACTERS USERNAME
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO PORTAL USERS TAB
    ${255_char_string}  Generate Random String  251  [NUMBERS]
    CREATE PORTAL USER INPUTS  User_${255_char_string}  Newuser@123
    Wait Until Element Is Visible  ${portal_user_max_length_username_error_message}
    Log to console  cannot create new admin in portal user due to username exceeds max length of 255 characters
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN



DELETE ANOTHER ADMIN USING TRASH ICON
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO PORTAL USERS TAB

    CREATE PORTAL USER INPUTS  ${EMPTY}  Newuser@123
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  Success
    Reload Page
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}

    ${portal_user_username}  Get Text  ${portal_user_table_username}
    Click Element  ${portal_user_delete_icon}
    Wait Until Page Contains  Delete PortalUser  ${TIMEOUT_20}
    Click Element  ${portal_user_delete_user_yes}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  Delete Success
    Wait Until Page Contains  Show All  ${TIMEOUT_20}
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Input Text  ${portal_user_search}  ${portal_user_username}
    Sleep  2.0
    Element Should Not Contain  ${portal_user_table}  ${portal_user_username}
    Log to console  deleted another admin using trash icon
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


DELETE ANOTHER ADMIN USING CHECK BOX
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO PORTAL USERS TAB

    CREATE PORTAL USER INPUTS  ${EMPTY}  Newuser@123
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  Success
    Reload Page
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}

    ${portal_user_username}  Get Text  ${portal_user_table_username}
    Click Element  ${portal_user_checkbox1}
    Click Element  ${portal_user_delete_selected_users}
    Wait Until Page Contains  Delete Portal Users  ${TIMEOUT_20}
    Click Element  ${portal_user_delete_user_yes}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  Delete Success
    Wait Until Page Contains  Show All  ${TIMEOUT_20}
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Input Text  ${portal_user_search}  ${portal_user_username}
    Sleep  3.0
    Element Should Not Contain  ${portal_user_table}  ${portal_user_username}
    Log to console  deleted another admin using check box
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN



DELETE MULTIPLE ADMIN USING CHECK BOX
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO PORTAL USERS TAB

    CREATE PORTAL USER INPUTS  ${EMPTY}  Newuser@123
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  Success
    Reload Page
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}

    CREATE PORTAL USER INPUTS  ${EMPTY}  Newuser@123
    Wait Until Page Contains Element  ${portal_user_create_the_user}  ${TIMEOUT_20}
    Click Element    ${portal_user_create_the_user}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  Success
    Reload Page
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}

    ${portal_user_username}  Get Text  ${portal_user_table_username}
    ${portal_user_username2}  Get Text  ${portal_user_table_username2}
    Wait Until Element is Visible  ${portal_user_checkbox1}  ${TIMEOUT_20}
    Click Element  ${portal_user_checkbox1}
    Click Element  ${portal_user_checkbox2}
    Click Element  ${portal_user_delete_selected_users}
    Wait Until Page Contains  Delete Portal Users  ${TIMEOUT_20}
    Click Element  ${portal_user_delete_user_yes}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  Delete Success
    Wait Until Page Contains  Show All  ${TIMEOUT_20}
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Input Text  ${portal_user_search}  ${portal_user_username}
    Sleep  2.0
    Element Should Not Contain  ${portal_user_table}  ${portal_user_username}
    Clear Element Text  ${portal_user_search}
    Reload Page
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Input Text  ${portal_user_search}  ${portal_user_username2}
    Sleep  3.0
    Element Should Not Contain  ${portal_user_table}  ${portal_user_username2}
    Log to console  deleted multiple admin using using checkbox
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


DELETE SYSTEM ADMIN
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO PORTAL USERS TAB
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    sleep  2.0
    Input Text  ${portal_user_search}  admin
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  3.0
    Element Text Should Be  ${portal_user_table_username}  admin
    Click Element  ${portal_user_checkbox1}
    Click Element  ${portal_user_delete_selected_users}
    Wait Until Page Contains  Delete Portal Users  ${TIMEOUT_20}
    Page Should Contain  Cannot delete system users
    Click Element  ${portal_user_delete_user_yes}
    Log to console  cannot delete system admin
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


VALIDATE BREADCRUMBS REDIRECT TO CORRECT PAGE
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO SPECIFIED PAGE FROM DASHBOARD  User Attributes
    Click Link  ${system_management_breadcrumb}
    Wait Until Page Contains Element  ${system_management_breadcrumb}  ${TIMEOUT_20}
    Element Text Should Be  ${system_management_breadcrumb}  System Management
    Wait Until Page Contains Element  ${system_management_dashboard_title}  ${TIMEOUT_20}
    Element Text Should Be  ${system_management_dashboard_title}  System Management
    Log to console  redirected to specified page according to selection of breadcrumb
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


VALIDATE SYSTEM MANAGEMENT DASHBOARD
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO PORTAL USERS TAB
    Click Link  ${system_management_breadcrumb}
    Wait Until Page Contains Element  ${system_management_breadcrumb}  ${TIMEOUT_20}
    Element Text Should Be  ${system_management_breadcrumb}  System Management
    Wait Until Page Contains Element  ${system_management_dashboard_title}  ${TIMEOUT_20}
    Element Text Should Be  ${system_management_dashboard_title}  System Management
    Log to console  system management dashboard is displayed properly on clicking System Management Breadcrumb
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


VALIDATE EDIT PORTAL USER
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO PORTAL USERS TAB
    Click Element  ${portal_user_edit_icon}
    Wait Until Page Contains  Edit Portal User  ${TIMEOUT_20}
    Page Should Contain  UserName
    Element Should Be Disabled  ${portal_user_edit_username}
    Page Should Contain  Email
    Element Should Be Enabled  ${portal_user_edit_email}
    Page Should Contain  User Roles
    Page Should Contain Element  ${portal_user_edit_user_roles_list}
    Element Should Be Enabled  ${portal_user_lock_button}
    Element Should Be Enabled  ${portal_user_cancel_button}
    Element Should Be Disabled  ${portal_user_update_button}
    Log to console  edit portal user fields validated
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN



EDIT ANOTHER ADMIN IN PORTAL USER
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO PORTAL USERS TAB
    Click Element  ${portal_user_edit_icon}
    Wait Until Page Contains  Edit Portal User  ${TIMEOUT_20}
    ${random_2digit}  Generate Random String  2  [NUMBERS]
    Input Text  ${portal_user_edit_email}  LT${random_2digit}@gmail.com
    Click Element  ${portal_user_toggle_button1}
    Click Element  ${portal_user_toggle_button2}
    Click Element  ${portal_user_update_button}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  Success
    Log to console  edited another admin portal user
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

EDIT OWN ADMIN IN PORTAL USER
    ${RESULT}  Set Variable  FAIL
    LOGOUT FROM NEPTUNE
    LOGIN TO NEPTUNE  test123  Newuser@123
    GO TO PORTAL USERS TAB
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Wait Until Page Contains  Portal User Name  ${TIMEOUT_20}
    Wait Until Element Is Visible  ${portal_user_search}  ${TIMEOUT_20}
    sleep  2.0
    Input Text  ${portal_user_search}  test123
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  2.0
    Element Text Should Be  ${portal_user_table_username}  test123
    Click Element  ${portal_user_edit_icon}
    Wait Until Page Contains  Edit Portal User  ${TIMEOUT_20}
    ${random_2digit}  Generate Random String  2  [NUMBERS]
    Input Text  ${portal_user_edit_email}  LT${random_2digit}@gmail.com
    Click Element  ${portal_user_toggle_button1}
    Click Element  ${portal_user_toggle_button2}
    Click Element  ${portal_user_update_button}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  Success
    LOGOUT FROM NEPTUNE
    Log to console  edited own admin portal user
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


SEARCH ADMIN IN PORTAL USER
    [Arguments]  ${portal_username}=admin
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    GO TO PORTAL USERS TAB
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  2.0
    Input Text  ${portal_user_search}  ${portal_username}
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    sleep  3.0
    Element Text Should Be  ${portal_user_table_username}  ${portal_username}
    Log to console  searched admin present in the portal user table
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

CHANGE OWN ADMIN PASSWORD WITH EMPTY PASSWORD
    [Arguments]  ${portal_username}=admin
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  Dashboard  ${TIMEOUT_20}
    GO TO CHANGE PASSWORD
    Clear Element Text  ${change_password_enter_current_password}
    Input Text  ${change_password_enter_current_password}  ${PSWRD}
    Element should be disabled  ${change_password_save_button}
    Clear Element Text  ${change_password_enter_new_password}
    Input Text  ${change_password_enter_new_password}  Newusers@1234
    Element should be disabled  ${change_password_save_button}
    Clear Element Text  ${change_password_enter_new_password}
    Clear Element Text  ${change_password_enter_new_confirm_password}
    Input Text  ${change_password_enter_new_confirm_password}   Newusers@12345
    Element should be disabled  ${change_password_save_button}
    Log to console  cannot change password with an empty password
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN



CHANGE OWN ADMIN PASSWORD WITH INCORRECT PASSWORD
    [Arguments]  ${portal_username}=admin
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  Dashboard  ${TIMEOUT_20}
    GO TO CHANGE PASSWORD
    CHANGE PASSWORD INPUTS  Newuser1  Newuser1
    Wait until element is visible  //div[@ng-reflect-ngx-error="pattern"]
    Log to console  cannot change password with an incorrect password
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN


CHANGE OWN ADMIN PASSWORD WITH OLD PASSWORD
    [Arguments]  ${portal_username}=admin
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  Dashboard  ${TIMEOUT_20}
    GO TO CHANGE PASSWORD
    CHANGE PASSWORD INPUTS  ${PSWRD}  ${PSWRD}
    Click Element  ${change_password_save_button}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  Reset Password Fail
    Log to console  cannot change password with an old password
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN



RESET PORTAL USER PASSWORD USING MANUAL RESET OPTION
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  Dashboard  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    Click Element    ${portal_user_lock_icon}
    Wait Until Page Contains  Reset Password  ${TIMEOUT_20}
    Click Element    ${portal_user_pwdreset_manual_reset}
    Wait Until Page Contains  New Password  ${TIMEOUT_20}
    ${no}  Generate Random String  4  [NUMBERS]
    Input Text    ${change_password_enter_new_password}  Newuser@${no}
    Input Text    ${portal_user_enter_confirm_password}  Newuser@${no}
    Click Element  ${reset_the_password}
    Wait Until Page Contains Element  ${portal_user_status_notification}  ${TIMEOUT_20}
    Element Text Should Be  ${portal_user_status_notification}  Success
    Log to console  portal user password reset successfully using manual reset option
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

RESET OWN PORTAL USER PASSWORD
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains  Dashboard  ${TIMEOUT_20}
    GO TO PORTAL USERS TAB
    Select From List By Value  ${portal_user_search_option}  username
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    Sleep  2.0
    Input Text  ${portal_user_search}  ${Login}
    Wait Until Page Contains Element  ${portal_user_edit_icon}  ${TIMEOUT_20}
    sleep  2.0
    Element Text Should Be  ${portal_user_table_username}  ${Login}
    Page Should Not Contain Element  ${portal_user_lock_icon}
    Log to console  cannot reset password of currently logged-in user.
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN