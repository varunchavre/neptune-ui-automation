*** Settings ***

Library         ExtendedSelenium2Library
Library         String
Library         Dialogs
Library         DateTime
Variables       ${EXECDIR}/VariableFiles/Web/Config.yaml
Resource        ${EXECDIR}/Res/Web/GenericFunction.robot

*** Variables ***

${password_security_username}  a
${password_security_password_uppercase}  newuser@1
${password_security_password_lowercase}  NEWUSER@1
${password_security_password_one_digit}  newuser@
${password_security_password_special_char}  newuser1
${password_security_password_min_10_char}  Newuser@1
${password_security_password_max_128_char}  Newuser@1234567891234567891234567891234567891234567891234567891234567891234567891234567891234567891234567891234567891234567891234
${password_security_password_2_identical_char}  Newuser@1222
${password_security_password_2_out_of_4}  newuser1111
${settings_button}  //mat-icon[@class="mat-icon material-icons ng-star-inserted"]
${portal_users}  //a[@title='Click here to manage portal roles']//mat-card[@class='mat-card']
${create_portal_user}  //a[@href="/system-management/portal-users/list/add"]
${new_username}  //input[@ng-reflect-name="username"]
${new_user_password}  //input[@ng-reflect-name="password"]
${new_user_password_confirm}  //input[@ng-reflect-name="confirmPassword"]
${new_user_password_error}  xpath=(//mat-error[@class="mat-error"])[6]
${reset_password_button}  //i[@class="fa fa-lock"]
${update_password_error_message}  //mat-error[@class="text-right mat-error"]
${new_password_input}  //input[@ng-reflect-name="newPassword"]
${user_logged_in_icon}  //a[@class="user-name"]
${change_password_icon}  xpath=(//mat-icon[@class="mat-icon material-icons"])[7]
${current_password_field}  //input[@ng-reflect-name="currentPassword"]
${reenter_new_password}  //input[@ng-reflect-name="newPasswordConfirm"]
${wrong_old_password}  newuser
${new_password}  Newuser@1234
${save_button}  //button[@type='submit']
${current_password_incorrect_error}  Current password is incorrect
${forgot_password}  //a[@class='block link text-small margin-bottom-15']
${forgot_password_email_input}  //input[@id='email']
${invalid_email}  a
${reset_button}  //span[contains(text(),'Reset')]
${valid_email_error}  //mat-error[contains(text(), "Valid Email is required")]
${not_registered_email}  a@gmail.com
${not_registered_email_error}  //mat-error[contains(text(), "Email not recognized")]

*** Keywords ***

UPPERCASE PASSWORD VALIDATION FOR NEW USER
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${portal_users}  ${TIMEOUT_20}
    Click Element    ${portal_users}
    Wait Until Page Contains Element    ${create_portal_user}  ${TIMEOUT_20}
    Click Link    ${create_portal_user}
    Wait Until Page Contains Element    ${new_username}  ${TIMEOUT_20}
    Input Text    ${new_username}    ${password_security_username}
    Wait Until Page Contains Element    ${new_user_password}  ${TIMEOUT_20}
    Input Text    ${new_user_password}    ${password_security_password_uppercase}
    Wait Until Page Contains Element    ${new_user_password_confirm}  ${TIMEOUT_20}
    Input Text    ${new_user_password_confirm}    ${password_security_password_uppercase}
    Wait Until Element Is Visible  ${new_user_password_error}  ${TIMEOUT_20}
    Log to console  Password should contain at least one uppercase
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

LOWERCASE PASSWORD VALIDATION FOR NEW USER
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${portal_users}  ${TIMEOUT_20}
    Click Element    ${portal_users}
    Wait Until Page Contains Element    ${create_portal_user}  ${TIMEOUT_20}
    Click Link    ${create_portal_user}
    Wait Until Page Contains Element    ${new_username}  ${TIMEOUT_20}
    Input Text    ${new_username}    ${password_security_username}
    Wait Until Page Contains Element    ${new_user_password}  ${TIMEOUT_20}
    Input Text    ${new_user_password}    ${password_security_password_lowercase}
    Wait Until Page Contains Element    ${new_user_password_confirm}  ${TIMEOUT_20}
    Input Text    ${new_user_password_confirm}    ${password_security_password_lowercase}
    Wait Until Element Is Visible  ${new_user_password_error}  ${TIMEOUT_20}
    Log to console  Password should contain at least one lowercase
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

ONE DIGIT PASSWORD VALIDATION FOR NEW USER
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${portal_users}  ${TIMEOUT_20}
    Click Element    ${portal_users}
    Wait Until Page Contains Element    ${create_portal_user}  ${TIMEOUT_20}
    Click Link    ${create_portal_user}
    Wait Until Page Contains Element    ${new_username}  ${TIMEOUT_20}
    Input Text    ${new_username}    ${password_security_username}
    Wait Until Page Contains Element    ${new_user_password}  ${TIMEOUT_20}
    Input Text    ${new_user_password}    ${password_security_password_one_digit}
    Wait Until Page Contains Element    ${new_user_password_confirm}  ${TIMEOUT_20}
    Input Text    ${new_user_password_confirm}    ${password_security_password_one_digit}
    Wait Until Element Is Visible  ${new_user_password_error}  ${TIMEOUT_20}
    Log to console  Password should contain at least one digit
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

SPECIAL CHARACTER PASSWORD VALIDATION FOR NEW USER
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${portal_users}  ${TIMEOUT_20}
    Click Element    ${portal_users}
    Wait Until Page Contains Element    ${create_portal_user}  ${TIMEOUT_20}
    Click Link    ${create_portal_user}
    Wait Until Page Contains Element    ${new_username}  ${TIMEOUT_20}
    Input Text    ${new_username}    ${password_security_username}
    Wait Until Page Contains Element    ${new_user_password}  ${TIMEOUT_20}
    Input Text    ${new_user_password}    ${password_security_password_special_char}
    Wait Until Page Contains Element    ${new_user_password_confirm}  ${TIMEOUT_20}
    Input Text    ${new_user_password_confirm}    ${password_security_password_special_char}
    Wait Until Element Is Visible  ${new_user_password_error}  ${TIMEOUT_20}
    Log to console  Password should contain at least one special character
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

MIN 10 CHARACTERS PASSWORD VALIDATION FOR NEW USER
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${portal_users}  ${TIMEOUT_20}
    Click Element    ${portal_users}
    Wait Until Page Contains Element    ${create_portal_user}  ${TIMEOUT_20}
    Click Link    ${create_portal_user}
    Wait Until Page Contains Element    ${new_username}  ${TIMEOUT_20}
    Input Text    ${new_username}    ${password_security_username}
    Wait Until Page Contains Element    ${new_user_password}  ${TIMEOUT_20}
    Input Text    ${new_user_password}    ${password_security_password_min_10_char}
    Wait Until Page Contains Element    ${new_user_password_confirm}  ${TIMEOUT_20}
    Input Text    ${new_user_password_confirm}    ${password_security_password_min_10_char}
    Wait Until Element Is Visible  ${new_user_password_error}  ${TIMEOUT_20}
    Log to console  Password should contain at least 10 characters
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

MAX 128 CHARACTERS PASSWORD VALIDATION FOR NEW USER
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${portal_users}  ${TIMEOUT_20}
    Click Element    ${portal_users}
    Wait Until Page Contains Element    ${create_portal_user}  ${TIMEOUT_20}
    Click Link    ${create_portal_user}
    Wait Until Page Contains Element    ${new_username}  ${TIMEOUT_20}
    Input Text    ${new_username}    ${password_security_username}
    Wait Until Page Contains Element    ${new_user_password}  ${TIMEOUT_20}
    Input Text    ${new_user_password}    ${password_security_password_max_128_char}
    Wait Until Page Contains Element    ${new_user_password_confirm}  ${TIMEOUT_20}
    Input Text    ${new_user_password_confirm}    ${password_security_password_max_128_char}
    Wait Until Element Is Visible  ${new_user_password_error}  ${TIMEOUT_20}
    Log to console  Password should contain at most 128 characters
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

IDENTICAL CHARACTERS PASSWORD VALIDATION FOR NEW USER
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${portal_users}  ${TIMEOUT_20}
    Click Element    ${portal_users}
    Wait Until Page Contains Element    ${create_portal_user}  ${TIMEOUT_20}
    Click Link    ${create_portal_user}
    Wait Until Page Contains Element    ${new_username}  ${TIMEOUT_20}
    Input Text    ${new_username}    ${password_security_username}
    Wait Until Page Contains Element    ${new_user_password}  ${TIMEOUT_20}
    Input Text    ${new_user_password}    ${password_security_password_2_identical_char}
    Wait Until Page Contains Element    ${new_user_password_confirm}  ${TIMEOUT_20}
    Input Text    ${new_user_password_confirm}    ${password_security_password_2_identical_char}
    Wait Until Element Is Visible  ${new_user_password_error}  ${TIMEOUT_20}
    Log to console  Password should contain more than 2 identical characters in a row
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN



UPPERCASE PASSWORD VALIDATION WHILE RESETTING
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${portal_users}  ${TIMEOUT_20}
    Click Element    ${portal_users}
    Wait Until Page Contains Element    ${reset_password_button}  ${TIMEOUT_20}
    Click Element    ${reset_password_button}
    Wait Until Page Contains Element    ${new_password_input}  ${TIMEOUT_20}
    Input Text    ${new_password_input}  ${password_security_password_uppercase}
    Wait Until Page Contains Element    ${new_user_password_confirm}  ${TIMEOUT_20}
    Input Text    ${new_user_password_confirm}  ${password_security_password_uppercase}
    wait Until Element Is Visible    ${update_password_error_message}
    Log to console  Password should contain at least one uppercase
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

LOWERCASE PASSWORD VALIDATION WHILE RESETTING
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${portal_users}  ${TIMEOUT_20}
    Click Element    ${portal_users}
    Wait Until Page Contains Element    ${reset_password_button}  ${TIMEOUT_20}
    Click Element    ${reset_password_button}
    Wait Until Page Contains Element    ${new_password_input}  ${TIMEOUT_20}
    Input Text    ${new_password_input}  ${password_security_password_lowercase}
    Wait Until Page Contains Element    ${new_user_password_confirm}  ${TIMEOUT_20}
    Input Text    ${new_user_password_confirm}  ${password_security_password_lowercase}
    wait Until Element Is Visible    ${update_password_error_message}
    Log to console  Password should contain at least one lowercase
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

ONE DIGIT PASSWORD VALIDATION WHILE RESETTING
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${portal_users}  ${TIMEOUT_20}
    Click Element    ${portal_users}
    Wait Until Page Contains Element    ${reset_password_button}  ${TIMEOUT_20}
    Click Element    ${reset_password_button}
    Wait Until Page Contains Element    ${new_password_input}  ${TIMEOUT_20}
    Input Text    ${new_password_input}    ${password_security_password_one_digit}
    Wait Until Page Contains Element    ${new_user_password_confirm}  ${TIMEOUT_20}
    Input Text    ${new_user_password_confirm}    ${password_security_password_one_digit}
    wait Until Element Is Visible    ${update_password_error_message}
    Log to console  Password should contain at least one digit
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

SPECIAL CHARACTER PASSWORD VALIDATION WHILE RESETTING
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${portal_users}  ${TIMEOUT_20}
    Click Element    ${portal_users}
    Wait Until Page Contains Element    ${reset_password_button}  ${TIMEOUT_20}
    Click Element    ${reset_password_button}
    Wait Until Page Contains Element    ${new_password_input}  ${TIMEOUT_20}
    Input Text    ${new_password_input}    ${password_security_password_special_char}
    Wait Until Page Contains Element    ${new_user_password_confirm}  ${TIMEOUT_20}
    Input Text    ${new_user_password_confirm}  ${password_security_password_special_char}
    wait Until Element Is Visible    ${update_password_error_message}
    Log to console  Password should contain at least one special character
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

MIN 10 CHARACTERS PASSWORD VALIDATION WHILE RESETTING
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${portal_users}  ${TIMEOUT_20}
    Click Element    ${portal_users}
    Wait Until Page Contains Element    ${reset_password_button}  ${TIMEOUT_20}
    Click Element    ${reset_password_button}
    Wait Until Page Contains Element    ${new_password_input}  ${TIMEOUT_20}
    Input Text    ${new_password_input}    ${password_security_password_min_10_char}
    Wait Until Page Contains Element    ${new_user_password_confirm}  ${TIMEOUT_20}
    Input Text    ${new_user_password_confirm}  ${password_security_password_min_10_char}
    wait Until Element Is Visible    ${update_password_error_message}
    Log to console  Password should contain at least 10 characters
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

MAX 128 CHARACTERS PASSWORD VALIDATION WHILE RESETTING
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${portal_users}  ${TIMEOUT_20}
    Click Element    ${portal_users}
    Wait Until Page Contains Element    ${reset_password_button}  ${TIMEOUT_20}
    Click Element    ${reset_password_button}
    Wait Until Page Contains Element    ${new_password_input}  ${TIMEOUT_20}
    Input Text    ${new_password_input}  ${password_security_password_max_128_char}
    Wait Until Page Contains Element    ${new_user_password_confirm}  ${TIMEOUT_20}
    Input Text    ${new_user_password_confirm}  ${password_security_password_max_128_char}
    wait Until Element Is Visible    ${update_password_error_message}
    Log to console  Password should contain at most 128 characters
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

IDENTICAL CHARACTERS PASSWORD VALIDATION WHILE RESETTING
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${portal_users}  ${TIMEOUT_20}
    Click Element    ${portal_users}
    Wait Until Page Contains Element    ${reset_password_button}  ${TIMEOUT_20}
    Click Element    ${reset_password_button}
    Wait Until Page Contains Element    ${new_password_input}  ${TIMEOUT_20}
    Input Text    ${new_password_input}  ${password_security_password_2_identical_char}
    Wait Until Page Contains Element    ${new_user_password_confirm}  ${TIMEOUT_20}
    Input Text    ${new_user_password_confirm}  ${password_security_password_2_identical_char}
    wait Until Element Is Visible    ${update_password_error_message}
    Log to console  Password should contain more than 2 identical characters in a row
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN



UPPERCASE PASSWORD VALIDATION WHILE CHANGING
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${user_logged_in_icon}  ${TIMEOUT_20}
    Click Element    ${user_logged_in_icon}
    Wait Until Page Contains Element    ${change_password_icon}  ${TIMEOUT_20}
    BuiltIn.sleep  1
    Click Element    ${change_password_icon}
    Wait Until Page Contains Element    ${current_password_field}  ${TIMEOUT_20}
    Input Text    ${current_password_field}  ${PASSWORD}
    Wait Until Page Contains Element    ${new_password_input}  ${TIMEOUT_20}
    Input Text    ${new_password_input}  ${password_security_password_uppercase}
    Wait Until Page Contains Element    ${reenter_new_password}  ${TIMEOUT_20}
    Input Text    ${reenter_new_password}  ${password_security_password_uppercase}
    Wait Until Element Is Visible    ${update_password_error_message}
    Log to console  Password should contain at least one uppercase
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

LOWERCASE PASSWORD VALIDATION WHILE CHANGING
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${user_logged_in_icon}  ${TIMEOUT_20}
    Click Element    ${user_logged_in_icon}
    Wait Until Page Contains Element    ${change_password_icon}  ${TIMEOUT_20}
    BuiltIn.sleep  1
    Click Element    ${change_password_icon}
    Wait Until Page Contains Element    ${current_password_field}  ${TIMEOUT_20}
    Input Text    ${current_password_field}  ${PASSWORD}
    Wait Until Page Contains Element    ${new_password_input}  ${TIMEOUT_20}
    Input Text    ${new_password_input}  ${password_security_password_lowercase}
    Wait Until Page Contains Element    ${reenter_new_password}  ${TIMEOUT_20}
    Input Text    ${reenter_new_password}  ${password_security_password_lowercase}
    Wait Until Element Is Visible    ${update_password_error_message}
    Log to console  Password should contain at least one lowercase
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

ONE DIGIT PASSWORD VALIDATION WHILE CHANGING
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${user_logged_in_icon}  ${TIMEOUT_20}
    Click Element    ${user_logged_in_icon}
    Wait Until Page Contains Element    ${change_password_icon}  ${TIMEOUT_20}
    BuiltIn.sleep  1
    Click Element    ${change_password_icon}
    Wait Until Page Contains Element    ${current_password_field}  ${TIMEOUT_20}
    Input Text    ${current_password_field}  ${PASSWORD}
    Wait Until Page Contains Element    ${new_password_input}  ${TIMEOUT_20}
    Input Text    ${new_password_input}  ${password_security_password_one_digit}
    Wait Until Page Contains Element    ${reenter_new_password}  ${TIMEOUT_20}
    Input Text    ${reenter_new_password}  ${password_security_password_one_digit}
    Wait Until Element Is Visible    ${update_password_error_message}
    Log to console  Password should contain at least one digit
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

SPECIAL CHARACTER PASSWORD VALIDATION WHILE CHANGING
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${user_logged_in_icon}  ${TIMEOUT_20}
    Click Element    ${user_logged_in_icon}
    Wait Until Page Contains Element    ${change_password_icon}  ${TIMEOUT_20}
    BuiltIn.sleep  1
    Click Element    ${change_password_icon}
    Wait Until Page Contains Element    ${current_password_field}  ${TIMEOUT_20}
    Input Text    ${current_password_field}  ${PASSWORD}
    Wait Until Page Contains Element    ${new_password_input}  ${TIMEOUT_20}
    Input Text    ${new_password_input}  ${password_security_password_special_char}
    Wait Until Page Contains Element    ${reenter_new_password}  ${TIMEOUT_20}
    Input Text    ${reenter_new_password}  ${password_security_password_special_char}
    Wait Until Element Is Visible    ${update_password_error_message}
    Log to console  Password should contain at least one special character
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

MIN 10 CHARACTERS PASSWORD VALIDATION WHILE CHANGING
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${user_logged_in_icon}  ${TIMEOUT_20}
    Click Element    ${user_logged_in_icon}
    Wait Until Page Contains Element    ${change_password_icon}  ${TIMEOUT_20}
    BuiltIn.sleep  1
    Click Element    ${change_password_icon}
    Wait Until Page Contains Element    ${current_password_field}  ${TIMEOUT_20}
    Input Text    ${current_password_field}  ${PASSWORD}
    Wait Until Page Contains Element    ${new_password_input}  ${TIMEOUT_20}
    Input Text    ${new_password_input}  ${password_security_password_min_10_char}
    Wait Until Page Contains Element    ${reenter_new_password}  ${TIMEOUT_20}
    Input Text    ${reenter_new_password}  ${password_security_password_min_10_char}
    Wait Until Element Is Visible    ${update_password_error_message}
    Log to console  Password should contain at least 10 characters
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

MAX 128 CHARACTERS PASSWORD VALIDATION WHILE CHANGING
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${user_logged_in_icon}  ${TIMEOUT_20}
    Click Element    ${user_logged_in_icon}
    Wait Until Page Contains Element    ${change_password_icon}  ${TIMEOUT_20}
    BuiltIn.sleep  1
    Click Element    ${change_password_icon}
    Wait Until Page Contains Element    ${current_password_field}  ${TIMEOUT_20}
    Input Text    ${current_password_field}  ${PASSWORD}
    Wait Until Page Contains Element    ${new_password_input}  ${TIMEOUT_20}
    Input Text    ${new_password_input}  ${password_security_password_max_128_char}
    Wait Until Page Contains Element    ${reenter_new_password}  ${TIMEOUT_20}
    Input Text    ${reenter_new_password}  ${password_security_password_max_128_char}
    Wait Until Element Is Visible    ${update_password_error_message}
    Log to console  Password should contain at most 128 characters
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

IDENTICAL CHARACTERS PASSWORD VALIDATION WHILE CHANGING
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${user_logged_in_icon}  ${TIMEOUT_20}
    Click Element    ${user_logged_in_icon}
    Wait Until Page Contains Element    ${change_password_icon}  ${TIMEOUT_20}
    BuiltIn.sleep  1
    Click Element    ${change_password_icon}
    Wait Until Page Contains Element    ${current_password_field}  ${TIMEOUT_20}
    Input Text    ${current_password_field}  ${PASSWORD}
    Wait Until Page Contains Element    ${new_password_input}  ${TIMEOUT_20}
    Input Text    ${new_password_input}  ${password_security_password_2_identical_char}
    Wait Until Page Contains Element    ${reenter_new_password}  ${TIMEOUT_20}
    Input Text    ${reenter_new_password}  ${password_security_password_2_identical_char}
    Wait Until Element Is Visible    ${update_password_error_message}
    Log to console  Password should contain more than 2 identical characters in a row
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

RESET PASSWORD WITH 2 OUT OF 4 RULES
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${portal_users}  ${TIMEOUT_20}
    Click Element    ${portal_users}
    Wait Until Page Contains Element    ${reset_password_button}  ${TIMEOUT_20}
    Click Element    ${reset_password_button}
    Wait Until Page Contains Element    ${new_password_input}  ${TIMEOUT_20}
    Input Text    ${new_password_input}  ${password_security_password_2_out_of_4}
    Wait Until Page Contains Element    ${new_user_password_confirm}  ${TIMEOUT_20}
    Input Text    ${new_user_password_confirm}  ${password_security_password_2_out_of_4}
    Wait Until Element is Visible    ${update_password_error_message}
    Log to console  Cannot reset password with only 2 conditions
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

CHANGE PASSWORD WITH INCORRECT OLD PASSWORD
    ${RESULT}  Set Variable  FAIL
    TEST SETUP
    Wait Until Page Contains Element    ${user_logged_in_icon}  ${TIMEOUT_20}
    Click Element    ${user_logged_in_icon}
    Wait Until Page Contains Element    ${change_password_icon}  ${TIMEOUT_20}
    BuiltIn.sleep  1
    Click Element    ${change_password_icon}
    Wait Until Page Contains Element    ${current_password_field}  ${TIMEOUT_20}
    Input Text    ${current_password_field}  ${wrong_old_password}
    Wait Until Page Contains Element    ${new_password_input}  ${TIMEOUT_20}
    Input Text    ${new_password_input}  ${new_password}
    Wait Until Page Contains Element    ${reenter_new_password}  ${TIMEOUT_20}
    Input Text    ${reenter_new_password}  ${new_password}
    Click Element  ${save_button}
    Wait Until Page Contains    ${current_password_incorrect_error}
    Log to console  Cannot reeset password as current password is incorrect
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

CHANGE PASSWORD SUCCESSFULLY
    ${RESULT}  Set Variable  FAIL
    Run Keyword And Ignore Error  LOGOUT FROM NEPTUNE
    LOGIN TO NEPTUNE  ${TESTUSERNAME}  ${PASSWORD}
    Wait Until Page Contains Element    ${user_logged_in_icon}  ${TIMEOUT_20}
    Click Element    ${user_logged_in_icon}
    Wait Until Page Contains Element    ${change_password_icon}  ${TIMEOUT_20}
    BuiltIn.sleep  1
    Click Element    ${change_password_icon}
    Wait Until Page Contains Element    ${current_password_field}  ${TIMEOUT_20}
    Input Text    ${current_password_field}  ${PASSWORD}
    Wait Until Page Contains Element    ${new_password_input}  ${TIMEOUT_20}
    Input Text    ${new_password_input}  ${new_password}
    Wait Until Page Contains Element    ${reenter_new_password}  ${TIMEOUT_20}
    Input Text    ${reenter_new_password}  ${new_password}
    Click Element  ${save_button}
    LOGIN TO NEPTUNE  ${TESTUSERNAME}  ${new_password}
    Wait Until Page Contains Element    ${user_logged_in_icon}  ${TIMEOUT_20}
    Click Element    ${user_logged_in_icon}
    Wait Until Page Contains Element    ${change_password_icon}  ${TIMEOUT_20}
    BuiltIn.sleep  1
    Click Element    ${change_password_icon}
    Wait Until Page Contains Element    ${current_password_field}  ${TIMEOUT_20}
    Input Text    ${current_password_field}  ${new_password}
    Wait Until Page Contains Element    ${new_password_input}  ${TIMEOUT_20}
    Input Text    ${new_password_input}  ${PASSWORD}
    Wait Until Page Contains Element    ${reenter_new_password}  ${TIMEOUT_20}
    Input Text    ${reenter_new_password}  ${PASSWORD}
    Click Element  ${save_button}
    Log to console  Password changed successfully
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

FORGOT PASSSWORD WITH INVALID EMAIL
    ${RESULT}  Set Variable  FAIL
    Run Keyword And Ignore Error  LOGOUT FROM NEPTUNE
    Wait Until Page Contains Element  ${forgot_password}
    Click Element  ${forgot_password}
    Wait Until Page Contains Element  ${forgot_password_email_input}
    Input Text  ${forgot_password_email_input}  ${invalid_email}
    Click Element  ${reset_button}
    Wait Until Element Is Visible  ${valid_email_error}
    Log to console  Valid email is required
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN

FORGOT PASSWORD WITH NOT REGISTERED EMAIL
    ${RESULT}  Set Variable  FAIL
    Run Keyword And Ignore Error  LOGOUT FROM NEPTUNE
    Wait Until Page Contains Element  ${forgot_password}
    Click Element  ${forgot_password}
    Wait Until Page Contains Element  ${forgot_password_email_input}
    Input Text  ${forgot_password_email_input}  ${not_registered_email}
    Click Element  ${reset_button}
    Wait Until Element Is Visible  ${not_registered_email_error}
    Log to console  Email not recognized
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  TEST TEARDOWN