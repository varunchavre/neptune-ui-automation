*** Settings ***

Library         Selenium2Library
Library         String
Library         Dialogs
Library         DateTime
Variables       ${EXECDIR}/VariableFiles/Web/Config.yaml

*** Keywords ***

# PASSWORD_SECURITY

Validate that the password for a Portal User should have at least 1 uppercase character (A-Z) when is created
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]  timeout=20
    Click Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]
    Wait Until Page Contains Element    //span
    Click Element    //span
    Wait Until Page Contains Element    xpath: //*[contains(text(), "System Management")]  timeout=20
    Click Element    xpath: //*[contains(text(), "System Management")]
    Wait Until Page Contains Element    xpath: //*[contains(text(), "Portal Users")]  timeout=20
    Click Element    xpath: //*[contains(text(), "Portal Users")]
    Wait Until Page Contains Element    //a[@href="/system-management/portal-users/list/add"]  timeout=20
    Click Link    //a[@href="/system-management/portal-users/list/add"]
    Wait Until Page Contains Element    //input[@ng-reflect-name="username"]  timeout=20
    Input Text    //input[@ng-reflect-name="username"]    ${password_security_username}
    Wait Until Page Contains Element    //input[@ng-reflect-name="password"]  timeout=20
    Input Text    //input[@ng-reflect-name="password"]    ${password_security_password_uppercase}
    Wait Until Page Contains Element    //input[@ng-reflect-name="confirmPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="confirmPassword"]    ${password_security_password_uppercase}
    Wait Until Element Is Visible    xpath=(//mat-error[@class="mat-error"])[6]
    Log to console  Password should contain at least one uppercase
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Validate that the password for a Portal User should have at least 1 lowercase character (a-z) when is created
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]  timeout=20
    Click Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]
    Wait Until Page Contains Element    //span
    Click Element    //span
    Wait Until Page Contains Element    xpath: //*[contains(text(), "System Management")]  timeout=20
    Click Element    xpath: //*[contains(text(), "System Management")]
    Wait Until Page Contains Element    xpath: //*[contains(text(), "Portal Users")]  timeout=20
    Click Element    xpath: //*[contains(text(), "Portal Users")]
    Wait Until Page Contains Element    //a[@href="/system-management/portal-users/list/add"]  timeout=20
    Click Link    //a[@href="/system-management/portal-users/list/add"]
    Wait Until Page Contains Element    //input[@ng-reflect-name="username"]  timeout=20
    Input Text    //input[@ng-reflect-name="username"]    ${password_security_username}
    Wait Until Page Contains Element    //input[@ng-reflect-name="password"]  timeout=20
    Input Text    //input[@ng-reflect-name="password"]    ${password_security_password_lowercase}
    Wait Until Page Contains Element    //input[@ng-reflect-name="confirmPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="confirmPassword"]    ${password_security_password_lowercase}
    Wait Until Element Is Visible    xpath=(//mat-error[@class="mat-error"])[6]
    Log to console  Password should contain at least one lowercase
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Validate that the password for a Portal User should have at least 1 digit (0-9) when is created
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]  timeout=20
    Click Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]
    Wait Until Page Contains Element    //span
    Click Element    //span
    Wait Until Page Contains Element    xpath: //*[contains(text(), "System Management")]  timeout=20
    Click Element    xpath: //*[contains(text(), "System Management")]
    Wait Until Page Contains Element    xpath: //*[contains(text(), "Portal Users")]  timeout=20
    Click Element    xpath: //*[contains(text(), "Portal Users")]
    Wait Until Page Contains Element    //a[@href="/system-management/portal-users/list/add"]  timeout=20
    Click Link    //a[@href="/system-management/portal-users/list/add"]
    Wait Until Page Contains Element    //input[@ng-reflect-name="username"]  timeout=20
    Input Text    //input[@ng-reflect-name="username"]    ${password_security_username}
    Wait Until Page Contains Element    //input[@ng-reflect-name="password"]  timeout=20
    Input Text    //input[@ng-reflect-name="password"]    ${password_security_password_one_digit}
    Wait Until Page Contains Element    //input[@ng-reflect-name="confirmPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="confirmPassword"]    ${password_security_password_one_digit}
    Wait Until Element Is Visible    xpath=(//mat-error[@class="mat-error"])[6]
    Log to console  Password should contain at least one digit
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Validate that the password for a Portal User should have at least 1 special character (punctuation) when is created
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]  timeout=20
    Click Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]
    Wait Until Page Contains Element    //span
    Click Element    //span
    Wait Until Page Contains Element    xpath: //*[contains(text(), "System Management")]  timeout=20
    Click Element    xpath: //*[contains(text(), "System Management")]
    Wait Until Page Contains Element    xpath: //*[contains(text(), "Portal Users")]  timeout=20
    Click Element    xpath: //*[contains(text(), "Portal Users")]
    Wait Until Page Contains Element    //a[@href="/system-management/portal-users/list/add"]  timeout=20
    Click Link    //a[@href="/system-management/portal-users/list/add"]
    Wait Until Page Contains Element    //input[@ng-reflect-name="username"]  timeout=20
    Input Text    //input[@ng-reflect-name="username"]    ${password_security_username}
    Wait Until Page Contains Element    //input[@ng-reflect-name="password"]  timeout=20
    Input Text    //input[@ng-reflect-name="password"]    ${password_security_password_special_char}
    Wait Until Page Contains Element    //input[@ng-reflect-name="confirmPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="confirmPassword"]    ${password_security_password_special_char}
    Wait Until Element Is Visible    xpath=(//mat-error[@class="mat-error"])[6]
    Log to console  Password should contain at least one special character
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Validate that the password for a Portal User should have at least 10 characters when is created
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]  timeout=20
    Click Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]
    Wait Until Page Contains Element    //span
    Click Element    //span
    Wait Until Page Contains Element    xpath: //*[contains(text(), "System Management")]  timeout=20
    Click Element    xpath: //*[contains(text(), "System Management")]
    Wait Until Page Contains Element    xpath: //*[contains(text(), "Portal Users")]  timeout=20
    Click Element    xpath: //*[contains(text(), "Portal Users")]
    Wait Until Page Contains Element    //a[@href="/system-management/portal-users/list/add"]  timeout=20
    Click Link    //a[@href="/system-management/portal-users/list/add"]
    Wait Until Page Contains Element    //input[@ng-reflect-name="username"]  timeout=20
    Input Text    //input[@ng-reflect-name="username"]    ${password_security_username}
    Wait Until Page Contains Element    //input[@ng-reflect-name="password"]  timeout=20
    Input Text    //input[@ng-reflect-name="password"]    ${password_security_password_min_10_char}
    Wait Until Page Contains Element    //input[@ng-reflect-name="confirmPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="confirmPassword"]    ${password_security_password_min_10_char}
    Wait Until Element Is Visible    xpath=(//mat-error[@class="mat-error"])[6]
    Log to console  Password should contain minimum 10 characters
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Validate that the password for a Portal User should have at most 128 characters when is created
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]  timeout=20
    Click Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]
    Wait Until Page Contains Element    //span
    Click Element    //span
    Wait Until Page Contains Element    xpath: //*[contains(text(), "System Management")]  timeout=20
    Click Element    xpath: //*[contains(text(), "System Management")]
    Wait Until Page Contains Element    xpath: //*[contains(text(), "Portal Users")]  timeout=20
    Click Element    xpath: //*[contains(text(), "Portal Users")]
    Wait Until Page Contains Element    //a[@href="/system-management/portal-users/list/add"]  timeout=20
    Click Link    //a[@href="/system-management/portal-users/list/add"]
    Wait Until Page Contains Element    //input[@ng-reflect-name="username"]  timeout=20
    Input Text    //input[@ng-reflect-name="username"]    ${password_security_username}
    Wait Until Page Contains Element    //input[@ng-reflect-name="password"]  timeout=20
    Input Text    //input[@ng-reflect-name="password"]    ${password_security_password_max_128_char}
    Wait Until Page Contains Element    //input[@ng-reflect-name="confirmPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="confirmPassword"]    ${password_security_password_max_128_char}
    Wait Until Element Is Visible    xpath=(//mat-error[@class="mat-error"])[6]
    Log to console  Password should contain at most 128 characters
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Validate that the password for a Portal User should doesn't have more than 2 identical characters in a row when is created
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]  timeout=20
    Click Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]
    Wait Until Page Contains Element    //span
    Click Element    //span
    Wait Until Page Contains Element    xpath: //*[contains(text(), "System Management")]  timeout=20
    Click Element    xpath: //*[contains(text(), "System Management")]
    Wait Until Page Contains Element    xpath: //*[contains(text(), "Portal Users")]  timeout=20
    Click Element    xpath: //*[contains(text(), "Portal Users")]
    Wait Until Page Contains Element    //a[@href="/system-management/portal-users/list/add"]  timeout=20
    Click Link    //a[@href="/system-management/portal-users/list/add"]
    Wait Until Page Contains Element    //input[@ng-reflect-name="username"]  timeout=20
    Input Text    //input[@ng-reflect-name="username"]    ${password_security_username}
    Wait Until Page Contains Element    //input[@ng-reflect-name="password"]  timeout=20
    Input Text    //input[@ng-reflect-name="password"]    ${password_security_password_2_identical_char}
    Wait Until Page Contains Element    //input[@ng-reflect-name="confirmPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="confirmPassword"]    ${password_security_password_2_identical_char}
    Wait Until Element Is Visible    xpath=(//mat-error[@class="mat-error"])[6]
    Log to console  Password should not contain 2 identical characters in a row
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Validate that the password for a Portal User should have at least 1 uppercase character (A-Z) when is edited
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]  timeout=20
    Click Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]
    Wait Until Page Contains Element    //span
    Click Element    //span
    Wait Until Page Contains Element    xpath: //*[contains(text(), "System Management")]  timeout=20
    Click Element    xpath: //*[contains(text(), "System Management")]
    Wait Until Page Contains Element    xpath: //*[contains(text(), "Portal Users")]  timeout=20
    Click Element    xpath: //*[contains(text(), "Portal Users")]
    Wait Until Page Contains Element    //i[@class="fa fa-lock"]  timeout=20
    Click Element    //i[@class="fa fa-lock"]
    Wait Until Page Contains Element    //input[@ng-reflect-name="newPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="newPassword"]    ${password_security_password_uppercase}
    Wait Until Page Contains Element    //input[@ng-reflect-name="confirmPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="confirmPassword"]    ${password_security_password_uppercase}
    Wait Until Page Contains Element    //mat-error[@class="text-right mat-error"]
    Log to console  Password should contain at least one uppercase
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}



Validate that the password for a Portal User should have at least 1 digit (0-9) when is edited
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]  timeout=20
    Click Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]
    Wait Until Page Contains Element    //span
    Click Element    //span
    Wait Until Page Contains Element    xpath: //*[contains(text(), "System Management")]  timeout=20
    Click Element    xpath: //*[contains(text(), "System Management")]
    Wait Until Page Contains Element    xpath: //*[contains(text(), "Portal Users")]  timeout=20
    Click Element    xpath: //*[contains(text(), "Portal Users")]
    Wait Until Page Contains Element    //i[@class="fa fa-lock"]  timeout=20
    Click Element    //i[@class="fa fa-lock"]
    Wait Until Page Contains Element    //input[@ng-reflect-name="newPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="newPassword"]    ${password_security_password_one_digit}
    Wait Until Page Contains Element    //input[@ng-reflect-name="confirmPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="confirmPassword"]    ${password_security_password_one_digit}
    Wait Until Page Contains Element    //mat-error[@class="text-right mat-error"]
    Log to console  Password should contain at least one digit
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Validate that the password for a Portal User should have at least 1 special character (punctuation) when is edited
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]  timeout=20
    Click Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]
    Wait Until Page Contains Element    //span
    Click Element    //span
    Wait Until Page Contains Element    xpath: //*[contains(text(), "System Management")]  timeout=20
    Click Element    xpath: //*[contains(text(), "System Management")]
    Wait Until Page Contains Element    xpath: //*[contains(text(), "Portal Users")]  timeout=20
    Click Element    xpath: //*[contains(text(), "Portal Users")]
    Wait Until Page Contains Element    //i[@class="fa fa-lock"]  timeout=20
    Click Element    //i[@class="fa fa-lock"]
    Wait Until Page Contains Element    //input[@ng-reflect-name="newPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="newPassword"]    ${password_security_password_special_char}
    Wait Until Page Contains Element    //input[@ng-reflect-name="confirmPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="confirmPassword"]    ${password_security_password_special_char}
    Wait Until Page Contains Element    //mat-error[@class="text-right mat-error"]
    Log to console  Password should contain at least one special character
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Validate that the password for a Portal User should have at least 10 characters when is edited
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]  timeout=20
    Click Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]
    Wait Until Page Contains Element    //span
    Click Element    //span
    Wait Until Page Contains Element    xpath: //*[contains(text(), "System Management")]  timeout=20
    Click Element    xpath: //*[contains(text(), "System Management")]
    Wait Until Page Contains Element    xpath: //*[contains(text(), "Portal Users")]  timeout=20
    Click Element    xpath: //*[contains(text(), "Portal Users")]
    Wait Until Page Contains Element    //i[@class="fa fa-lock"]  timeout=20
    Click Element    //i[@class="fa fa-lock"]
    Wait Until Page Contains Element    //input[@ng-reflect-name="newPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="newPassword"]    ${password_security_password_min_10_char}
    Wait Until Page Contains Element    //input[@ng-reflect-name="confirmPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="confirmPassword"]    ${password_security_password_min_10_char}
    Wait Until Page Contains Element    //mat-error[@class="text-right mat-error"]
    Log to console  Password should contain minimum 10 characters
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Validate that the password for a Portal User should have at most 128 characters when is edited
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]  timeout=20
    Click Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]
    Wait Until Page Contains Element    //span
    Click Element    //span
    Wait Until Page Contains Element    xpath: //*[contains(text(), "System Management")]  timeout=20
    Click Element    xpath: //*[contains(text(), "System Management")]
    Wait Until Page Contains Element    xpath: //*[contains(text(), "Portal Users")]  timeout=20
    Click Element    xpath: //*[contains(text(), "Portal Users")]
    Wait Until Page Contains Element    //i[@class="fa fa-lock"]  timeout=20
    Click Element    //i[@class="fa fa-lock"]
    Wait Until Page Contains Element    //input[@ng-reflect-name="newPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="newPassword"]    ${password_security_password_max_128_char}
    Wait Until Page Contains Element    //input[@ng-reflect-name="confirmPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="confirmPassword"]    ${password_security_password_max_128_char}
    Wait Until Page Contains Element    //mat-error[@class="text-right mat-error"]
    Log to console  Password should contain at most 128 characters
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Validate that the password for a Portal User should doesn't have more than 2 identical characters in a row when is edited
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]  timeout=20
    Click Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]
    Wait Until Page Contains Element    //span
    Click Element    //span
    Wait Until Page Contains Element    xpath: //*[contains(text(), "System Management")]  timeout=20
    Click Element    xpath: //*[contains(text(), "System Management")]
    Wait Until Page Contains Element    xpath: //*[contains(text(), "Portal Users")]  timeout=20
    Click Element    xpath: //*[contains(text(), "Portal Users")]
    Wait Until Page Contains Element    //i[@class="fa fa-lock"]  timeout=20
    Click Element    //i[@class="fa fa-lock"]
    Wait Until Page Contains Element    //input[@ng-reflect-name="newPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="newPassword"]    ${password_security_password_2_identical_char}
    Wait Until Page Contains Element    //input[@ng-reflect-name="confirmPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="confirmPassword"]    ${password_security_password_2_identical_char}
    Wait Until Page Contains Element    //mat-error[@class="text-right mat-error"]
    Log to console  Password should not contain 2 identical characters in a row
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Validate that the password for a Portal User should have at least 1 uppercase character (A-Z) when is edited from reset password functionality
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //a[@class="user-name"]  timeout=20
    Click Element    //a[@class="user-name"]
    Wait Until Page Contains Element    xpath=(//mat-icon[@class="mat-icon material-icons"])[7]  timeout=20
    Click Element    xpath=(//mat-icon[@class="mat-icon material-icons"])[7]
    Wait Until Page Contains Element    //input[@ng-reflect-name="currentPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="currentPassword"]  ${PASSWORD}
    Wait Until Page Contains Element    //input[@ng-reflect-name="newPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="newPassword"]    ${password_security_password_uppercase}
    Wait Until Page Contains Element    //input[@ng-reflect-name="newPasswordConfirm"]  timeout=20
    Input Text    //input[@ng-reflect-name="newPasswordConfirm"]    ${password_security_password_uppercase}
    Wait Until Element Is Visible    //mat-error[@class="text-right mat-error"]
    Log to console  Password should contain at least one uppercase
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Validate that the password for a Portal User should have at least 1 lowercase character (a-z) when is edited from reset password functionality
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //a[@class="user-name"]  timeout=20
    Click Element    //a[@class="user-name"]
    Wait Until Page Contains Element    xpath=(//mat-icon[@class="mat-icon material-icons"])[7]  timeout=20
    Click Element    xpath=(//mat-icon[@class="mat-icon material-icons"])[7]
    Wait Until Page Contains Element    //input[@ng-reflect-name="currentPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="currentPassword"]  ${PASSWORD}
    Wait Until Page Contains Element    //input[@ng-reflect-name="newPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="newPassword"]    ${password_security_password_lowercase}
    Wait Until Page Contains Element    //input[@ng-reflect-name="newPasswordConfirm"]  timeout=20
    Input Text    //input[@ng-reflect-name="newPasswordConfirm"]    ${password_security_password_lowercase}
    Wait Until Element Is Visible    //mat-error[@class="text-right mat-error"]
    Log to console  Password should contain at least one uppercase
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Validate that the password for a Portal User should have at least 1 digit (0-9) when is edited from reset password functionality
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //a[@class="user-name"]  timeout=20
    Click Element    //a[@class="user-name"]
    Wait Until Page Contains Element    xpath=(//mat-icon[@class="mat-icon material-icons"])[7]  timeout=20
    Click Element    xpath=(//mat-icon[@class="mat-icon material-icons"])[7]
    Wait Until Page Contains Element    //input[@ng-reflect-name="currentPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="currentPassword"]  ${PASSWORD}
    Wait Until Page Contains Element    //input[@ng-reflect-name="newPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="newPassword"]    ${password_security_password_one_digit}
    Wait Until Page Contains Element    //input[@ng-reflect-name="newPasswordConfirm"]  timeout=20
    Input Text    //input[@ng-reflect-name="newPasswordConfirm"]    ${password_security_password_one_digit}
    Wait Until Element Is Visible    //mat-error[@class="text-right mat-error"]
    Log to console  Password should contain at least one uppercase
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Validate that the password for a Portal User should have at least 1 special character (punctuation) when is edited from reset password functionality
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //a[@class="user-name"]  timeout=20
    Click Element    //a[@class="user-name"]
    Wait Until Page Contains Element    xpath=(//mat-icon[@class="mat-icon material-icons"])[7]  timeout=20
    Click Element    xpath=(//mat-icon[@class="mat-icon material-icons"])[7]
    Wait Until Page Contains Element    //input[@ng-reflect-name="currentPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="currentPassword"]  ${PASSWORD}
    Wait Until Page Contains Element    //input[@ng-reflect-name="newPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="newPassword"]    ${password_security_password_special_char}
    Wait Until Page Contains Element    //input[@ng-reflect-name="newPasswordConfirm"]  timeout=20
    Input Text    //input[@ng-reflect-name="newPasswordConfirm"]    ${password_security_password_special_char}
    Wait Until Element Is Visible    //mat-error[@class="text-right mat-error"]
    Log to console  Password should contain at least one uppercase
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Validate that the password for a Portal User should have at least 10 characters when is edited from reset password functionality
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //a[@class="user-name"]  timeout=20
    Click Element    //a[@class="user-name"]
    Wait Until Page Contains Element    xpath=(//mat-icon[@class="mat-icon material-icons"])[7]  timeout=20
    Click Element    xpath=(//mat-icon[@class="mat-icon material-icons"])[7]
    Wait Until Page Contains Element    //input[@ng-reflect-name="currentPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="currentPassword"]  ${PASSWORD}
    Wait Until Page Contains Element    //input[@ng-reflect-name="newPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="newPassword"]    ${password_security_password_min_10_char}
    Wait Until Page Contains Element    //input[@ng-reflect-name="newPasswordConfirm"]  timeout=20
    Input Text    //input[@ng-reflect-name="newPasswordConfirm"]    ${password_security_password_min_10_char}
    Wait Until Element Is Visible    //mat-error[@class="text-right mat-error"]
    Log to console  Password should contain at least one uppercase
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Validate that the password for a Portal User should have at most 128 characters when is edited from reset password functionality
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //a[@class="user-name"]  timeout=20
    Click Element    //a[@class="user-name"]
    Wait Until Page Contains Element    xpath=(//mat-icon[@class="mat-icon material-icons"])[7]  timeout=20
    Click Element    xpath=(//mat-icon[@class="mat-icon material-icons"])[7]
    Wait Until Page Contains Element    //input[@ng-reflect-name="currentPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="currentPassword"]  ${PASSWORD}
    Wait Until Page Contains Element    //input[@ng-reflect-name="newPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="newPassword"]    ${password_security_password_max_128_char}
    Wait Until Page Contains Element    //input[@ng-reflect-name="newPasswordConfirm"]  timeout=20
    Input Text    //input[@ng-reflect-name="newPasswordConfirm"]    ${password_security_password_max_128_char}
    Wait Until Element Is Visible    //mat-error[@class="text-right mat-error"]
    Log to console  Password should contain at least one uppercase
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Validate that the password for a Portal User should doesn't have more than 2 identical characters in a row when is edited from reset password functionality
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //a[@class="user-name"]  timeout=20
    Click Element    //a[@class="user-name"]
    Wait Until Page Contains Element    xpath=(//mat-icon[@class="mat-icon material-icons"])[7]  timeout=20
    Click Element    xpath=(//mat-icon[@class="mat-icon material-icons"])[7]
    Wait Until Page Contains Element    //input[@ng-reflect-name="currentPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="currentPassword"]  ${PASSWORD}
    Wait Until Page Contains Element    //input[@ng-reflect-name="newPassword"]  timeout=20
    Input Text    //input[@ng-reflect-name="newPassword"]    ${password_security_password_2_identical_char}
    Wait Until Page Contains Element    //input[@ng-reflect-name="newPasswordConfirm"]  timeout=20
    Input Text    //input[@ng-reflect-name="newPasswordConfirm"]    ${password_security_password_2_identical_char}
    Wait Until Element Is Visible    //mat-error[@class="text-right mat-error"]
    Log to console  Password should contain at least one uppercase
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}


# Create_Attributes

Verify that Attributes page display the correct Content
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    xpath=(//mat-card[@class="mat-card"])[7]    timeout=20
    Click Element    xpath=(//mat-card[@class="mat-card"])[7]
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Verify that new attribute can be created
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    xpath=//a[@title="Click here to manage user attributes"]    timeout=20
    Click Element    xpath=//a[@title="Click here to manage user attributes"]
    Wait Until Page Contains Element    //input[@name="query"]    timeout=10
    Input Text    //input[@ng-reflect-name="query"]    ${Name}
    Run Keyword And Ignore Error  Wait Until Page Contains Element    //i[@class="fa fa-trash"]    timeout=40
    Run Keyword And Ignore Error  Click Element    //i[@class="fa fa-trash"]
    Run Keyword And Ignore Error  Click Element    xpath: //*[contains(text(), "Yes")]
    Wait Until Page Contains Element  //a[@class="link-btn ng-star-inserted"]  timeout=20
    Click Link    //a[@class="link-btn ng-star-inserted"]
    Input Text    //input[@ng-reflect-name="name"]    ${Name}
    Input Text    //input[@ng-reflect-name="displayName"]    ${Display Name}
    Input Text    //input[@ng-reflect-name="uiOrder"]    ${UI Order}
    Click Element    //div[@class="mat-slide-toggle-thumb"]
    Wait Until Page Contains Element    xpath=(//div[@class="mat-slide-toggle-thumb"])[2]    timeout=10
    Click Element    xpath=(//div[@class="mat-slide-toggle-thumb"])[2]
    Click Element  //mat-select[@ng-reflect-name="type"]
    Click Element  //mat-option[@ng-reflect-value="STRING"]
    Click Element  //button[@type="submit"]
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Verify that name is a mandatory field
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    xpath=//a[@title="Click here to manage user attributes"]    timeout=20
    Click Element    xpath=//a[@title="Click here to manage user attributes"]
    Wait Until Page Contains Element    xpath=//a[@class="link-btn ng-star-inserted"]    timeout=20
    Click Link    //a[@class="link-btn ng-star-inserted"]
    Click Element    //input[@ng-reflect-name="name"]
    Input Text    //input[@ng-reflect-name="displayName"]    ${Display Name}
    Input Text    //input[@ng-reflect-name="uiOrder"]    ${UI Order}
    Click Element    //div[@class="mat-slide-toggle-thumb"]
    Wait Until Page Contains Element    xpath=(//div[@class="mat-slide-toggle-thumb"])[2]    timeout=10
    Click Element    xpath=(//div[@class="mat-slide-toggle-thumb"])[2]
    Click Element  //mat-select[@ng-reflect-name="type"]
    Click Element  //mat-option[@ng-reflect-value="STRING"]
    Element Should Be Disabled  //button[@type="submit"]
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Verify that required is a mandatory field
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    xpath=//a[@title="Click here to manage user attributes"]    timeout=20
    Click Element    xpath=//a[@title="Click here to manage user attributes"]
    Wait Until Page Contains Element    xpath=//a[@class="link-btn ng-star-inserted"]    timeout=20
    Click Link    //a[@class="link-btn ng-star-inserted"]
    Input Text    //input[@ng-reflect-name="name"]    ${Name}
    Input Text    //input[@ng-reflect-name="displayName"]    ${Display Name}
    Input Text    //input[@ng-reflect-name="uiOrder"]    ${UI Order}
    Wait Until Page Contains Element    xpath=(//div[@class="mat-slide-toggle-thumb"])[2]    timeout=10
    Click Element    xpath=(//div[@class="mat-slide-toggle-thumb"])[2]
    Click Element  //mat-select[@ng-reflect-name="type"]
    Click Element  //mat-option[@ng-reflect-value="STRING"]
    Click Element  //button[@type="submit"]
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Verify that displayName is a mandatory field to fill when creating a new attribute
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    xpath=//a[@title="Click here to manage user attributes"]    timeout=20
    Click Element    xpath=//a[@title="Click here to manage user attributes"]
    Wait Until Page Contains Element    xpath=//a[@class="link-btn ng-star-inserted"]    timeout=20
    Click Link    //a[@class="link-btn ng-star-inserted"]
    Input Text    //input[@ng-reflect-name="name"]    ${Name}
    Click Element    //input[@ng-reflect-name="displayName"]
    Input Text    //input[@ng-reflect-name="uiOrder"]    ${UI Order}
    Click Element    //div[@class="mat-slide-toggle-thumb"]
    Wait Until Page Contains Element    xpath=(//div[@class="mat-slide-toggle-thumb"])[2]    timeout=10
    Click Element    xpath=(//div[@class="mat-slide-toggle-thumb"])[2]
    Click Element  //mat-select[@ng-reflect-name="type"]
    Click Element  //mat-option[@ng-reflect-value="STRING"]
    Element Should Be Disabled  //button[@type="submit"]
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Verify uiOrder is a mandatory field to fill when creating a new attribute
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    xpath=//a[@title="Click here to manage user attributes"]    timeout=20
    Click Element    xpath=//a[@title="Click here to manage user attributes"]
    Wait Until Page Contains Element    xpath=//a[@class="link-btn ng-star-inserted"]    timeout=20
    Click Link    //a[@class="link-btn ng-star-inserted"]
    Input Text    //input[@ng-reflect-name="name"]    ${Name}
    Input Text    //input[@ng-reflect-name="displayName"]    ${Display Name}
    Click Element    //input[@ng-reflect-name="uiOrder"]
    Click Element    //div[@class="mat-slide-toggle-thumb"]
    Wait Until Page Contains Element    xpath=(//div[@class="mat-slide-toggle-thumb"])[2]    timeout=10
    Click Element    xpath=(//div[@class="mat-slide-toggle-thumb"])[2]
    Click Element  //mat-select[@ng-reflect-name="type"]
    Click Element  //mat-option[@ng-reflect-value="STRING"]
    Element Should Be disabled  //button[@type="submit"]
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Verify user can Create a required attribute
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    xpath=//a[@title="Click here to manage user attributes"]    timeout=20
    Click Element    xpath=//a[@title="Click here to manage user attributes"]
    Wait Until Page Contains Element    //input[@name="query"]    timeout=10
    Input Text    //input[@ng-reflect-name="query"]    ${Name}
    Run Keyword And Ignore Error  Wait Until Page Contains Element    //i[@class="fa fa-trash"]    timeout=40
    Run Keyword And Ignore Error  Click Element    //i[@class="fa fa-trash"]
    Run Keyword And Ignore Error  Click Element    xpath: //*[contains(text(), "Yes")]
    Wait Until Page Contains Element  //a[@class="link-btn ng-star-inserted"]  timeout=20
    Click Link    //a[@class="link-btn ng-star-inserted"]
    Input Text    //input[@ng-reflect-name="name"]    ${Name}
    Input Text    //input[@ng-reflect-name="displayName"]    ${Display Name}
    Input Text    //input[@ng-reflect-name="uiOrder"]    ${UI Order}
    Click Element    //div[@class="mat-slide-toggle-thumb"]
    # Wait Until Page Contains Element    xpath=(//div[@class="mat-slide-toggle-thumb"])[2]    timeout=10
    # Click Element    xpath=(//div[@class="mat-slide-toggle-thumb"])[2]
    Click Element  //mat-select[@ng-reflect-name="type"]
    Click Element  //mat-option[@ng-reflect-value="STRING"]
    # Click Element  //mat-option[@tabindex="0"]
    Element Should Be Enabled  //button[@type="submit"]
    Click Element  //button[@type="submit"]
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

#Lock/Unlock Portal Users

login with incorrect password for 1 or 2 trials
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Wait Until Page Contains Element    //input[@id="mat-input-0"]  timeout=20
    Input Text    //input[@id="mat-input-0"]    ${USERNAME}
    Wait Until Page Contains Element    //input[@id="mat-input-1"]
    Input Text    //input[@id="mat-input-1"]    Wrongpassword
    Wait Until Page Contains Element    //span[@class="mat-button-wrapper"]  timeout=20
    Click Element    //span[@class="mat-button-wrapper"]
    Wait Until Page Contains  Bad credentials.  timeout=20
    Login
    Log to Console  login with incorrect password for 1 or 2 trials validated
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

login with correct password after 1 or 2 failure trials
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Wait Until Page Contains Element    //input[@id="mat-input-0"]  timeout=20
    Input Text    //input[@id="mat-input-0"]    ${USERNAME}
    Wait Until Page Contains Element    //input[@id="mat-input-1"]
    Input Text    //input[@id="mat-input-1"]    Wrongpassword
    Wait Until Page Contains Element    //span[@class="mat-button-wrapper"]  timeout=20
    Click Element    //span[@class="mat-button-wrapper"]
    Wait Until Page Contains  Bad credentials.  timeout=20
    Wait Until Page Contains Element    //input[@id="mat-input-1"]
    Input Text    //input[@id="mat-input-1"]    ${PASSWORD}
    Wait Until Page Contains Element    //span[@class="mat-button-wrapper"]  timeout=20
    Click Element    //span[@class="mat-button-wrapper"]
    Log to Console  login with correct password after 1 or 2 failure trials verified
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}


Verify warning message when the user enter a wrong password
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Wait Until Page Contains Element    //input[@id="mat-input-0"]  timeout=20
    Input Text    //input[@id="mat-input-0"]    ${USERNAME}
    Wait Until Page Contains Element    //input[@id="mat-input-1"]
    Input Text    //input[@id="mat-input-1"]    Wrongpassword
    Wait Until Page Contains Element    //span[@class="mat-button-wrapper"]  timeout=20
    Click Element    //span[@class="mat-button-wrapper"]
    Wait Until Page Contains  Bad credentials.  timeout=20
    Login
    Log to Console  warning message when the user enter a wrong password verified
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}


Verify that user without permissions cannot lock/unlock another portal user
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login  Logintest
    Wait Until Page Contains Element    xpath=(//mat-card[@class="mat-card"])[3]  timeout=20
    Click Element    xpath=(//mat-card[@class="mat-card"])[3]
    Page Should Not Contain Element  /html/body/app-root/mat-sidenav-container/mat-sidenav-content/div/app-system-management/div/div/app-portal-users/div/mat-card/app-portal-user-list/div/app-data-grid/div/div[2]/mat-table/mat-row[1]/mat-cell[2]/span[1]/i
    Log to Console  login with correct password after 3 or more failure trials verified
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}


Verify that a lock user cannot login in the Portal
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login  Lockedadmin
    Wait Until Page Contains  User account is locked  timeout=20
    Log to Console  lock user cannot login in the Portal verified
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}


# Create Role Permission

Validate that search tool works correctly
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //div[@class="col-xs-12 col-sm margin-top-10-xs margin-bottom-10-xs end-xs"]  timeout=20
    Click Element    //div[@class="col-xs-12 col-sm margin-top-10-xs margin-bottom-10-xs end-xs"]
    Wait Until Page Contains Element    xpath=(//mat-card[@class="mat-card"])[5]  timeout=20
    Click Element    xpath=(//mat-card[@class="mat-card"])[5]
    Wait Until Page Contains Element    xpath=(//mat-cell[@class="mat-cell cdk-column-name mat-column-name ng-star-inserted"])[4]    timeout=20
    Click Element    xpath=(//mat-cell[@class="mat-cell cdk-column-name mat-column-name ng-star-inserted"])[4]
    Wait Until Page Contains Element    //input[@name="query"]  timeout=20
    Input Text    //input[@name="query"]    Viewlicenses
    Wait Until Page Contains Element    xpath=//*[contains(text(), "Viewlicenses")]  timeout=20
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Validate that Devices page only can have View and Delete Permissions
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    xpath=(//mat-card[@class="mat-card"])[5]  timeout=20
    Click Element    xpath=(//mat-card[@class="mat-card"])[5]
    Wait Until Page Contains Element    //a[@class="link-btn ng-star-inserted"]  timeout=20
    Click Link    //a[@class="link-btn ng-star-inserted"]
    Wait Until Page Contains Element    xpath=(//mat-panel-title[@class="text-no-wrap mat-expansion-panel-header-title"])[3]  timeout=20
    Click Element    xpath=(//mat-panel-title[@class="text-no-wrap mat-expansion-panel-header-title"])[3]
    Wait Until Page Contains Element    //*[@id="cdk-accordion-child-3"]/div  timeout=20
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}


Verify that AD list page display the correct content
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element    //h1[@class="padding-top-10 padding-left-10"]  timeout=20
    Click Element    //h1[@class="padding-top-10 padding-left-10"]
    Wait Until Page Contains Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]  timeout=20
    Click Element    //mat-icon[@class="mat-icon material-icons ng-star-inserted"]
    Wait Until Page Contains Element    xpath=//*[contains(text(), "Users & Group Import")]  timeout=20
    Click Link    xpath=//*[contains(text(), "Users & Group Import")]
    Wait Until Page Contains Element    xpath=//*[contains(text(), "Jobs")]  timeout=20
    Click Element    xpath=//*[contains(text(), "Jobs")]
    Wait Until Page Contains Element  xpath=//*[contains(text(), "Name")]  timeout=20
    Wait Until Page Contains Element  xpath=//*[contains(text(), "Description")]  timeout=20
    Wait Until Page Contains Element  xpath=//*[contains(text(), "CreatedOnTime")]  timeout=20
    Wait Until Page Contains Element  xpath=//*[contains(text(), "Last Updated Time")]  timeout=20
    Wait Until Page Contains Element  xpath=//*[contains(text(), "Create Job")]  timeout=20
    Wait Until Page Contains Element    xpath=(//mat-icon[@class="mat-icon material-icons"])[7]  timeout=20
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

Validate that Role Permissions page display the correct content
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element  xpath=(//mat-card[@class="mat-card"])[5]  timeout=20
    click element  xpath=(//mat-card[@class="mat-card"])[5]
    Wait Until Page Contains  Portal Roles  timeout=20
    Wait Until Page Contains  All Portal Roles  timeout=20
    Page Should Contain Element  //div[@class="mat-paginator-page-size ng-star-inserted"]
    Page Should Contain Element  //div[@class="mat-paginator-range-actions"]
    Page Should Contain Element  //input[@ng-reflect-name="query"]
    Page Should Contain  Name
    Page Should Contain  Type
    Page Should Contain  Description
    Page Should Contain  CreatedOnTime
    Page Should Contain  Last Updated Time
    Page Should Contain Element  //i[@class="fa fa-pencil"]
    Page Should Contain Element  //i[@class="fa fa-trash"]
    Log to Console  Validate that Role Permissions page display the correct content
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}

# Import Extensions
Import Telephone Extension from a file with incorrect format
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Import Extension Tab  xyz.txt
    Builtin.Sleep  ${SLEEP}
    Page Should Contain  ${PLEASE_SELECT_VALID_FILE}  loglevel=INFO
    Log to Console  Import Telephone Extension from a file with incorrect format verified
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}


Validate mandatory fields when import telephone extensions
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Import Extension Tab  sample3.csv
    Builtin.Sleep  ${SLEEP}
    Page Should Contain  ${MANDATORY_FIELDS_MISSING}  loglevel=INFO
    Log to Console  Validate mandatory fields when import telephone extensions verified
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}


Import an empty telephone extension file
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Import Extension Tab  empty.csv
    Builtin.Sleep  ${SLEEP}
    Page Should Contain  ${NOTHING_TO_IMPORT}  loglevel=INFO
    Log to Console  Import an empty telephone extension file verified
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}


Verify that Import Extension tab display the correct content
    [Arguments]  ${ITERATION}
    ${RESULT}  Set Variable  FAIL
    Log To Console  \nIteration No.: ${ITERATION}
    Launch Browser
    Login
    Wait Until Page Contains Element     xpath=(//mat-card[@class="mat-card"])[10]  timeout=20
    Click Element    xpath=(//mat-card[@class="mat-card"])[10]
    Wait Until Page Contains Element    //a[@class="mat-tab-link ng-star-inserted"]  timeout=20
    Click Link    //a[@class="mat-tab-link ng-star-inserted"]
    Wait Until Page Contains Element  //form[@class="ng-untouched ng-pristine ng-invalid"]    timeout=20
    Page Should Contain  Upload the file to add bulk extensions  loglevel=INFO
    Page Should Contain  Overwrite deltas  loglevel=INFO
    Page Should Contain  Replace entire database  loglevel=INFO
    Page Should Contain  Upload or Drag csv file here  loglevel=INFO
    Page Should Contain  Reminder: database is case sensitive, review your file before importing  loglevel=INFO
    Page Should Contain  Cancel  loglevel=INFO
    Page Should Contain  Import  loglevel=INFO
    Log to Console  Verify that Import Extension tab display the correct content
    Close Browser
    ${RESULT}  Set Variable  PASS
    [Teardown]  Run Keyword If  '${RESULT}' == 'FAIL'  For Loop Failure Teardown  ${ITERATION}
