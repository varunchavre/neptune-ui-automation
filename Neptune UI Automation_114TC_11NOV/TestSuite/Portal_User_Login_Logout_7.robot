
*** Settings ***

# Library         Selenium2Library
Library         ExtendedSelenium2Library
Library         OperatingSystem
Library         String
Library         Dialogs
Resource        ${EXECDIR}/Res/Web/GenericFunction.robot
Resource        ${EXECDIR}/Res/Web/Portal_User_Login_Logout_Resource.robot
Variables       ${EXECDIR}/VariableFiles/Web/Config.yaml
Suite Setup     LAUNCH BROWSER
Suite Teardown  Close Browser
Metadata    Version        0.6


*** Test Cases ***

Login Page for Admin Portal
     [Tags]  C709605  Portal User Login/Logout
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  LOGIN PAGE VALIDATION

Login as an system administrator
     [Tags]  C626778  Portal User Login/Logout
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  DASHBOARD VALIDATION SYSTEM ADMIN

Login as a regular administrator.
     [Tags]  C709571  Portal User Login/Logout
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  DASHBOARD VALIDATION REGULAR ADMIN

Login as a regular administrator with no permissions assigned
     [Tags]  C709572  Portal User Login/Logout
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  DASHBOARD VALIDATION NO PERMISSION ADMIN

Login name has uppercase validation
     [Tags]  C662958  Portal User Login/Logout
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  LOGIN UPPERCASE VALIDATION

Login with incorrect user or password
     [Tags]  C662959  Portal User Login/Logout
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  LOGIN WITH INCORRECT CREDENTIALS

Log out as an administrator
     [Tags]  C703527  Portal User Login/Logout
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  LOGOUT AS AN ADMIN

