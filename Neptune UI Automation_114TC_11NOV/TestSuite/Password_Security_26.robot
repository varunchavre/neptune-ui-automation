*** Settings ***
#Section to import libraries and other dependent modules
Library         ExtendedSelenium2Library
Library         OperatingSystem
Library         String
Library         Dialogs
Resource        ${EXECDIR}/Res/Web/GenericFunction.robot
Resource        ${EXECDIR}/Res/Web/Password_Security_Resource.robot
Variables       ${EXECDIR}/VariableFiles/Web/Config.yaml
Suite Setup     SUITE SETUP
Suite Teardown  Close Browser
Metadata    Version        0.6

*** Test Cases ***

Create Portal User:password should have at least 1 uppercase character (A-Z)
    [Tags]  C699065  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  UPPERCASE PASSWORD VALIDATION FOR NEW USER

Create Portal User:password should have at least 1 lowercase character (A-Z)
    [Tags]  C699066  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  LOWERCASE PASSWORD VALIDATION FOR NEW USER

Create Portal User:password should have at least 1 digit (0-9)
    [Tags]  C699067  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  ONE DIGIT PASSWORD VALIDATION FOR NEW USER

Create Portal User:password should have at least 1 special character (punctuation)
    [Tags]  C699068  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  SPECIAL CHARACTER PASSWORD VALIDATION FOR NEW USER

Create Portal User:password should have at least 10 characters
    [Tags]  C699069  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  MIN 10 CHARACTERS PASSWORD VALIDATION FOR NEW USER

Create Portal User:password should have at most 128 characters
    [Tags]  C699070  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  MAX 128 CHARACTERS PASSWORD VALIDATION FOR NEW USER

Create Portal User:password should not have more than 2 identical characters in a row
    [Tags]  C699071  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  IDENTICAL CHARACTERS PASSWORD VALIDATION FOR NEW USER

Reset Portal User password should have at least 1 uppercase character (A-Z)
    [Tags]  C699072  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  UPPERCASE PASSWORD VALIDATION WHILE RESETTING

Reset Portal User password should have at least 1 lowercase character (a-z)
    [Tags]  C699073  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  LOWERCASE PASSWORD VALIDATION WHILE RESETTING

Reset Portal User password should have at least 1 digit (0-9)
    [Tags]  C699074  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  ONE DIGIT PASSWORD VALIDATION WHILE RESETTING

Reset Portal User password should have at least 1 special character (punctuation)
    [Tags]  C699075  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  SPECIAL CHARACTER PASSWORD VALIDATION WHILE RESETTING

Reset Portal User password should have at least 10 characters
    [Tags]  C699076  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  MIN 10 CHARACTERS PASSWORD VALIDATION WHILE RESETTING

Reset Portal User password should have at most 128 characters
    [Tags]  C699077  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  MAX 128 CHARACTERS PASSWORD VALIDATION WHILE RESETTING

Reset Portal User password should doesn't have more than 2 identical characters in a row
    [Tags]  C699078  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  IDENTICAL CHARACTERS PASSWORD VALIDATION WHILE RESETTING

Reset Portal User password that meets 2 out of 4 rules
    [Tags]  C699674  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  RESET PASSWORD WITH 2 OUT OF 4 RULES

Change Password should have at least 1 uppercase character (A-Z)
    [Tags]  C704328  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  UPPERCASE PASSWORD VALIDATION WHILE CHANGING

Change Password should have at least 1 lowercase character (a-z)
    [Tags]  C704329  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  LOWERCASE PASSWORD VALIDATION WHILE CHANGING

Change Password should have at least 1 digit (0-9)
    [Tags]  C704330  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  ONE DIGIT PASSWORD VALIDATION WHILE CHANGING

Change Password should have at least 1 special character (punctuation)
    [Tags]  C704331  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  SPECIAL CHARACTER PASSWORD VALIDATION WHILE CHANGING

Change Password should have at least 10 characters
    [Tags]  C704332  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  MIN 10 CHARACTERS PASSWORD VALIDATION WHILE CHANGING

Change Password should have at most 128 characters
    [Tags]  C704333  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  MAX 128 CHARACTERS PASSWORD VALIDATION WHILE CHANGING

Change Password should NOT have more than 2 identical characters in a row
    [Tags]  C704334  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  IDENTICAL CHARACTERS PASSWORD VALIDATION WHILE CHANGING

Change Password with incorrect password in 'Old Password'
    [Tags]  C704335  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CHANGE PASSWORD WITH INCORRECT OLD PASSWORD

Change Password: password updated successfully
    [Tags]  C706225  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  CHANGE PASSWORD SUCCESSFULLY

"Forgot password?": user can not set a new password with an invalid email
    [Tags]  C708220  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  FORGOT PASSSWORD WITH INVALID EMAIL

"Forgot password?": user can not set a new password with a not registered email
    [Tags]  C708222  Password security
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  FORGOT PASSWORD WITH NOT REGISTERED EMAIL