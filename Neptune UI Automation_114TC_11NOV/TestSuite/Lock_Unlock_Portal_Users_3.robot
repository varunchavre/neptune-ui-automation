*** Settings ***
#Section to import libraries and other dependent modules
Library         ExtendedSelenium2Library
Library         OperatingSystem
Library         String
Library         Dialogs
Resource        ${EXECDIR}/Res/Web/GenericFunction.robot
Resource        ${EXECDIR}/Res/Web/Lock_Unlock_Portal_Users_Resource.robot
Variables       ${EXECDIR}/VariableFiles/Web/Config.yaml
Suite Setup     LAUNCH BROWSER
Suite Teardown  Close Browser
Metadata    Version        0.6


*** Test Cases ***

Login with incorrect password for 1 or 2 trials
    [Tags]  C704666  Lock/Unlock Portal Users
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  ONE OR TWO LOGIN TRIALS INCORRECT PASSWORD

Login with correct password after 1 or 2 failure trials
    [Tags]  C704667  Lock/Unlock Portal Users
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  LOGIN AFTER ONE OR TWO FAILURES

Admin user without permissions cannot lock/unlock another portal user
    [Tags]  C704674  Lock/Unlock Portal Users
    : FOR  ${ITERATION}  IN RANGE  1  ${LOOP}
    \   Log To Console  \nIteration No.: ${ITERATION}
    \   Run Keyword and Continue on Failure  USER WITHOUT PERMISSION CANNOT LOCK/UNLOCK ANOTHER USER